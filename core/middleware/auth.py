from typing import Optional

import pytz
import requests
from django.conf import settings
from django.http import JsonResponse
from django.utils import timezone
from rest_framework import status

from bot.models import Franchisee

API_HOST = settings.API_HOST


class FranchiseeMiddleware:
    # URL, для которых не проверяется токен в запросе
    URL_TOKEN_EXEMPT = (
        'admin', 'web_hook', 'views',
        'webhook', 'callback', 'public', 'debug',
    )

    def __init__(self, get_response):
        self.get_response = get_response

    def is_need_token(self, request):
        path = request.META['PATH_INFO']
        for url in self.URL_TOKEN_EXEMPT:
            if url in path:
                return False
        return True

    def process_view(self, request, view_func, view_args, view_kwargs):
        if not self.is_need_token(request):
            return

        token = self._check_auth_header(request)
        if not token:
            return JsonResponse({'message': 'Неккоректный токен'},
                                status=status.HTTP_401_UNAUTHORIZED)
        user = self._check_token(token)
        if not user:
            return JsonResponse({'message': 'Не удалось авторизоватся в Delta'},
                                status=status.HTTP_401_UNAUTHORIZED)

        selected_city = user['selectedCity']
        available_cities = user['cities']
        # Регистрируем все доступные города пользователя
        for city in available_cities:
            city_id = city['id']
            city_name = city['name']
            city_timezone = city['timezone']
            # Проверяем существование города
            try:
                franchisee = Franchisee.objects.get(api_id=city_id)
                franchisee.name = city_name
                franchisee.timezone = city_timezone
                franchisee.save()
            except Franchisee.DoesNotExist:
                Franchisee.objects.create(
                    name=city_name,
                    timezone=city_timezone,
                    api_id=city_id,
                )

        request.franchisee = Franchisee.objects.get(api_id=selected_city['id'])
        request.timezone = selected_city['timezone']
        request.token = token
        request.delta_id = user['id']
        request.permissions = user['role']['permissions']
        timezone.activate(pytz.timezone(selected_city['timezone']))

    def _check_auth_header(self, request) -> Optional[str]:
        """
        Проверка наличия токена в запросе

        Возвращает корректный токен,
        иначе False

        """

        if 'HTTP_AUTHORIZATION' not in request.META:
            return None
        auth_header = request.META['HTTP_AUTHORIZATION']

        if len(auth_header.split()) != 2:
            return None

        header_token, token = auth_header.split()
        if header_token.lower() != 'bearer':
            return None

        return token

    def _check_token(self, token) -> Optional[dict]:
        """
        Проверка корректности токена через сервис авторизации

        Возвращает пользователя, при успшной авторизации
        иначе возвращает False

        """
        auth_url = API_HOST + '/api/auth/me'
        headers = {'Authorization': f'Bearer {token}'}
        r = requests.get(auth_url, headers=headers)
        if r.status_code == 401:
            return None
        else:
            return r.json()

    def __call__(self, request):
        response = self.get_response(request)
        return response
