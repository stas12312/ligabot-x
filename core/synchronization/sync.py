import json
import logging
import re
from abc import ABC, abstractmethod
from datetime import datetime
from typing import List

import pytz
import requests
from django.db.models import Q
from django.utils import timezone
from rest_framework import exceptions

from bot.models import (User, Location, Lesson,
                        Franchisee, RegularLesson, Role,
                        DayOfWeak, CRMInfo, Notification)
from .types import *
from .utils import *

logger = logging.getLogger('celery_logger')


class BaseSynchronization(ABC):
    """Базовый класс для синхронизации"""

    def __init__(self, user: str, password: str, franchisee: Franchisee, url: str):
        self.user = user
        self.password = password
        self.franchisee = franchisee
        self.url = url
        self.timezone = pytz.timezone(franchisee.timezone)
        self.auth()

    @abstractmethod
    def auth(self):
        """
        Метод для авторизации в CRM системе и получения токена

        При ошибке во время атворизации генерирует исключение
        AuthenticationFailed (403 Forbidden)
        """
        pass

    @abstractmethod
    def get_users(self) -> List[CrmTeacher]:
        """Метод получения пользователей из CRM системы"""
        pass

    @abstractmethod
    def get_locations(self) -> List[Location]:
        """Получение локаций из CRM системы"""
        pass

    @abstractmethod
    def get_lessons(self) -> List[CrmLocation]:
        """Получение занятий из CRM системы"""
        pass

    @abstractmethod
    def get_regular_lessons(self) -> List[CrmRegularLesson]:
        """Получение регулярных занятий из CRM системы"""
        pass

    @abstractmethod
    def get_groups(self) -> List[CrmGroup]:
        """Получение групп из CRM системы"""
        pass

    def users_synchronization(self) -> Result:
        """Синхронизация пользователей"""
        crm_users = self.get_users()
        # Получаем всех преподавателей для конкертного хоста CRM системы
        users = User.objects.filter(franchisee__crminfo__url=self.franchisee.crminfo.url).select_related()
        created_count = 0
        updated_count = 0
        error_count = 0
        archived_count = 0
        crm_ids = [user.crm_id for user in crm_users]
        crm_ids.append(0)
        for crm_user in crm_users:
            # Получаем пользователя по CRM-id иначе добавляем его
            need_update = False
            try:
                user = users.get(crm_id=crm_user.crm_id)

                # Обновляем данные в модели если требуется
                if user.email != crm_user.email:
                    user.email = crm_user.email
                    need_update = True

                if user.phone != user.phone:
                    user.phone = crm_user.phone
                    need_update = True

                if user.full_name != crm_user.full_name:
                    user.full_name = crm_user.full_name
                    need_update = True

                if user.is_active is False:
                    user.is_active = True
                    need_update = True

                if user.birthday != crm_user.birthday:
                    user.birthday = crm_user.birthday
                    need_update = True

                if user.cities.filter(api_id=self.franchisee.id).count() == 0:
                    user.cities.add(self.franchisee)

                if need_update:
                    user.save()
                    updated_count += 1

            except User.DoesNotExist:
                try:
                    user = User.objects.create(
                        full_name=crm_user.full_name,
                        email=crm_user.email,
                        phone=crm_user.phone,
                        franchisee=self.franchisee,
                        crm_id=crm_user.crm_id,
                        birthday=crm_user.birthday,
                    )
                    user.roles.add(Role.objects.get(name='Преподаватель'))
                    user.cities.add(self.franchisee)
                    created_count += 1
                except Exception as e:
                    logger.info('Пользователь существует', crm_user, e)
                    error_count += 1
            except User.MultipleObjectsReturned as e:
                logger.info('Одинаковые пользователи', crm_user)
                error_count += 1
        # Архивируем не пришедших CRM_ID
        archive_user = users.filter(cities=self.franchisee).exclude(crm_id__in=crm_ids)
        for user in archive_user:
            if user.cities.count() > 1:
                user.cities.remove(self.franchisee)
            else:
                user.is_active = False
                user.save()
            archived_count += 1

        return Result(len(crm_users), updated_count, created_count, error_count, archived_count)

    def locations_synchronization(self) -> Result:
        """Синхронизация локаций"""
        crm_locations = self.get_locations()
        locations = Location.objects.filter(franchisee=self.franchisee).prefetch_related()

        created_count = 0
        updated_count = 0

        crm_ids = [location.crm_id for location in crm_locations]
        crm_ids.append(0)
        for crm_location in crm_locations:
            try:
                location = locations.get(Q(crm_id=crm_location.crm_id) | Q(name=crm_location.name))
                location.name = crm_location.name
                location.is_active = crm_location.is_active
                location.crm_id = crm_location.crm_id
                location.save()
                updated_count += 1
            except Location.DoesNotExist:
                Location.objects.create(
                    name=crm_location.name,
                    crm_id=crm_location.crm_id,
                    franchisee=self.franchisee,
                    is_active=crm_location.is_active,
                )
                created_count += 1
        archived_count = locations.exclude(crm_id__in=crm_ids).update(is_active=False)
        return Result(len(crm_locations), updated_count, created_count, 0, archived_count)

    def lessons_synchronization(self) -> Result:
        """Синхронизация занятий"""
        crm_lessons = self.get_lessons()
        lessons = Lesson.objects.filter(franchisee=self.franchisee)
        return Result(len(crm_lessons), 0, 0, 0, 0)

    def regular_lessons_synchronization(self) -> Result:
        """Синхронизация регулярных занятий"""
        crm_regular_lessons = self.get_regular_lessons()
        # Получаем все группы в Альфа-CRM
        crm_groups = self.get_groups()
        # Преобразуем список кортежей из двух элементов
        # в словарь, где лючом является id группы,
        # а значением название группы
        crm_groups_dict = dict(crm_groups)

        now = self.franchisee.get_localtime()
        # Оставляем только актуальные регулярные занятия
        # те, которые еще не окончены и у которых есть локация
        crm_regular_lessons = list(filter(lambda x: x.date_to > now.date(), crm_regular_lessons))
        crm_regular_lessons = list(filter(lambda x: x.location_id is not None, crm_regular_lessons))

        regular_lessons = RegularLesson.objects.filter(franchisee=self.franchisee)
        days_of_weak = DayOfWeak.objects.all()

        crm_ids = [lesson.id for lesson in crm_regular_lessons]
        crm_ids.append(0)

        created_count = 0
        updated_count = 0
        error_count = 0
        archived_count = 0
        for lesson in crm_regular_lessons:
            # Получаем преподавателей, которые привязаны к регулярному расписанию
            lesson_teachers = User.objects.filter(cities=self.franchisee,
                                                  crm_id__in=lesson.teacher_ids)
            try:
                regular_lesson = regular_lessons.get(crm_id=lesson.id)
                # Если группа не найдена для регулярного занятия,
                # то архивируем регулярное занятие
                if crm_groups_dict.get(lesson.group_id, None) is None:
                    regular_lesson.delete()
                    continue

                need_update = False
                # Обновляем поля, если требуется
                old_regular = Regular(
                    [x for x in regular_lesson.teachers.all()],
                    [x for x in regular_lesson.days_of_weak.all()],
                    regular_lesson.time_start,
                    regular_lesson.time_end,
                    regular_lesson.start_date,
                    regular_lesson.end_date,
                    regular_lesson.location,
                    None,
                )

                if regular_lesson.start_date != lesson.date_from:
                    regular_lesson.start_date = lesson.date_from
                    need_update = True

                if regular_lesson.end_date != lesson.date_to:
                    regular_lesson.end_date = lesson.date_to
                    need_update = True

                if not queryset_is_equal(regular_lesson.teachers.all(),
                                         lesson_teachers):
                    regular_lesson.teachers.clear()
                    regular_lesson.teachers.add(*lesson_teachers)
                    need_update = True

                if regular_lesson.days_of_weak.first().number != lesson.day:
                    regular_lesson.days_of_weak.clear()
                    regular_lesson.days_of_weak.add(DayOfWeak.objects.get(number=lesson.day))
                    need_update = True

                if not time_is_equal(regular_lesson.time_start, lesson.time_from):
                    regular_lesson.time_start = lesson.time_from
                    need_update = True

                if not time_is_equal(regular_lesson.time_end, lesson.time_to):
                    regular_lesson.time_end = lesson.time_to
                    need_update = True

                if regular_lesson.location.crm_id != lesson.location_id:
                    regular_lesson.location = Location.objects.get(crm_id=lesson.location_id)
                    need_update = True

                if lesson.group_id is not None \
                        and regular_lesson.group_name != crm_groups_dict.get(lesson.group_id, None):
                    regular_lesson.group_crm_id = lesson.group_id
                    regular_lesson.group_name = crm_groups_dict.get(lesson.group_id, None)
                    need_update = True

                if need_update:
                    updated_count += 1
                    regular_lesson.update_lessons(old_regular)
                    regular_lesson.save()

            except RegularLesson.DoesNotExist:
                if crm_groups_dict.get(lesson.group_id, None):
                    regular_lesson = RegularLesson.objects.create(
                        start_date=lesson.date_from,
                        end_date=lesson.date_to,
                        location=Location.objects.get(crm_id=lesson.location_id, franchisee=self.franchisee),
                        franchisee=self.franchisee,
                        time_start=lesson.time_from,
                        time_end=lesson.time_to,
                        crm_id=lesson.id,
                    )

                    regular_lesson.teachers.set(lesson_teachers)
                    regular_lesson.days_of_weak.add(days_of_weak.get(number=lesson.day))
                    regular_lesson.save()
                    regular_lesson.create_lessons()
                    created_count += 1
            except Exception as e:
                error_count += 1

        archive_regular_lessons = regular_lessons.exclude(crm_id__in=crm_ids)
        for lesson in archive_regular_lessons:
            lesson.delete()
            archived_count += 1

        return Result(len(crm_regular_lessons), updated_count, created_count, error_count, 0)


class AlphaCRMSynchronization(BaseSynchronization):
    """Класс для синхронизации с Альфа-CRM"""
    AUTH_URL = 'v2api/auth/login'
    TEACHER_URL = 'v2api/teacher'
    LOCATION_URL = 'v2api/location'
    LESSON_URL = 'v2api/lesson'
    REGULAR_LESSON_URL = 'v2api/regular-lesson'
    ROOM_URL = 'v2api/room'
    GROUP_URL = 'v2api/group'

    PER_PAGE = '?per-page=100'
    DATE_FORMAT = '%Y-%m-%d'
    DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
    TIME_FORMAT = '%H:%M'
    DOB_DATE_FORMAT = '%d.%m.%Y'

    headers = {'content-type': 'application/json'}

    def auth(self):
        data = {
            'email': self.user,
            'api_key': self.password,
        }
        response = requests.post(self.url + self.AUTH_URL,
                                 data=json.dumps(data), headers=self.headers)
        # Если не получилось авторизироваться
        if response.status_code == 403:
            raise exceptions.AuthenticationFailed("Возникла ошибка при авторизации в Альфа-CRM")

        token = response.json()['token']
        self.headers['X-ALFACRM-TOKEN'] = token

    def get_users(self) -> List[CrmTeacher]:
        # Получение всех преподавателей
        data = {'page': 0}
        count = -1
        teachers = []
        # Пока количество записей не равно 0
        while count != 0:
            response = requests.post(self.url + self.TEACHER_URL + self.PER_PAGE,
                                     headers=self.headers, data=json.dumps(data))
            result = response.json()
            for item in result['items']:
                phone = item['phone'][0] if item['phone'] else None
                email = item['email'][0] if item['email'] else None
                birthday = item.get('dob', None)
                birthday = datetime.strptime(birthday, self.DOB_DATE_FORMAT).date() if birthday else None
                crm_user = CrmTeacher(item['name'], email, item['id'], phone, birthday)
                teachers.append(crm_user)

            # Переходим на следующую страницу
            data['page'] += 1
            count = result['count']
        return teachers

    def get_lessons(self) -> List[CrmLesson]:
        """Получение всех занятий"""
        data = {'page': 0}
        count = -1
        lessons = []
        while count != 0:
            response = requests.post(self.url + self.LESSON_URL + self.PER_PAGE,
                                     headers=self.headers, data=json.dumps(data))
            result = response.json()
            count = result['count']
            for item in result['items']:
                date = datetime.strptime(item['date'], self.DATE_FORMAT)
                time_from = datetime.strptime(item['time_from'], self.DATETIME_FORMAT)
                time_to = datetime.strptime(item['time_to'], self.DATETIME_FORMAT)
                lessons.append(CrmLesson(
                    item['id'], item['branch_id'], date,
                    item['teacher_ids'], time_from, time_to
                ))
                data['page'] += 1
        return lessons

    def get_locations(self) -> List[CrmLocation]:
        # Получение всех локаций
        data = {'page': 0}
        count = -1
        locations = []
        while count != 0:

            response = requests.post(self.url + self.LOCATION_URL + self.PER_PAGE,
                                     headers=self.headers)
            result = response.json()

            count = result['count']
            if data['page'] != result['page']:
                break
            data['page'] += 1

            for location in result['items']:
                locations.append(CrmLocation(location['name'], location['id'], location['is_active']))
        return locations

    def get_groups(self) -> List[CrmGroup]:
        data = {'page': 0}
        groups = []
        while True:
            response = requests.post(self.url + self.GROUP_URL + self.PER_PAGE,
                                     headers=self.headers, data=json.dumps(data))
            result = response.json()
            if data['page'] != result['page'] or result['count'] == 0:
                break
            data['page'] += 1
            for item in result['items']:
                groups.append(
                    CrmGroup(item['id'], item['name'])
                )
        return groups

    def get_regular_lessons(self) -> List[CrmRegularLesson]:
        data = {'page': 0}
        count = -1
        regular_lessons = []

        rooms_to_locations = self.get_rooms()

        while count != 0:
            response = requests.post(self.url + self.REGULAR_LESSON_URL + self.PER_PAGE,
                                     headers=self.headers, data=json.dumps(data))

            result = response.json()
            count = result['count']
            if data['page'] != result['page']:
                break
            data['page'] += 1
            for item in result['items']:
                time_from = datetime.strptime(item['time_from'], self.TIME_FORMAT)
                time_to = datetime.strptime(item['time_to'], self.TIME_FORMAT)
                date_from = datetime.strptime(item['b_date'], self.DATE_FORMAT).date()
                date_to = datetime.strptime(item['e_date'], self.DATE_FORMAT).date()

                regular_lessons.append(
                    CrmRegularLesson(
                        item['id'],
                        rooms_to_locations[item['room_id']],
                        time_from,
                        time_to,
                        date_from,
                        date_to,
                        item['day'],
                        item['teacher_ids'],
                        item['related_id'] if item['related_class'] == 'Group' else None,
                    )
                )
        return regular_lessons

    def get_group_by_id(self, group_id):
        """Получение группы по ID"""
        data = {
            'id': group_id
        }

        response = requests.post(self.url + self.GROUP_URL, headers=self.headers, data=json.dumps(data))
        result = response.json()
        if result['count'] == 1:
            return result['items'][0]
        else:
            return None

    def get_rooms(self) -> dict:
        data = {'page': 0}
        count = -1
        rooms_to_locations = {None: None}
        while count != 0:
            response = requests.post(self.url + self.ROOM_URL + self.PER_PAGE,
                                     headers=self.headers, data=json.dumps(data))
            result = response.json()

            count = result['count']
            if data['page'] != result['page']:
                break
            data['page'] += 1

            for item in result['items']:
                rooms_to_locations[item['id']] = item['location_id']
        return rooms_to_locations


class ProcessingWebhook(ABC):
    """Абстрактный класс для обработки входящего Webhook от CRM"""

    # Тип события
    CREATE = 0
    UPDATE = 1
    DELETE = 2

    def __init__(self, data: dict, franchisee: Franchisee, crm_info: CRMInfo) -> None:
        self.data = data
        self.franchisee = franchisee
        self.crm_info = crm_info

    def on_location(self):
        """Обработка входящего webhook на филиал"""
        location = self.get_location()
        action = self.get_action()
        if action == self.CREATE:
            # Создаем филиал
            Location.objects.create(
                franchisee=self.franchisee,
                name=location.name,
                crm_id=location.crm_id,
            )
        elif action == self.UPDATE:
            # Обновляем филиал
            try:
                location = Location.objects.get(franchisee=self.franchisee,
                                            crm_id=location.crm_id)
                if location.name is not None:
                    location.name = location.name
                if location.is_active is not None:
                    location.is_active = location.is_active
                location.save()
            except Location.DoesNotExist:
                logger.info('Филиал не найден')

    def on_teacher(self):
        """Обработка входящего webhook на преподавателя"""
        crm_teacher = self.get_teacher()
        action = self.get_action()
        if action == self.CREATE:
            user = User.objects.create(
                full_name=crm_teacher.full_name,
                phone=crm_teacher.phone,
                email=crm_teacher.email,
                franchisee=self.franchisee,
                crm_id=crm_teacher.crm_id,
                birthday=crm_teacher.birthday,
            )
            user.cities.add(self.franchisee)
            user.roles.add(Role.objects.get(name='Преподаватель'))

        elif action == self.UPDATE:
            try:
                user = User.objects.get(crm_id=crm_teacher.crm_id,
                                        cities=self.franchisee)
                if crm_teacher.full_name is not None:
                    user.full_name = crm_teacher.full_name

                if crm_teacher.birthday is not None:
                    user.birthday = crm_teacher.birthday

                if crm_teacher.phone is not None:
                    user.phone = crm_teacher.phone

                if crm_teacher.email is not None:
                    user.email = crm_teacher.email
                user.save()
            except User.DoesNotExist:
                pass
        elif action == self.DELETE:
            try:
                user = User.objects.get(crm_id=crm_teacher.crm_id,
                                        cities=self.franchisee)
                user.is_active = False
                user.save()
            except User.DoesNotExist:
                pass

    def on_lesson(self):
        """Обработка входящего webhook на занятие"""
        crm_lesson = self.get_lesson()
        action = self.get_action()

        if action == self.CREATE:
            # Создаем занятие, если оно не привязано к
            # регулярному занятию
            if crm_lesson.regular_lesson_id is None:

                teachers = User.objects.filter(crm_id__in=crm_lesson.teacher_ids,
                                               cities=self.franchisee)
                lesson = Lesson.objects.create(
                    franchisee=self.franchisee,
                    start=crm_lesson.time_from,
                    end=crm_lesson.time_to,
                    date=crm_lesson.date,
                    location=Location.objects.get(crm_id=crm_lesson.location_id,
                                              franchisee=self.franchisee),
                    crm_id=crm_lesson.id
                )
                lesson.teachers.set(teachers)
                logger.info(f'---\nСоздано разовое занятие {self.franchisee.name}\n'
                            f'{lesson.start.isoformat()} - {lesson.end.isoformat()}\n'
                            f'{lesson.date}\n'
                            )
            # Иначе присваиваем занятию, которое уже есть в системе,
            # ID занятию из CRM системы
            else:
                regular_lesson = RegularLesson.objects.get(crm_id=crm_lesson.regular_lesson_id,
                                                           franchisee=self.franchisee)

                regular_lesson.lesson_set.filter(date=crm_lesson.date).update(crm_id=crm_lesson.id)

                logger.info(f'---\nСоздано занятие, привязанное к регулярному {self.franchisee.name}\n'
                            f'{crm_lesson.time_from.isoformat()} - {crm_lesson.time_to.isoformat()}\n'
                            f'{crm_lesson.date}')

        elif action == self.UPDATE:

            try:
                # Обновляем занятие в системе, если если оно есть в системе
                lesson = Lesson.objects.get(crm_id=crm_lesson.id, franchisee=self.franchisee)

                # Если статус == 2, то архивируем
                if crm_lesson.status == 2:
                    logger.info(f'---\nЗанятие архивировано {self.franchisee.name}\n'
                                f'{lesson.start.isoformat()} - {lesson.end.isoformat()}\n'
                                f'{lesson.date}\n'
                                f'{lesson.location.name}\n'
                                f'Рег: {lesson.regular_lesson.id if lesson.regular_lesson else "-"}'
                                )
                    lesson.delete()

                elif crm_lesson.status == 1 or crm_lesson.status is None:
                    if crm_lesson.teacher_ids is not None:
                        teachers = User.objects.filter(crm_id__in=crm_lesson.teacher_ids,
                                                       cities=self.franchisee)
                        lesson.teachers.set(teachers)

                    if crm_lesson.time_from:
                        lesson.start = crm_lesson.time_from
                    if crm_lesson.time_to:
                        lesson.end = crm_lesson.time_to
                    if crm_lesson.location_id:
                        lesson.location.id = crm_lesson.location_id
                    if crm_lesson.date:
                        lesson.date = crm_lesson.date
                    lesson.restore()

                    lesson.save()
                    if crm_lesson.status == 1:
                        title = 'Занятие проведено'
                    else:
                        title = 'Занятие отредактировано'
                    logger.info(f'---\n'
                                f'{title} {self.franchisee.name}\n'
                                f'{lesson.start.isoformat()} - {lesson.end.isoformat()}\n'
                                f'{lesson.date}\n'
                                f'{lesson.location.name}\n'
                                f'Рег: {lesson.regular_lesson.id if lesson.regular_lesson else "-"}\n'
                                )
                else:
                    logger.info(f'---\n'
                                f'Занятие проведено {self.franchisee.name}\n'
                                f'{lesson.start.isoformat()} - {lesson.end.isoformat()}\n'
                                f'{lesson.date}\n'
                                )

            except Lesson.DoesNotExist:
                logger.info(f'---\n'
                            f'Занятие не найдено {self.franchisee.name}')
                raise AttributeError('Занятие не найдено')

        elif action == self.DELETE:
            try:
                # Если находим занятие с этми ID,
                # то архивируем его
                Lesson.objects.get(crm_id=crm_lesson.id, franchisee=self.franchisee).delete()
            except Lesson.DoesNotExist:
                logger.info('Занятие для удаления не найдено в системе')

    def on_regular_lesson(self):
        """Обработка входящего webhook на регуляное занятие"""
        regular_lesson = self.get_regular_lesson()
        action = self.get_action()
        if action == self.CREATE:
            # Создем регулярное занятие, при условии, что указан филиал
            if regular_lesson.location_id:
                teachers = User.objects.filter(crm_id__in=regular_lesson.teacher_ids, cities=self.franchisee)
                location = Location.objects.get(crm_id=regular_lesson.location_id, franchisee=self.franchisee)
                day = DayOfWeak.objects.get(number=regular_lesson.day)
                lesson = RegularLesson.objects.create(
                    start_date=regular_lesson.date_from,
                    end_date=regular_lesson.date_to,
                    location=location,
                    franchisee=self.franchisee,
                    time_start=regular_lesson.time_from,
                    time_end=regular_lesson.time_to,
                    crm_id=regular_lesson.id,
                    group_crm_id=regular_lesson.group_id[0],
                    group_name=regular_lesson.group_id[1],
                )
                lesson.teachers.set(teachers)
                for teacher in teachers:
                    Notification.create_tomorrow_teacher_notification(teacher, self.franchisee)
                    Notification.create_today_teacher_notification(teacher, self.franchisee)

                lesson.days_of_weak.add(day)
                lesson.create_lessons()

        elif action == self.UPDATE:
            # Сохраняем информацию об обновляемом занятии
            lesson = RegularLesson.objects.get(crm_id=regular_lesson.id,
                                               franchisee=self.franchisee)
            old_regular = Regular(
                [x for x in lesson.teachers.all()],
                [x for x in lesson.days_of_weak.all()],
                lesson.time_start,
                lesson.time_end,
                lesson.start_date,
                lesson.end_date,
                lesson.location,
                None
            )
            # Обновляем нужные поля
            if regular_lesson.teacher_ids is not None:
                teachers = User.objects.filter(crm_id__in=regular_lesson.teacher_ids,
                                               cities=self.franchisee)
                lesson.teachers.set(teachers)

            if regular_lesson.day:
                day_of_weak = DayOfWeak.objects.filter(number=regular_lesson.day)
                lesson.days_of_weak.set(day_of_weak)

            if regular_lesson.time_from:
                lesson.time_start = regular_lesson.time_from

            if regular_lesson.time_to:
                lesson.time_end = regular_lesson.time_to

            if regular_lesson.date_from:
                lesson.start_date = regular_lesson.date_from

            if regular_lesson.date_to:
                lesson.end_date = regular_lesson.date_to

            if regular_lesson.location_id:
                lesson.location = Location.objects.get(crm_id=regular_lesson.location_id,
                                                   franchisee=self.franchisee)

            lesson.save()
            lesson.update_lessons(old_regular)

        elif action == self.DELETE:
            # Архивируем регулярное занятие
            lesson = RegularLesson.objects.get(crm_id=regular_lesson.id,
                                               franchisee=self.franchisee)
            lesson.delete()

    @abstractmethod
    def get_location(self) -> CrmLocation:
        pass

    @abstractmethod
    def get_teacher(self) -> CrmTeacher:
        pass

    @abstractmethod
    def get_lesson(self) -> CrmLesson:
        pass

    @abstractmethod
    def get_regular_lesson(self) -> CrmRegularLesson:
        pass

    @abstractmethod
    def get_action(self) -> int:
        pass


class AlphaCRMProcessingWebhook(ProcessingWebhook):
    """Класс для обработки входящего Webhook от Альфа-CRM"""

    DATE_FORMAT = '%Y-%m-%d'
    DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
    TIME_FORMAT = '%H:%M'

    def get_action(self) -> int:

        event = self.data.get('event', None)
        if not event:
            raise AttributeError('Нет поля event')
        # Извлекаем поле event из запроса
        if event == 'update':
            return self.UPDATE
        elif event == 'create':
            return self.CREATE
        elif event == 'delete':
            return self.DELETE

    def get_location(self) -> CrmLocation:
        new_fields = self.data['fields_new']

        # Получаем нужные поля
        name = new_fields.get('name', None)
        is_active = new_fields.get('is_active', None)
        crm_id = self.data['entity_id']

        return CrmLocation(name, crm_id, is_active)

    def get_teacher(self):
        new_fields = self.data['fields_new']

        # Извлекаем поля
        crm_id = self.data.get('entity_id')

        email = None
        phone = None

        if new_fields:
            name = new_fields.get('name', None)
            birthday = new_fields.get('dob', None)
            contacts = new_fields.get('contact_contacts', [])

            birthday = datetime.strptime(birthday, self.DATE_FORMAT) if birthday else birthday

            # Разбираем все контакты
            for contact in contacts:
                if not contact:
                    email = ''
                    phone = ''
                elif '@' in contact:
                    email = contact
                else:
                    contact = re.sub(r'[()-]', "", contact)
                    if contact[1:].isdigit():
                        phone = contact
        else:
            name = None
            birthday = None

        return CrmTeacher(name, email, crm_id, phone, birthday)

    def get_lesson(self) -> CrmLesson:
        new_fields = self.data['fields_new']
        # Извлекаем поля
        crm_id = self.data.get('entity_id', None)

        if new_fields:
            time_from = new_fields.get('time_from', None)
            time_to = new_fields.get('time_to', None)
            date = new_fields.get('lesson_date', None)
            teacher_ids = new_fields.get('teacher_ids', None)
            regular_lesson_id = new_fields.get('regular_id', None)
            room_id = new_fields.get('room_id', None)
            status = new_fields.get('status', None)

            # Переводим дату и время в объекты Python
            time_from = datetime.strptime(time_from, self.DATETIME_FORMAT).time() if time_from else None
            time_to = datetime.strptime(time_to, self.DATETIME_FORMAT).time() if time_to else None
            date = datetime.strptime(date, self.DATE_FORMAT).date() if date else None
            location_id = self.room_to_location(int(room_id)) if room_id else None
            status = int(status) if status else None
        else:
            time_from = None
            time_to = None
            date = None
            teacher_ids = None
            regular_lesson_id = None
            location_id = None
            status = None

        return CrmLesson(crm_id, location_id, date, teacher_ids,
                         time_from, time_to, regular_lesson_id, status)

    def room_to_location(self, room_id):
        sync = AlphaCRMSynchronization(self.crm_info.email, self.crm_info.api_key,
                                       self.franchisee, self.crm_info.url)
        rooms = sync.get_rooms()
        return rooms[room_id]

    def group_id_to_name(self, group_id):
        """Получение название группы по ID"""
        sync = AlphaCRMSynchronization(self.crm_info.email, self.crm_info.api_key,
                                       self.franchisee, self.crm_info.url)
        group = sync.get_group_by_id(group_id)
        if group:
            return group['name']
        else:
            return ''

    def get_regular_lesson(self):
        new_fields = self.data['fields_new']
        # Извлекаем поля
        crm_id = self.data.get('entity_id', None)

        if new_fields:
            time_from = new_fields.get('time_from', None)
            time_to = new_fields.get('time_to', None)
            date_from = new_fields.get('b_date', None)
            date_to = new_fields.get('e_date', None)
            teacher_ids = new_fields.get('teacher_ids', None)
            room_id = new_fields.get('room_id', None)
            group_id = new_fields.get('related_id', 0) if new_fields.get('related_class', None) == 'Group' else 0

            # Переводим дату и время в объекты Python
            time_from = datetime.strptime(time_from, self.DATETIME_FORMAT) if time_from else None
            time_to = datetime.strptime(time_to, self.DATETIME_FORMAT).time() if time_to else None
            date_from = datetime.strptime(date_from, self.DATE_FORMAT).date() if date_from else None
            date_to = datetime.strptime(date_to, self.DATE_FORMAT).date() if date_to else None
            day = time_from.day if time_from else None
            time_from = time_from.time() if time_from else None
            location_id = self.room_to_location(int(room_id)) if room_id else None
            group_name = self.group_id_to_name(group_id)
        else:
            time_from = None
            time_to = None
            date_from = None
            date_to = None
            teacher_ids = None
            location_id = None
            day = None
            group_id = None
            group_name = None

        return CrmRegularLesson(crm_id, location_id, time_from, time_to,
                                date_from, date_to, day, teacher_ids, (group_id, group_name))
