from itertools import chain
from django.db.models import QuerySet
from datetime import time


def time_is_equal(time1: time, time2: time) -> bool:
    """
    Функция для определения равенства дух объектов time
    :param time1:
    :param time2:
    :return:
    """
    if time1.hour == time2.hour and time1.minute == time2.minute:
        return True
    else:
        return False


def queryset_is_equal(queryset1: QuerySet, queryset2: QuerySet) -> bool:
    """
    Проверка, что два queryset Равны
    :param queryset1:
    :param queryset2:
    :return:
    """
    return list(queryset1.values_list('id', flat=True)) == list(queryset2.values_list('id', flat=True))
