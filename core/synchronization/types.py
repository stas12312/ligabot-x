from collections import namedtuple

CrmTeacher = namedtuple('CrmUser', ['full_name', 'email', 'crm_id', 'phone', 'birthday'])
CrmLocation = namedtuple('CrmLocation', ['name', 'crm_id', 'is_active'])
CrmLesson = namedtuple('CrmLesson', ['id', 'location_id', 'date', 'teacher_ids',
                                     'time_from', 'time_to', 'regular_lesson_id',
                                     'status'])
CrmRegularLesson = namedtuple('CrmRegularLesson',
                              ['id', 'location_id', 'time_from', 'time_to',
                               'date_from', 'date_to', 'day', 'teacher_ids',
                               'group_id'])

CrmGroup = namedtuple('CrmGroup', ['id', 'name'])

Result = namedtuple('Result', ['total', 'updated', 'created', 'errors', 'archived'])

Regular = namedtuple('Regular', [
    'teachers',
    'days',
    'time_start',
    'time_end',
    'start_date',
    'end_date',
    'location',
    'name',
])
