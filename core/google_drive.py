"""
Модуль для работы с Google Drive
"""
import os
from dateutil.parser import parse
from collections import namedtuple

from django.conf import settings
from google.auth.transport import Response
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build, Resource
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseUpload

from bot.models import GoogleInfo

UploadResult = namedtuple('UploadResult', ['folder_url', 'status'])


class GoogleDrive:
    """Класс для удобной работы с загрузкой фото на Google диск"""
    # Необходимые права для досутпа к Google диск
    SCOPES = ['https://www.googleapis.com/auth/drive']
    # URI для callback от Google после авторизации
    redirect_uri = settings.API_HOST + '/api/bot/v1/integrations/google-drive/callback/'

    # Путь к файлу
    path = os.path.join(settings.BASE_DIR, 'credentials.json')

    def __init__(self, credentials_dict: dict = None, credentials: Credentials = None,
                 google_info: GoogleInfo = None):
        """
        Конструктор класса
        :param credentials_dict: Данные для авторизации в виде словаря
        :param credentials: Данные для аторизации
        :param google_info: Объект с инфррмацией об интеграции
        """
        self.google_info = google_info
        if credentials_dict:
            self.credentials = self.dict_to_credentials(credentials_dict)
        elif credentials:
            self.credentials = credentials
        else:
            raise AttributeError()

        self.service = self.get_service(self.credentials, google_info)

    @classmethod
    def get_flow(cls, scopes=None, cred_path=None, redirect_uri=None) -> InstalledAppFlow:
        """Создание объекта flow для работы с OAuth"""
        cred_path = cred_path if cred_path else cls.path
        scopes = scopes if scopes else cls.SCOPES
        redirect_uri = redirect_uri if redirect_uri else cls.redirect_uri

        return InstalledAppFlow.from_client_secrets_file(
            cred_path,
            scopes,
            redirect_uri=cls.redirect_uri,
        )

    @staticmethod
    def credentials_to_dict(credentials: Credentials) -> dict:
        """
        Преобразование Credentials в dict
        :param credentials: Данные для авторизации пользователя
        и работы с Google API
        :return: Словарь с данными об авторизации пользователя
        """
        return {
            'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes,
            'expiry': credentials.expiry.isoformat() if credentials.expiry else None

        }

    @staticmethod
    def dict_to_credentials(data: dict) -> Credentials:
        """
        Преобразование Credentials в dict
        :param data: Данные для авторизации пользователя в dict
        и работы с Google API
        :return: Credentials с данными об авторизации пользователя
        """
        # Извлекаем из словаря время отзыва токена
        # и преобразовываем его в дату
        expiry = data.pop('expiry', None)

        if expiry:
            expiry = parse(expiry)

        credentials = Credentials(**data)

        # Добавляем время отзыва токена
        credentials.expiry = expiry
        return credentials

    @staticmethod
    def get_service(credentials: Credentials, google_info: GoogleInfo = None):
        """
        Создание сервиса для работы с Google API
        :param credentials: Данные пользователя для авторизации и работы с Google API
        :param google_info: Объект с информацией об интеграции
        :return: Resource объект для работы с Google API
        """
        # Обновляем токен при необходимости
        if credentials and credentials.expired and credentials.refresh_token or credentials.expiry is None:
            credentials.refresh(Request())
            if google_info:
                google_info.credentials = GoogleDrive.credentials_to_dict(credentials)
                google_info.save()

        return build('drive', 'v3', credentials=credentials, cache_discovery=False)

    def get_about(self):
        """
        Получение информации об аккаунте
        :return: Информация об Google аккаунте
        """
        return self.service.about().get(fields='storageQuota').execute()

    def upload_file_on_disk(self, files: list,
                            path: str, root_folder_id: str) -> UploadResult:
        """Загрузка фотографий на Google диск
        :param files: Список файлов для загрузки на Google диск
        [(Файловый объек фото, Имя файла)]
        :param path: Путь, куда поместить файл
        {
            date: 'Дата проведения занятия'
            teacher: 'ФИО преподавателя'
            location: 'Локация где проводилось занятие'
        }
        :param root_folder_id: Индентификатор корневой папки
        :return:

        Файлы размещаются на Google диске по следующему формату

         -> Корневая папка (root_folder)
         -> Дата проведения занятия
         -> Фотографии с занятия (Именем файла является ФИО преподавателя)
        """
        # Разбиваем путь на папки
        folders = [x for x in path.split('/') if x]

        current_folder_id = root_folder_id
        for folder in folders:
            find_folder = self._check_file(folder, current_folder_id)
            if find_folder:
                current_folder_id = find_folder['id']
            else:
                create_folder = self._create_folder(folder, current_folder_id)
                current_folder_id = create_folder['id']

        folder_url = 'https://drive.google.com/drive/folders/' + current_folder_id

        for file, file_name, file_info in files:
            mime_type = file_info.mime_type

            file_metadata = {
                'name': file_name,
                'parents': [current_folder_id],
            }

            media_body = MediaIoBaseUpload(file, mimetype=mime_type, resumable=True)
            self.service.files().create(body=file_metadata, media_body=media_body,
                                        fields='id, parents, name').execute()
        self.google_info.credentials = self.credentials_to_dict(self.credentials)
        self.google_info.save()
        return UploadResult(folder_url, 'ok')

    def _create_folder(self, name: str, parent_id: str):
        """
        Создание папки
        :param name: Имя папки
        :param parent_id: Идентификатор родительской папки
        """
        file_metadata = {
            'name': name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [parent_id],
        }

        # Возвращаем созданную папку
        try:
            return self.service.files().create(body=file_metadata, fields='id, parents').execute()
        except HttpError:
            return None

    def _check_file(self, file_name: str, parent_folder_id: str):
        """
        Проверка существования файла с именем file_name
        """
        try:
            result = self.service.files().list(
                pageSize=20, fields="nextPageToken, files(id, name, mimeType, parents)",
                q=f'"{parent_folder_id}" in parents and name="{file_name}" and trashed = false'
            ).execute()
            files = result['files']
            if files:
                return files[0]
            else:
                return None
        except HttpError:
            return None

    def _get_files_in_folder(self, folder_id: str) -> list:
        """
        Получение фалов из директории с folder_id
        :param folder_id: Идентификатор папки, из которой получаем файлы
        :return: Список полученных файлов
        """

        page_token = None
        all_files = []
        while True:
            param = {}
            if page_token:
                param['pageToken'] = page_token
            result = self.service.files().list(
                pageSize=20, fields="nextPageToken, files(id, name, mimeType, parents)",
                q=f"'{folder_id}' in parents", **param).execute()
            all_files += result['files']
            page_token = result.get('nextPageToken')
            if page_token is None:
                break
        return all_files

    def _delete_file(self, file_id):
        try:
            self.service.files().delete(fileId=file_id).execute()
            return True
        except HttpError:
            return None

    def folder_exist(self, folder_id: str) -> bool:
        """Проверка на сущестования директории или прав доступа на неё"""
        try:
            # Создаем и удаляем файла
            # для определения досутпа к папке
            file = self.service.files().get(fileId=folder_id).execute()
            return True
        except HttpError as e:
            return False
