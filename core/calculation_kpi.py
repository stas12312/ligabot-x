"""
Модуль для формирование и вычисления KPI
"""
from datetime import datetime, timedelta

from bot.models import Franchisee, User, Preparation, Meeting, Lesson, LessonLog


def get_kpi_for_city(start: datetime.date, end: datetime.date, user: User, city: Franchisee):
    """
    Получение KPI для пользователя

    Учитываются только регулярные занятия
    которые указаны как влияющие на KPI

    На KPI влияют следующие отчеты:
    - Собрания
    - Подготовки
    - Сортировки
    - Фото с занятий

    KPI за сортировки начисляется в зависимости от параметра
    need_sorting_count у настройки города


    :param start: Начало интервала для вычисления KPI
    :param end: Конец интервала для вычисления KPI
    :param user: Пользователь для которого строится KPI
    :param city: Город, для которого строится KPI
    :return:


    """

    # Начало интервала поиска отчетов меняем на начало понедельника
    # Это нужно для корректного построения KPI
    start_reports = start - timedelta(days=start.weekday())
    # Минимальный KPI для каждой категории
    min_category_kpi = 0.5

    # Отчет формируем для каждой недели
    weak_reports_start = start_reports
    # Начало и конец недели для поиска занятий
    weak_start = start
    weak_end = weak_start + timedelta(days=6)

    kpi_data = {
        'weaks': []
    }

    while weak_end <= end:
        # Получаем все типы отчетов для каждой недели в интервале
        preparations = Preparation.objects.filter(teacher=user,
                                                  timestamp__date__gte=weak_reports_start,
                                                  timestamp__date__lte=weak_end)
        # Получаем регулярные занятие, в рамках указанной недели
        lessons = Lesson.objects.filter(date__gte=weak_start, date__lte=weak_end)\
            .exclude(regular_lesson__is_null=False)

        # Если найдены занятия, то заносим отметку в результирующий словарь
        if lessons.exists():
            pass
