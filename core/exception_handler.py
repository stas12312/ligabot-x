from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    """Кастомный обработчик исключения"""
    response = exception_handler(exc, context)

    if response is not None:
        detail = response.data.get('detail', None)
        if detail:
            response.data['message'] = detail
        else:
            response.data['message'] = 'Некорректное заполнение полей'
    return response
