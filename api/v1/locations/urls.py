from django.urls import path

from . import views

urlpatterns = [
    path('', views.LocationListView.as_view()),
    path('<int:pk>/', views.LocationRetrieveUpdateDestroyView.as_view())
]
