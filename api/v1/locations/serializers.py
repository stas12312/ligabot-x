from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.users.serializers import UserShortSerializer
from bot.models import Location


class LocationShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'name')


class LocationSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = Location
        fields = '__all__'


class LocationDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    lessons = serializers.SerializerMethodField('regular_lessons')
    inspectors = UserShortSerializer(many=True)

    def regular_lessons(self, obj: Location):
        from api.v1.regular_lessons.serializers import RegularLessonDetailSerializer
        # Получение списка регулярных занятий для филиала
        regular_lessons = obj.regularlesson_set.filter(is_active=True)
        serializer = RegularLessonDetailSerializer(regular_lessons, many=True)
        return serializer.data

    class Meta:
        model = Location
        fields = ('franchisee', 'id', 'name', 'is_active', 'crm_id', 'inspectors', 'lessons')
