from rest_framework import generics
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.core.mixins import UpdateModelMixin
from api.core.paginations import LargeResultsSetPagination
from bot.models import Location
from .serializers import LocationSerializer, LocationDetailSerializer


class LocationListView(generics.ListCreateAPIView):
    serializer_class = LocationSerializer
    pagination_class = LargeResultsSetPagination
    queryset = Location.objects.filter(is_active=True)
    filter_backends = [IsFranchiseeFilterBackend]

    def list(self, request, *args, **kwargs):
        self.serializer_class = LocationDetailSerializer
        return super(LocationListView, self).list(request, args, kwargs)

    def create(self, request, *args, **kwargs):
        # Получаем имя филиала из запроса
        name = request.data['name']
        try:
            # Проверяем, есть ли филиал с таким названием в архиве
            # если есть, то делаем его активным
            location = Location.objects.get(name=name, franchisee=request.franchisee)
            location.is_active = True
            location.save()
            return Response(self.serializer_class(location).data)
        except Location.DoesNotExist:
            return super(LocationListView, self).create(request, args, kwargs)


class LocationRetrieveUpdateDestroyView(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LocationDetailSerializer
    queryset = Location.objects.all()
    update_serializer_class = LocationSerializer
    filter_backends = [IsFranchiseeFilterBackend]

    def delete(self, request, *args, **kwargs):
        location = self.get_object()
        location.is_active = False
        location.save()
        return Response({'status': f'Филиал {location.id} удалён'})
