from collections import namedtuple

Regular = namedtuple('Regular', [
    'teachers',
    'days',
    'time_start',
    'time_end',
    'start_date',
    'end_date',
    'location',
    'name',
])
