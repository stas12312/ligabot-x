from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend, ActiveFilterBackend
from api.core.mixins import CreateModelMixin, UpdateModelMixin
from bot.models import RegularLesson
from .serializers import RegularLessonDetailSerializer, RegularLessonSerializer
from ..lessons.filters import LessonFilterBackend


class RegularLessonListView(CreateModelMixin, generics.ListCreateAPIView):
    queryset = RegularLesson.objects.all()
    serializer_class = RegularLessonDetailSerializer
    create_serializer_class = RegularLessonSerializer
    filter_backends = [IsFranchiseeFilterBackend, ActiveFilterBackend,
                       LessonFilterBackend, DjangoFilterBackend]
    filterset_fields = ['location']


class RegularLessonUpdateDestroyView(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = RegularLesson.objects.all()
    serializer_class = RegularLessonDetailSerializer
    update_serializer_class = RegularLessonSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class RegularLessonActionView(viewsets.ModelViewSet):
    queryset = RegularLesson.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]

    @action(detail=True, methods=['get'])
    def restore(self, request, pk):
        object_ = RegularLesson.objects.get(id=pk)
        object_.restore()
        return Response({'detail': 'Регулярное расписание восстановлено'})
