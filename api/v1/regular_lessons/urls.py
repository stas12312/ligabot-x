from django.urls import path

from . import views

urlpatterns = [
    path('', views.RegularLessonListView.as_view()),
    path('<int:pk>/', views.RegularLessonUpdateDestroyView.as_view()),
    path('<int:pk>/restore/', views.RegularLessonActionView.as_view({'get': 'restore'})),
]
