from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.locations.serializers import LocationShortSerializer
from api.v1.days_of_weak.serializers import DaySerializer
from api.v1.users.serializers import UserShortSerializer
from bot.models import RegularLesson
from .types import Regular


class RegularLessonShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegularLesson
        fields = ['id', 'comment', 'start_date', 'end_date', 'group_name']


class RegularLessonSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    group_crm_id = serializers.HiddenField(default=0)

    class Meta:
        model = RegularLesson
        fields = '__all__'

    def create(self, validated_data):
        instance = super(RegularLessonSerializer, self).create(validated_data)
        instance.create_lessons()
        return instance

    def update(self, instance, validated_data):
        """Обновление записи"""
        old_regular = Regular(
            [x for x in instance.teachers.all()],
            [x for x in instance.days_of_weak.all()],
            instance.time_start,
            instance.time_end,
            instance.start_date,
            instance.end_date,
            instance.location,
            None,
        )
        instance = super(RegularLessonSerializer, self).update(instance, validated_data)
        instance.update_lessons(old_regular)
        return instance


class RegularLessonDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    teachers = UserShortSerializer(many=True)
    location = LocationShortSerializer()
    days_of_weak = DaySerializer(many=True)
    time_start = serializers.TimeField(format="%H:%M")
    time_end = serializers.TimeField(format="%H:%M")

    class Meta:
        model = RegularLesson
        fields = '__all__'
