from rest_framework import serializers

from bot.models import DayOfWeak


class DaySerializer(serializers.ModelSerializer):
    class Meta:
        model = DayOfWeak
        fields = ['full_name', 'short_name', 'number']
        read_only_fields = fields
