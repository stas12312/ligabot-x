from rest_framework import generics

from bot.models import DayOfWeak
from .serializers import DaySerializer


class DaysOfWeakListView(generics.ListAPIView):
    queryset = DayOfWeak.objects.all()
    serializer_class = DaySerializer
