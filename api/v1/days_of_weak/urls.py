from django.urls import path

from . import views

urlpatterns = [
    path('', views.DaysOfWeakListView.as_view()),
]
