class CurrentFranchiseeDefault:
    """
    Класс для получение города, который делает запрос к API
    """
    franchisee = None

    def set_context(self, serializer_field):
        print(serializer_field)
        self.franchisee = serializer_field.context['request'].franchisee

    def __call__(self):
        return self.franchisee

    def __repr__(self):
        return '%s()' % self.__class__.__name__