from django.urls import include, path

urlpatterns = [
    path('sortings/', include('api.v1.reports.sortings.urls')),
    path('photos/', include('api.v1.reports.photos.urls')),
]
