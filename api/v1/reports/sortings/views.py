import calendar

from dateutil.parser import parse
from django.db.models import Q
from django.utils import timezone
from rest_framework import generics, exceptions
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.v1.locations.serializers import LocationShortSerializer
from api.v1.journals.sortings.serializers import SortingsStatisticSerializer
from api.v1.users.serializers import UserShortSerializer
from bot.models import Location, User, SortingLog
from bot import utils


class StatisticSortingView(generics.RetrieveAPIView):
    queryset = SortingLog.objects.filter(status__in=[0, 1]).select_related()
    serializer_class = SortingsStatisticSerializer
    filter_backends = [IsFranchiseeFilterBackend]

    def get_active_objects(self, queryset):
        queryset = queryset.filter(is_active=True)
        return IsFranchiseeFilterBackend().filter_queryset(self.request, queryset, self)

    def get_interval(self):
        now = utils.get_localtime(timezone_=self.request.timezone)
        start = now.replace(day=1)
        _, end_day = calendar.monthrange(start.year, start.month)
        end = start.replace(day=end_day)
        timezone_ = timezone.get_current_timezone()
        if self.request.GET.get('start'):
            start = timezone_.localize(parse(self.request.GET.get('start')))
            start = timezone.localtime(start, timezone.get_current_timezone())
        if self.request.GET.get('end'):
            end = timezone_.localize(parse(self.request.GET.get('end')))
            end = timezone.localtime(end, timezone.get_current_timezone())
        start = start.replace(hour=0, minute=0, second=0, microsecond=0)
        end = end.replace(hour=23, minute=59, second=59, microsecond=99999)

        return start, end

    def get_count_interval(self):
        """Получение интервала по количеству сортировок"""
        min_count = 0
        max_count = 9999999999

        if self.request.GET.get('min_count'):
            min_count = int(self.request.GET.get('min_count'))
        if self.request.GET.get('max_count'):
            max_count = int(self.request.GET.get('max_count'))
        return min_count, max_count

    def get(self, request, *args, **kwargs):
        sortings = self.filter_queryset(self.get_queryset())
        mode = kwargs['mode']
        if mode == 'on-locations':
            objects = self.get_active_objects(Location.objects.all())
            object_serializer_class = LocationShortSerializer
            title = 'location'
        elif mode == 'on-teachers':
            objects = self.get_active_objects(User.objects.filter(roles__name='Преподаватель'))
            object_serializer_class = UserShortSerializer
            title = 'teacher'
        else:
            raise exceptions.MethodNotAllowed(mode)
        # Получаем интервал и применяем его на сортировки
        start, end = self.get_interval()
        sortings = sortings.filter(timestamp__range=(start, end))
        # Формируем ответ
        data = {
            'start': start.date().isoformat(),
            'end': end.date().isoformat()
        }
        results = []
        min_count, max_count = self.get_count_interval()
        for object_ in objects:
            if mode == 'on-teachers':
                query = Q(teacher=object_)
            elif mode == 'on-locations':
                query = Q(location=object_)
            else:
                raise exceptions.MethodNotAllowed(mode)

            sortings_for_object = sortings.filter(query)
            if max_count >= sortings_for_object.count() >= min_count:
                object_statistic = {
                    'count': sortings_for_object.count(),
                    'sortings': self.serializer_class(sortings_for_object, many=True).data,
                    title: object_serializer_class(object_).data

                }
                results.append(object_statistic)
        results = sorted(results, key=lambda x: x['count'], reverse=True)
        data['results'] = results
        return Response(data)
