from django.urls import path

from . import views

urlpatterns = [
    path('<str:mode>/', views.StatisticSortingView.as_view()),
]
