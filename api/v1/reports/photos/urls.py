from django.urls import path

from . import views

urlpatterns = [
    path('', views.PhotosReportView.as_view()),
]