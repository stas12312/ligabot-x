from dateutil.parser import isoparse
from rest_framework import views
from rest_framework.response import Response

from api.v1.users.serializers import UserShortSerializer
from bot.models import User


def get_teacher_ids(teachers):
    """Получение илентификаторов преподавателей из queryset"""
    return teachers.values_list('id', flat=True)


class PhotosReportView(views.APIView):
    """Построение отчета по фотографиям с занятий"""

    def get(self, request, *args, **kwargs):
        # Получаем выбранный инетрвал
        start = request.query_params.get('start', None)
        end = request.query_params.get('end', None)
        city = request.franchisee

        # Преобразуем строковые даты к Python объектам
        start = isoparse(start) if start else city.get_localtime().replace(day=1)
        end = isoparse(end) if end else city.get_localtime()

        # Получаем преподавателей, занятия за выбранный период
        # и очеты за выбранный период
        teachers = User.objects.filter(franchisee=city, is_active=True, roles__name='Преподаватель')

        data = {
            'start': start.isoformat(),
            'end': end.isoformat(),
            'results': [],
        }
        # Строим отчет по преподавателям
        teachers_data = []
        for teacher in teachers:
            # Для каждого преподавателя на список занятий
            # накладываем запись из журнала, если она есть
            lessons_data = []
            for lesson in teacher.lesson_set.filter(
                    date__gte=start.date(), date__lte=end.date(),
                    regular_lesson__isnull=False, is_active=True).prefetch_related('photolog_set', 'photolog_set__teacher'):

                lesson_data = {
                    'date': lesson.date.isoformat(),
                    'start': lesson.start.isoformat(),
                    'end': lesson.end.isoformat(),
                    'group': lesson.group_name,
                    'entry': None,
                }
                for entry in lesson.photolog_set.all():
                    if entry.teacher == teacher:
                        lesson_data['entry'] = {
                            'created': entry.created.isoformat()
                        }

                lessons_data.append(lesson_data)
            teachers_data.append({
                'teacher': {'id': teacher.id, "full_name": teacher.full_name},
                'lessons': lessons_data
            })
        data['results'] = teachers_data
        return Response(data)
