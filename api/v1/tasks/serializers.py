from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.departments.serializers import DepartmentShortSerializer
from api.v1.users.serializers import UserDetailSerializer, UserShortSerializer
from bot.models import Task


class TaskDetailSerializers(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    director = UserDetailSerializer()
    executor = UserShortSerializer()
    department = DepartmentShortSerializer()
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        return {
            'number': obj.status,
            'text': obj.get_status_display()
        }

    class Meta:
        model = Task
        fields = '__all__'
