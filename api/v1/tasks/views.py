from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, filters

from api.core.filters import IsFranchiseeFilterBackend
from bot.models import Task
from .filters import TaskFilterBackend
from .serializers import TaskDetailSerializers


class TaskListView(generics.ListCreateAPIView):
    queryset = Task.objects.filter(department__isnull=False)
    serializer_class = TaskDetailSerializers

    filter_backends = [IsFranchiseeFilterBackend, TaskFilterBackend,
                       DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['franchisee', 'id', 'status', 'department', 'executor', 'director']
    ordering_fields = ['created', 'deadline']
