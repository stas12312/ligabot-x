from django.utils import timezone
from rest_framework import filters


class TaskFilterBackend(filters.BaseFilterBackend):
    """Фильтр для раздела задач"""

    def filter_queryset(self, request, queryset, view):
        now = timezone.localtime(timezone.now())

        is_overdue = request.query_params.get('is_overdue')
        if is_overdue == 'true':
            queryset = queryset.filter(deadline__lt=now.date()).exclude(status=3)
        elif is_overdue == 'false':
            pass

        return queryset
