from django.urls import path

from . import views

urlpatterns = [
    path('', views.LessonListView.as_view()),
    path('<int:pk>/', views.LessonRetrieveUpdateDestroyView.as_view()),
    path('<int:pk>/restore/', views.LessonActionView.as_view({'get': 'restore'})),
]
