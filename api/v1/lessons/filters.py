from datetime import timedelta

from dateutil.parser import parse
from django.core import exceptions
from django.utils import timezone
from rest_framework import filters


class ActiveLessonsFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        active = request.query_params.get('active')
        active = active.lower() if active else None

        if active == 'false' or active == '0':
            return queryset.filter(is_active=False, is_parent_archive=False)
        elif active == 'all' or active == '-1':
            return queryset.all()
        else:
            return queryset.filter(is_active=True)


class LessonFilterBackend(filters.BaseFilterBackend):
    """Фильтр для занятий"""

    def filter_queryset(self, request, queryset, view):
        """
        Фильры для раздела занятий

        - Преподаватель
        - Интервал проведения занятий
        - Филиал

        :param request:
        :param queryset:
        :param view:
        :return:
        """

        # Извлекаем строковые параметры
        location = request.query_params.get('location')
        start = request.query_params.get('start')
        end = request.query_params.get('end')
        teacher = request.query_params.get('teacher')
        day_of_weak = request.query_params.get('day_of_weak')
        active = request.query_params.get('active')
        active = active.lower() if active else None

        if active == 'false' or active == '0':
            if hasattr(queryset.model, 'is_parent_archive'):
                queryset = queryset.filter(is_active=False, is_parent_archive=False)
            else:
                queryset = queryset.filter(is_active=False)
        elif active == 'all' or active == '-1':
            queryset = queryset.all()
        else:
            queryset = queryset.filter(is_active=True)

        if location:
            queryset = queryset.filter(location__id=int(location))
        if teacher:
            queryset = queryset.filter(teachers__id=int(teacher))
        if start:
            # Парсим дату и применяем фильтр
            start = parse(start)
            queryset = queryset.filter(date__gte=start)
        elif active != 'false':
            # Показываем актуальные занятия
            start = timezone.localtime(timezone.now()) - timedelta(days=1)
            try:
                queryset = queryset.filter(date__gte=start.date())
            except exceptions.FieldError:
                pass
        if end:
            end = parse(end)
            queryset = queryset.filter(date__lte=end)

        if day_of_weak:
            queryset = queryset.filter(days_of_weak=day_of_weak)
        return queryset
