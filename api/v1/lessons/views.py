from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.core.mixins import CreateModelMixin, UpdateModelMixin
from bot.models import Lesson
from .filters import LessonFilterBackend, ActiveLessonsFilterBackend
from .serializers import LessonDetailSerializer, LessonSerializer


class LessonListView(CreateModelMixin, generics.ListCreateAPIView):
    serializer_class = LessonDetailSerializer
    create_serializer_class = LessonSerializer
    queryset = Lesson.objects.filter()
    filter_backends = [IsFranchiseeFilterBackend, ActiveLessonsFilterBackend, LessonFilterBackend]


class LessonRetrieveUpdateDestroyView(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LessonDetailSerializer
    queryset = Lesson.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]
    update_serializer_class = LessonSerializer


class LessonActionView(viewsets.ModelViewSet):
    queryset = Lesson.objects.all()

    @action(detail=True, methods=['get'])
    def restore(self, request, pk):
        """Восстановление занятие"""
        object_ = self.queryset.get(id=pk)
        object_.restore()
        return Response({'detail': 'Занятие восстановлено'})
