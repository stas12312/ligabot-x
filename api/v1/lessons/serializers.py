from datetime import timedelta
from itertools import chain

from django.utils import timezone
from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.locations.serializers import LocationShortSerializer
from api.v1.days_of_weak.serializers import DaySerializer
from api.v1.regular_lessons.serializers import RegularLessonShortSerializer
from api.v1.users.serializers import UserShortSerializer
from bot.models import Lesson, Notification, DayOfWeak


class LessonSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    start = serializers.TimeField(format="%H:%M")
    end = serializers.TimeField(format="%H:%M")
    regular_lesson = RegularLessonShortSerializer(required=False)

    class Meta:
        model = Lesson
        fields = '__all__'

    def create(self, validated_data):
        instance = super(LessonSerializer, self).create(validated_data)
        # Переформировываем уведомления для указанных преподавателей
        # если занятие сегодняшней датой

        for teacher in instance.teachers.all():
            Notification.create_today_teacher_notification(teacher, instance.franchisee)
            Notification.create_tomorrow_teacher_notification(teacher, instance.franchisee)

        return instance

    def update(self, instance, validated_data):
        old_teachers = instance.teachers.all()

        new_instance = super(LessonSerializer, self).update(instance, validated_data)
        new_teachers = new_instance.teachers.all()

        for teacher in chain(old_teachers, new_teachers):
            Notification.create_today_teacher_notification(teacher, instance.franchisee)
            Notification.create_tomorrow_teacher_notification(teacher, instance.franchisee)

        return new_instance


class LessonDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    start = serializers.TimeField(format="%H:%M")
    end = serializers.TimeField(format="%H:%M")
    teachers = UserShortSerializer(many=True)
    location = LocationShortSerializer()
    regular_lesson = RegularLessonShortSerializer()
    day = serializers.SerializerMethodField('day_of_weak')

    def day_of_weak(self, obj):
        day = obj.date.weekday() + 1
        day_of_weak = DayOfWeak.objects.get(id=day)
        serializer = DaySerializer(day_of_weak)
        return serializer.data

    class Meta:
        model = Lesson
        fields = '__all__'
        read_only_fields = ('franchisee', 'start', 'end',
                            'teachers', 'location', 'regular_lesson', 'day',
                            'name', 'crm_id', 'is_active', 'date')
        depth = 1
