from rest_framework import filters


class ExpandedUserFilterBackend(filters.BaseFilterBackend):
    """Дополнительный фильтр для пользователей Telegram бота"""

    def filter_queryset(self, request, queryset, view):
        is_auth = request.query_params.get('is_auth')
        role = request.query_params.get('role')
        is_auth = is_auth.lower() if is_auth else None
        if role:
            queryset = queryset.filter(roles=int(role))

        if is_auth in ['true', '1']:
            return queryset.exclude(chat_id=0)
        elif is_auth in ['false', '0']:
            return queryset.filter(chat_id=0)
        return queryset.all()


class UserCitiesBackend(filters.BaseFilterBackend):
    """
    Фильтр для получения пользователей,
    привязанных к городу, который делает запрос к API
    """
    def filter_queryset(self, request, queryset, view):
        franchisee = request.franchisee
        return queryset.filter(cities=franchisee)