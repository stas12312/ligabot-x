import requests
from django.conf import settings
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from api.core.filters import ActiveFilterBackend
from api.core.mixins import CreateModelMixin, UpdateModelMixin
from api.core.paginations import LargeResultsSetPagination
from bot.models import User, Department, Token
from .filters import UserCitiesBackend, ExpandedUserFilterBackend
from .serializers import UserSerializer, UserShortSerializer, UserDetailSerializer


class UserListCreateView(CreateModelMixin, generics.ListCreateAPIView):
    queryset = User.objects.all().order_by('full_name')
    filterset_fields = ['franchisee', 'is_active', 'id']
    serializer_class = UserDetailSerializer
    create_serializer_class = UserSerializer
    filter_backends = [UserCitiesBackend, ActiveFilterBackend,
                       ExpandedUserFilterBackend, DjangoFilterBackend]


class UserRetrieveUpdateDestroyView(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    update_serializer_class = UserSerializer
    filter_backends = [UserCitiesBackend]

    def delete(self, request, *args, **kwargs):
        user = self.get_object()
        user.is_active = False
        user.save()
        # Удаляем пользователя из отдела
        for department in Department.objects.filter(franchisee=user.franchisee, employees=user):
            department.employees.remove(user)
        # Удаляем пользователя из непрошедших занятий и регулярных
        return Response({'message': 'Преподаватель архивирован'})


class UserActionViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter()

    @action(detail=True, methods=['get'])
    def get_token(self, request, pk):
        user = self.get_object()
        token = Token.objects.get(user=user)
        invite_link = f'https://t.me/{settings.BOT_NAME}?start={token.value}'
        return Response({'token': token.value, 'invite': invite_link})

    @action(detail=True, methods=['get'])
    def restore(self, request, pk):
        user = self.get_object()
        user.is_active = True
        user.save()
        return Response({'detail': 'Пользователь восстановлен'})


class SendInviteAction(APIView):
    title = 'Приглашение в TelegramBot Лиги Роботов'
    invite_message = (
        '<p>Привет, {name}</p>'
        '<p>Для того, чтобы начать пользоваться TelegramBot\'ом Лиги Роботов '
        'перейдите по следующей <a href=\'{invite_link}\'>ссылке</a> или перейдите в '
        'TelegramBot\'а <b>{bot_name}</b> и отправьте данный ключ {invite_key}</p>'
    )
    send_email_url = settings.API_HOST + '/api/system/mails/create'
    bot_name = settings.BOT_NAME

    def get_invite_link(self, user):
        return f'https://t.me/{self.bot_name}?start={user.token.value}'

    def get(self, request, pk):

        user = User.objects.get(id=pk)
        if user.chat_id != 0:
            return Response({'message': 'Данный пользователь уже авторизован'}, status=400)
        else:
            if not user.email:
                return Response({'message': 'У пользователя отсутствует email'}, status=400)
            message = self.invite_message.format(
                invite_link=self.get_invite_link(user),
                bot_name=settings.BOT_NAME,
                invite_key=user.token.value,
                name=user.full_name
            )

            headers = {
                'Authorization': 'Bearer ' + request.token
            }

            data = {
                'title': self.title,
                'text': message,
                'recipient': user.email
            }

            response = requests.post(self.send_email_url, headers=headers, data=data)
            if response.status_code == 201:
                message = f'Приглашение для пользователя {user.full_name} успешно отправлено'
            else:
                message = f'При отправке приглашения пользователю {user.full_name} произошла ошибка'

            return Response({'message': message}, response.status_code)


class UserAllListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserShortSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = [UserCitiesBackend, ExpandedUserFilterBackend]


class TeacherListView(generics.ListAPIView):
    queryset = User.objects.filter(roles__name='Преподаватель', is_active=True)
    serializer_class = UserShortSerializer
    filter_backends = [UserCitiesBackend, ExpandedUserFilterBackend]
    pagination_class = LargeResultsSetPagination


class EmployeeListView(generics.ListAPIView):
    queryset = User.objects.filter(roles__name='Офисный сотрудник', is_active=True)
    serializer_class = UserShortSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = [UserCitiesBackend, ExpandedUserFilterBackend]
