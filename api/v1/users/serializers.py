from rest_framework import serializers

from bot.models import User, Role
from api.v1.utils import CurrentFranchiseeDefault
from api.v1.cities.serializers import FranchiseeSerializer
from api.v1.roles.serializers import RoleSerializer


class UserSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    is_active = serializers.BooleanField(default=True)

    class Meta:
        model = User
        fields = '__all__'


class UserDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    is_active = serializers.BooleanField(default=True)
    roles = RoleSerializer(many=True)
    cities = FranchiseeSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'full_name', 'phone', 'email', 'roles',
                  'chat_id', 'is_active', 'franchisee', 'cities',
                  'delta_id', 'crm_id', 'birthday']
        read_only_fields = fields


class UserShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name']
        read_only_fields = fields
