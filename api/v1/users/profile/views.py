from dateutil.parser import parse
from django.core.validators import ValidationError
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.v1.journals.cabinets.serializers import LessonLogSerializer
from api.v1.journals.orders.serializers import DetailOrderSerializer
from api.v1.journals.photos.serializers import PhotoLogSerializer
from api.v1.journals.preparations.serializers import PreparationDetailSerializer
from api.v1.journals.sortings.serializers import SortingLogSerializer
from api.v1.users.serializers import UserDetailSerializer
from bot.models import User, SortingLog, PhotoLog, Preparation, LessonLog, DetailsOrder


class ProfileView(viewsets.ViewSet):
    request = None
    user = None
    date_from = None
    date_to = None

    def _get_interval(self):
        now = self.user.franchisee.get_localtime()
        date_from = self.request.query_params.get('date_from')
        date_to = self.request.query_params.get('date_to')

        # Если не передан инетрвал, устанавливаем его сами
        self.date_from = parse(date_from).date() if date_from else now.replace(day=1).date()
        self.date_to = parse(date_to).date() if date_to else now.date()

    def _get_sortings_data(self):
        """Получение данных о сортировах"""
        sortings = SortingLog.objects.filter(teacher=self.user,
                                             timestamp__date__gte=self.date_from,
                                             timestamp__date__lte=self.date_to)
        return SortingLogSerializer(sortings, many=True).data

    def _get_photos_data(self):
        """Получение данных о фотографиях с занятий"""
        photos = PhotoLog.objects.filter(teacher=self.user,
                                         lesson_date__gte=self.date_from,
                                         lesson_date__lte=self.date_to).order_by('-lesson_date')
        return PhotoLogSerializer(photos, many=True).data

    def _get_cabinets_data(self):
        """Получение данных о фотографиях кабинетов"""
        cabinets = LessonLog.objects.filter(teacher=self.user,
                                            timestamp__date__gte=self.date_from,
                                            timestamp__date__lte=self.date_to)
        return LessonLogSerializer(cabinets, many=True).data

    def _get_orders_data(self):
        """Получение данных о сортировках"""
        orders = DetailsOrder.objects.filter(teacher=self.user,
                                             created__date__gte=self.date_from,
                                             created__date__lte=self.date_to)\
            .exclude(info={})
        return DetailOrderSerializer(orders, many=True).data

    def _get_preparations_data(self):
        """Получение данных о подготовках"""
        preparations = Preparation.objects.filter(teacher=self.user,
                                                  timestamp__date__gte=self.date_from,
                                                  timestamp__date__lte=self.date_to)

        return PreparationDetailSerializer(preparations, many=True).data

    def _get_all_data(self):
        """Получение всез типов отчетов"""
        sortings_data = self._get_sortings_data()
        photos_data = self._get_photos_data()
        cabinets_data = self._get_cabinets_data()
        orders_data = self._get_orders_data()
        preparations_data = self._get_preparations_data()
        user_data = self._get_user_data()
        # Собераем все данные в один dict
        data = {
            'user': user_data,
            'sortings': sortings_data,
            'photos': photos_data,
            'cabinets': cabinets_data,
            'orders': orders_data,
            'preparations': preparations_data,
        }
        return data

    def _get_user_data(self):
        """Получение данных о пользователе"""
        return UserDetailSerializer(self.user).data

    def _get_interval_data(self):
        """Получение информации об интервале"""

        return {'date_from': self.date_from.isoformat(), 'date_to': self.date_to.isoformat()}

    @action(detail=True, methods=['get'])
    def public_profile(self, request, token):
        """Получение профиля пользователя с помощью токена пользователя бота"""
        self.request = request
        try:
            self.user = User.objects.get(profile_token=token)
        except User.DoesNotExist:
            return Response({'message': 'Пользователь не найден'}, status=404)
        except ValidationError:
            return Response({'message': 'Некорректный токен'}, status=404)

        self._get_interval()
        reports_data = self._get_all_data()
        interval_data = self._get_interval_data()
        data = {
            **interval_data,
            'results': reports_data
        }

        return Response(data)

    @action(detail=True, methods=['get'])
    def admin_profile(self, request, pk):
        """Получение профиля пользователя с помощью токена Delta-CRM"""
        self.request = request
        self._get_interval()
        reports_data = self._get_all_data()
        interval_data = self._get_interval_data()
        data = {
            **interval_data,
            'results': reports_data
        }

        return Response(data)
