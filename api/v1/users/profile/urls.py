from django.urls import path

from . import views

urlpatterns = [
    path('', views.ProfileView.as_view({'get': 'admin_profile'})),
    path('public/<str:token>/', views.ProfileView.as_view({'get': 'public_profile'})),

]