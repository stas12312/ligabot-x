from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.UserListCreateView.as_view()),
    path('<int:pk>/', views.UserRetrieveUpdateDestroyView.as_view()),
    path('<int:pk>/token/', views.UserActionViewSet.as_view({'get': 'get_token'})),
    path('<int:pk>/restore/', views.UserActionViewSet.as_view({'get': 'restore'})),
    path('<int:pk>/send-invite/', views.SendInviteAction.as_view()),
    path('<int:pk>/profile/', include('api.v1.users.profile.urls')),
    path('profile/', include('api.v1.users.profile.urls')),

    path('list/', views.UserAllListView.as_view()),
    path('teachers/', views.TeacherListView.as_view()),
    path('employees/', views.EmployeeListView.as_view()),
]
