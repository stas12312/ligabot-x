from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.users.serializers import UserShortSerializer
from bot.models import Department


class DepartmentDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    employees = UserShortSerializer(many=True, required=False)

    class Meta:
        model = Department
        fields = '__all__'


class DepartmentSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = Department
        fields = '__all__'


class DepartmentShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ['id', 'name']
        read_only_fields = fields
