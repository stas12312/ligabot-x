from django.urls import path

from . import views

urlpatterns = [
    path('', views.DepartmentsListView.as_view()),
    path('<int:pk>/', views.DepartmentUpdateDestroyView.as_view()),
    path('list/', views.DepartmentAllListView.as_view()),
]
