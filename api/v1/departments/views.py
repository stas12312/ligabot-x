from rest_framework import generics

from api.core.filters import IsFranchiseeFilterBackend
from api.core.mixins import CreateModelMixin, UpdateModelMixin
from api.core.paginations import LargeResultsSetPagination
from bot.models import Department
from .serializers import DepartmentDetailSerializer, DepartmentSerializer, DepartmentShortSerializer


class DepartmentsListView(CreateModelMixin, generics.ListCreateAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentDetailSerializer
    filter_backends = [IsFranchiseeFilterBackend]
    create_serializer_class = DepartmentSerializer


class DepartmentUpdateDestroyView(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentDetailSerializer
    filter_backends = [IsFranchiseeFilterBackend]
    update_serializer_class = DepartmentSerializer


class DepartmentAllListView(generics.ListAPIView):
    serializer_class = DepartmentShortSerializer
    queryset = Department.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]
    pagination_class = LargeResultsSetPagination
