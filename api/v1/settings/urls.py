from django.urls import path

from . import views
urlpatterns = [
    path('', views.CitySettingList.as_view()),
    path('<str:alias>/', views.CitySettingList.as_view())
]