from rest_framework import viewsets, generics
from rest_framework.response import Response
from settings.models import DefaultSetting


class CitySettingList(generics.GenericAPIView):
    """Модель для получение и редактирования настройки"""

    def get(self, request):
        city = request.franchisee
        # Получаем настройки для города
        settings = DefaultSetting.get_all_settings(city)
        return Response(settings)

    def post(self, request, alias):
        city = request.franchisee
        value = request.data['value']
        try:
            setting = DefaultSetting.set_setting(alias, city, value)
            return Response(setting)
        except ValueError as e:
            return Response({'message': e}, status=400)
