from django.db import transaction
from django.db.models import F
from rest_framework import exceptions
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.core.mixins import CreateModelMixin, UpdateModelMixin
from api.core.paginations import LargeResultsSetPagination
from bot.models import Reference
from .serializers import ReferenceSerializer, ReferenceDetailSerializer


class ReferenceListCreateView(CreateModelMixin, generics.ListCreateAPIView):
    queryset = Reference.objects.all()
    serializer_class = ReferenceDetailSerializer
    create_serializer_class = ReferenceSerializer
    filter_backends = [IsFranchiseeFilterBackend]
    pagination_class = LargeResultsSetPagination


class ReferenceUpdateDestroyReviev(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = Reference.objects.all()
    serializer_class = ReferenceDetailSerializer
    update_serializer_class = ReferenceSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class ReferenceActioModelView(viewsets.ModelViewSet):
    queryset = Reference.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]

    @action(detail=True, methods=['post'])
    def set_position(self, request, pk):
        """Изменение позиции в списке для заметки"""
        note = self.get_object()
        old_position = note.position
        new_position = request.data.get('position')
        notes = self.filter_queryset(self.get_queryset())
        if new_position is None:
            raise exceptions.NotFound('Не передана позиция элемента')

        new_position = int(new_position)

        if new_position < 0 or new_position >= notes.count():
            raise exceptions.APIException('Некорректная позиция', code=400)
        with transaction.atomic():
            if old_position > new_position:
                notes.filter(position__range=(new_position, old_position)).update(position=F('position') + 1)
            else:
                notes.filter(position__range=(old_position, new_position)).update(position=F('position') - 1)
            note.position = new_position
            note.save()

        return Response(ReferenceDetailSerializer(self.filter_queryset(self.get_queryset()), many=True).data)
