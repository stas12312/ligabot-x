from django.urls import path

from . import views

urlpatterns = [
    path('', views.ReferenceListCreateView.as_view()),
    path('<int:pk>/', views.ReferenceUpdateDestroyReviev.as_view()),
    path('<int:pk>/set-position/', views.ReferenceActioModelView.as_view({'post': 'set_position'}))
]
