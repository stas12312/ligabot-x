from django.core.validators import URLValidator
from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from bot.models import Reference


class ReferenceSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = Reference
        fields = ['franchisee', 'title', 'body', 'type']

    def validate(self, data):
        # Если типом заметки является ссылка
        # то проверяем корректность содержимого
        if data['type'] == 0:
            validate = URLValidator()
            validate(data['body'])
        return data


class ReferenceDetailSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        return {
            'number': obj.type,
            'text': obj.get_type_display()}

    class Meta:
        model = Reference
        fields = ['id', 'title', 'body', 'updated', 'type', 'position']
        read_only_fields = fields
