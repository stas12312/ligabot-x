from rest_framework import serializers

from bot.models import Franchisee


class FranchiseeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='pk')

    class Meta:
        model = Franchisee
        fields = ['name', 'id']
        read_only_fields = fields
