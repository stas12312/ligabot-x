from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.CRMInfoListView.as_view()),
    path('<int:franchisee_id>/webhook/<str:api_key>/', views.ProcessingCRMWebhookView.as_view()),
    path('sync/all/', views.SyncAllCRM.as_view())
]
