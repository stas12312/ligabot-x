from django.core.cache import cache
from rest_framework import generics, exceptions
from rest_framework.response import Response
from rest_framework.views import APIView

from api.core.filters import IsFranchiseeFilterBackend
from api.core.tasks import sync_data, proc_webhook
from bot.models import CRMInfo
from core.synchronization import sync
from .serializers import CRMInfoSerializer


class CRMInfoListView(generics.GenericAPIView):
    queryset = CRMInfo.objects.all()
    serializer_class = CRMInfoSerializer
    filter_backends = [IsFranchiseeFilterBackend]

    def get(self, request, *args, **kwargs):
        franchisee = request.franchisee
        try:
            crm_info = CRMInfo.objects.get(franchisee=franchisee)
            serializer = self.serializer_class(crm_info)
            return Response(serializer.data)
        except CRMInfo.DoesNotExist:
            return Response({'status': 0}, status=200)

    def delete(self, request, *args, **kwargs):
        franchisee = request.franchisee
        try:
            CRMInfo.objects.get(franchisee=franchisee).delete()
            return Response({'message': 'Данные о CRM удалены'})
        except CRMInfo.DoesNotExist:
            return Response({'message': 'Данные о CRM не установлены'}, status=404)

    def post(self, request, *args, **kwargs):
        franchisee = request.franchisee
        try:
            CRMInfo.objects.get(franchisee=franchisee)
            return Response({'message': 'Перед добавлением необходимо удалить данные о CRM'}, status=400)
        except CRMInfo.DoesNotExist:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            crm_info = serializer.initial_data
            # Дополняем URL слэшем, если он отсутствует
            if crm_info['url'][-1] != '/':
                crm_info['url'] += '/'
            # Выдаст исключение, если неверные данные для авторизации в CRM
            sync.AlphaCRMSynchronization(crm_info['email'], crm_info['api_key'],
                                         franchisee, crm_info['url'])
            serializer.save()
            return Response(serializer.data)


class ProcessingCRMWebhookView(APIView):
    """Обработка входящего webhook от CRM"""

    def post(self, request, franchisee_id, api_key):
        try:
            crm_info = CRMInfo.objects.get(franchisee_id=franchisee_id, api_key=api_key)
        except CRMInfo.DoesNotExist:
            raise exceptions.NotFound('Неверные данные для webhook')

        proc_webhook.delay(franchisee_id, request.data)
        return Response(request.data)


class SyncAllCRM(APIView):

    def get(self, request):
        franchisee = request.franchisee
        try:
            crm_info = CRMInfo.objects.get(franchisee=franchisee)
        except CRMInfo.DoesNotExist:
            return Response({'message': 'Не подключена интеграция'}, status=404)

        if cache.get(f'sync_{franchisee.id}') is None:
            sync_data.delay(franchisee.id, 'all')
            now = franchisee.get_localtime()
            crm_info.info['all'] = now.isoformat()
            crm_info.save()
            return Response(
                {
                    'message': 'Поставлена задача на синхронизацию с CRM системой',
                    'sync_time': now.isoformat(),
                }
            )
        else:
            return Response(
                {'message': 'Дождитесь окончания предыдущей синхронизации'},
                status=400
            )
