from django.conf import settings
from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from bot.models import CRMInfo


class CRMInfoSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    status = serializers.SerializerMethodField()
    webhook_url = serializers.SerializerMethodField()

    def get_status(self, obj):
        return 1

    def get_webhook_url(self, obj):
        return f'{settings.API_HOST}/api/bot/integrations/crm/{obj.franchisee.id}/webhook/{obj.api_key}/'

    class Meta:
        model = CRMInfo
        fields = '__all__'
