from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from bot.models import GoogleInfo


class GoogleInfoSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = GoogleInfo
        fields = ['folder', 'is_active', 'franchisee']
