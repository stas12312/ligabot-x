from django.conf import settings
from django.core.cache import cache
from django.shortcuts import redirect
from rest_framework import generics, viewsets
from rest_framework.response import Response

from api.v1.integrations.google_drive.serializers import GoogleInfoSerializer
from bot.models import GoogleInfo
from core.google_drive import GoogleDrive

measures = ['б', 'КБ', 'МБ', 'ГБ', 'ТБ']


def get_measures(value):
    """Перевод байтов в читаемое значение"""
    m = 0
    while value > 512:
        value /= 1024
        m += 1

    return round(value, 2), measures[m]


class GoogleInfoView(generics.GenericAPIView):
    serializer_class = GoogleInfoSerializer
    queryset = GoogleInfo.objects.all()

    def get(self, request, *args, **kwargs):
        franchisee = request.franchisee

        try:
            google_info = GoogleInfo.objects.get(franchisee=franchisee)
            serializer = self.serializer_class(google_info)
            data = serializer.data
            data['status'] = 1
            google_drive = GoogleDrive(credentials_dict=google_info.credentials)
            about = google_drive.get_about()
            storage_quota = about['storageQuota']
            limit = get_measures(int(storage_quota['limit']))
            usage = get_measures(int(storage_quota['usage']))
            available = get_measures(int(storage_quota['limit']) - int(storage_quota['usage']))
            data['limit'] = {
                'value': limit[0],
                'measures': limit[1],
                'raw': int(storage_quota['limit']),
            }
            data['usage'] = {
                'value': usage[0],
                'measures': usage[1],
                'raw': int(storage_quota['usage']),
            }
            data['available'] = {
                'value': available[0],
                'measures': available[1],
                'raw': int(storage_quota['limit']) - int(storage_quota['usage'])

            }
            return Response(data)
        except GoogleInfo.DoesNotExist:
            flow = GoogleDrive.get_flow()
            url = flow.authorization_url(prompt='consent')
            cache.set(f'google_{url[1]}', franchisee)
            return Response({'status': 0, 'url': url[0]})

    def put(self, request, *args, **kwargs):
        franchisee = request.franchisee
        try:
            google_info = GoogleInfo.objects.get(franchisee=franchisee)
            folder = request.data.get('folder', None)
            is_active = request.data.get('is_active', None)
            if folder:
                google_info.folder = folder
                google_info.is_active = True

                # Проверяем, что указана корректная папка
                google_drive = GoogleDrive(credentials_dict=google_info.credentials)
                if not google_drive.folder_exist(folder):
                    return Response({'message': 'Не удалось получить доступ к указанной папке'}, status=400)

            # Если передан параметр is_active
            # и установлена папка, то можем включать/выключать
            if is_active is not None and google_info.folder:
                google_info.is_active = is_active
            elif is_active is not None and is_active and not google_info.folder:
                return Response({'message': 'Не указан идентификатор папки'}, status=400)

            google_info.save()
            data = self.serializer_class(google_info).data
            data['message'] = 'Информация обновлена'
            return Response(data)
        except GoogleInfo.DoesNotExist:
            return Response({'message': 'Интеграция с Google не найдена'}, status=404)

    def delete(self, request, *args, **kwargs):
        franchisee = request.franchisee
        try:
            GoogleInfo.objects.get(franchisee=franchisee).delete()
            return Response({'message': 'Интеграция с Google диском удалена'})
        except GoogleInfo.DoesNotExist:
            return Response({'message': 'Интеграция с Google не найдена'})


class GoogleInfoActionView(viewsets.ViewSet):

    def set_integrations(self, request):
        state = request.GET.get('state')
        franchisee = cache.get(f'google_{state}')
        flow = GoogleDrive.get_flow()
        google_response = settings.API_HOST + request.get_full_path()
        flow.fetch_token(authorization_response=google_response)
        credentials = flow.credentials

        # Данные для авторизации в dict
        cred_dict = GoogleDrive.credentials_to_dict(credentials)
        GoogleInfo.objects.create(
            franchisee=franchisee,
            credentials=cred_dict,
            folder='',
            is_active=False,
        )
        return redirect(settings.API_HOST + '/integrations/ligabot')
