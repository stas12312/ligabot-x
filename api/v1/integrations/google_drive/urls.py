from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.GoogleInfoView.as_view()),
    path('callback/', views.GoogleInfoActionView.as_view({'get': 'set_integrations'})),
]
