from django.urls import path, include

urlpatterns = [
    path('crm/', include('api.v1.integrations.crm.urls')),
    path('google-drive/', include('api.v1.integrations.google_drive.urls')),
]
