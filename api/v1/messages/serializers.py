from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.users.serializers import UserShortSerializer
from api.v1.groups.users.serializers import GroupNameSerializer
from bot.models import Message, User, UserGroup, TelegramGroup


class MessageShortSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = Message
        fields = ['receivers', 'text', 'sending_datetime', 'is_sent', 'franchisee', 'created', 'type']

    def validate_receivers(self, receivers):
        return receivers


class MessageSerializer(serializers.ModelSerializer):
    receivers = serializers.SerializerMethodField()

    def get_receivers(self, obj):
        """Получение списка получателей,
        в зависимости от типа сообщения"""
        if obj.type == 0:
            users = User.objects.filter(id__in=obj.receivers)
            data = UserShortSerializer(users, many=True).data
        elif obj.type == 1:
            groups = UserGroup.objects.filter(id__in=obj.receivers)
            data = GroupNameSerializer(groups, many=True).data
        elif obj.type == 2:
            groups = TelegramGroup.objects.filter(id__in=obj.receivers)
            data = GroupNameSerializer(groups, many=True).data
        else:
            data = {}
        return data

    class Meta:
        model = Message
        fields = ['id', 'receivers', 'text', 'sending_datetime', 'is_sent', 'franchisee', 'created']
