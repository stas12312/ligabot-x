from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, views
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from bot.tasks import send_notifications
from bot.models import Message
from .serializers import MessageSerializer, MessageShortSerializer


class MessageListCreateView(generics.ListCreateAPIView):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    filter_backends = [IsFranchiseeFilterBackend, DjangoFilterBackend]
    filterset_fields = ['is_sent']

    def post(self, request, *args, **kwargs):
        kwargs['context'] = self.get_serializer_context()
        request.data['created'] = request.franchisee.get_localtime()
        if not request.data.get('sending_datetime'):
            request.data['sending_datetime'] = request.data['created']
            schedule = False
        else:
            schedule = True

        serializer = MessageShortSerializer(data=request.data, **kwargs)
        serializer.is_valid(raise_exception=True)
        model = serializer.save()

        # Если передано время отправки
        if schedule:
            response = {'message': 'Сообщение запланировано'}
        # Иначе сразу отправляем сообщение
        else:
            send_notifications.delay()
            response = {'message': 'Сообщения отправляются'}

        return Response(response)


class MessageDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Message.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]

    def put(self, request, pk, *args, **kwargs):
        """
        Редактирование сообщения
        Возможно только для запланированных сообщения
        """
        kwargs['context'] = self.get_serializer_context()
        message = self.get_object()
        if message.is_sent:
            return Response({'message': 'Невозможно редактировать отправленное сообщение'}, status=400)

        if not request.data.get('sending_datetime'):
            del (request.data['sending_datetime'])

        serializer = MessageShortSerializer(message, data=request.data, partial=True, **kwargs)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        detail_serializer = MessageSerializer(instance)
        return Response(detail_serializer.data)
