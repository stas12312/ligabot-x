from django.utils import timezone
from django.utils.dateparse import parse_datetime
from rest_framework import generics, viewsets, exceptions
from rest_framework.decorators import action
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from bot.models import Meeting, MeetingDate
from .serializers import MeetingSerializer, MeetingDetailSerializer


class MeetingListCreateView(generics.ListCreateAPIView):
    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer
    filter_backends = [IsFranchiseeFilterBackend]

    def list(self, request, *args, **kwargs):
        self.serializer_class = MeetingDetailSerializer
        return super(MeetingListCreateView, self).list(args, kwargs)


class MeetingsUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class MeetingActionView(viewsets.ModelViewSet):
    queryset = Meeting.objects.all()

    @action(detail=True, methods=['post'])
    def add_date_meeting(self, request, pk):
        """
        Добавление даты проведения собрания
        :param request:
        :param pk:
        :return:
        """
        meeting = self.get_object()
        date = parse_datetime(request.data['date'])
        if date is None:
            raise exceptions.ValidationError(detail='Неверный формат даты проведения')
        date = timezone.make_aware(date)
        MeetingDate(meeting=meeting, datetime_carried=date).save()
        serializer = MeetingDetailSerializer(meeting)
        return Response(serializer.data)

    @action(detail=True, methods=['delete', 'get'])
    def delete_date_meeting(self, request, pk, date):
        """
        Удаление даты проведения занятия
        :param request:
        :param pk:
        :param date:
        :return:
        """
        meeting = self.get_object()
        try:
            date_meeting = MeetingDate.objects.get(id=date)
            if date_meeting.meeting != meeting:
                raise exceptions.PermissionDenied(detail='Дата не принадлежит данному собранию')
            date_meeting.delete()
        except MeetingDate.DoesNotExist:
            raise exceptions.NotFound(detail='Даты не существует')
        serializer = MeetingDetailSerializer(meeting)
        return Response(serializer.data)
