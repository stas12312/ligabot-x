from django.urls import path

from . import views

urlpatterns = [
    path('', views.MeetingListCreateView.as_view()),
    path('<int:pk>/', views.MeetingsUpdateDestroyView.as_view()),
    path('<int:pk>/add_date/', views.MeetingActionView.as_view({'post': 'add_date_meeting'})),
    path('<int:pk>/delete_date/<int:date>/',
         views.MeetingActionView.as_view({'delete': 'delete_date_meeting',
                                          'get': 'delete_date_meeting'})),
]
