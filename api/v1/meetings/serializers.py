from rest_framework import serializers

from api.core.utils import CurrentFranchiseeDefault
from api.v1.users.serializers import UserShortSerializer
from bot.models import Meeting, MeetingDate


class MeetingSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = Meeting
        fields = '__all__'


class MeetingDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeetingDate
        fields = ['id', 'datetime_carried']


#    datetime_carried = DateTimeFieldWihTZ()


class MeetingDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    teachers = UserShortSerializer(many=True)
    dates = MeetingDateSerializer(source='meetingdate_set', many=True)

    class Meta:
        model = Meeting
        fields = '__all__'
