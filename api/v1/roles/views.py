from rest_framework import generics

from .serializers import RoleSerializer
from bot.models import Role


class RolesListView(generics.ListAPIView):
    serializer_class = RoleSerializer
    queryset = Role.objects.all()
