from rest_framework import serializers

from bot.models import Role


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['id', 'name']


class RoleNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['name']
