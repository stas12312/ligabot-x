"""
Модуль для передачи запросу нужному модулю в рамках API V1
"""
from django.urls import path, include


urlpatterns = [
    path('users/', include('api.v1.users.urls')),
    path('locations/', include('api.v1.locations.urls')),
    path('days/', include('api.v1.days_of_weak.urls')),
    path('departments/', include('api.v1.departments.urls')),
    path('integrations/', include('api.v1.integrations.urls')),
    path('lessons/', include('api.v1.lessons.urls')),
    path('regular-lessons/', include('api.v1.regular_lessons.urls')),
    path('journals/', include('api.v1.journals.urls')),
    path('roles/', include('api.v1.roles.urls')),
    path('reports/', include('api.v1.reports.urls')),
    path('tasks/', include('api.v1.tasks.urls')),
    path('meetings/', include('api.v1.meetings.urls')),
    path('messages/', include('api.v1.messages.urls')),
    path('notes/', include('api.v1.notes.urls')),
    path('settings/', include('api.v1.settings.urls')),
    path('tg-groups/', include('api.v1.groups.telegram.urls')),
    path('groups/', include('api.v1.groups.urls')),
    path('dashboards/', include('api.v1.dashboards.urls'))
]
