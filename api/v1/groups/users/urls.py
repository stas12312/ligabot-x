from django.urls import path

from . import views
from api.v1.groups.views import NotificationListView
urlpatterns = [
    path('', views.GroupListCreateView.as_view()),
    path('list/', views.GroupListView.as_view()),
    path('<int:pk>/', views.GroupReviewUpdateDestroyView.as_view()),

    path('notifications/', NotificationListView.as_view()),
]
