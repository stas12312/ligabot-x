from rest_framework import generics

from api.core.filters import IsFranchiseeFilterBackend
from api.core.mixins import CreateModelMixin, UpdateModelMixin
from api.core.paginations import LargeResultsSetPagination
from bot.models import UserGroup
from .serializers import GroupSerializer, GroupDetailSerializer, GroupNameSerializer


class GroupListCreateView(CreateModelMixin, generics.ListCreateAPIView):
    queryset = UserGroup.objects.all()
    serializer_class = GroupDetailSerializer
    create_serializer_class = GroupSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class GroupReviewUpdateDestroyView(UpdateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = UserGroup.objects.all()
    serializer_class = GroupDetailSerializer
    update_serializer_class = GroupSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class GroupListView(generics.ListCreateAPIView):
    queryset = UserGroup.objects.all()
    serializer_class = GroupNameSerializer
    filter_backends = [IsFranchiseeFilterBackend]
    pagination_class = LargeResultsSetPagination
