from rest_framework import serializers
from bot.models import UserGroup
from api.v1.users.serializers import UserShortSerializer
from api.core.utils import CurrentFranchiseeDefault


class GroupSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())

    class Meta:
        model = UserGroup
        fields = ['id', 'franchisee', 'name', 'users', 'notifications']


class GroupNameSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='name')

    class Meta:
        model = UserGroup
        fields = ['id', 'full_name']


class GroupDetailSerializer(serializers.ModelSerializer):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    users = UserShortSerializer(many=True)
    notifications = serializers.SerializerMethodField()

    def get_notifications(self, obj):
        types = dict(obj.NOTIFICATIONS)
        data = []
        for notification in obj.notifications:
            data.append({
                'name': types.get(notification),
                'number': notification
            })
        return data

    class Meta:
        model = UserGroup
        fields = ['id', 'franchisee', 'name', 'users', 'notifications']
