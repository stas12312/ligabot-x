from rest_framework import generics, views
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.core.mixins import UpdateModelMixin
from api.core.paginations import LargeResultsSetPagination
from bot.models import TelegramGroup
from .serializers import TelegramGroupDetailSerializer, TelegramGroupSerializer, TelegramGroupListSerializer


class TelegramGroupListView(generics.ListAPIView):
    queryset = TelegramGroup.objects.all()
    serializer_class = TelegramGroupDetailSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class TelegramGroupRetrieveUpdateDestroy(UpdateModelMixin,
                                         generics.RetrieveUpdateDestroyAPIView):
    queryset = TelegramGroup.objects.all()
    serializer_class = TelegramGroupDetailSerializer
    update_serializer_class = TelegramGroupSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class TelegramGroupActionsView(views.APIView):

    def get(self, request, *args, **kwargs):
        return Response({'token': request.franchisee.uuid})


class TelegramGroupLargeListView(generics.ListAPIView):
    queryset = TelegramGroup.objects.all()
    serializer_class = TelegramGroupListSerializer
    pagination_class = LargeResultsSetPagination
