from rest_framework import serializers
from bot.models import TelegramGroup


class TelegramGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = TelegramGroup
        fields = ['id', 'type', 'name', 'notifications']
        read_only_fields = ['type', 'name']


class TelegramGroupDetailSerializer(serializers.ModelSerializer):

    notifications = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        return {
            'number': obj.type,
            'name': obj.get_type_display()
        }

    def get_notifications(self, obj):
        types = dict(obj.NOTIFICATIONS)
        data = []
        for notification in obj.notifications:
            data.append({
                'name': types.get(notification),
                'number': notification
            })
        return data

    class Meta:
        model = TelegramGroup
        fields = ['id', 'type', 'name', 'notifications']
        read_only_fields = fields


class TelegramGroupListSerializer(serializers.ModelSerializer):

    class Meta:
        model = TelegramGroup
        fields = ['id', 'name']
        read_only_fields = fields
