from django.urls import path

from . import views

urlpatterns = [
    path('', views.TelegramGroupListView.as_view()),
    path('list/', views.TelegramGroupLargeListView.as_view()),
    path('<int:pk>/', views.TelegramGroupRetrieveUpdateDestroy.as_view()),

    path('token/', views.TelegramGroupActionsView.as_view()),
]