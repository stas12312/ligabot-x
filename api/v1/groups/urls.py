from django.urls import include, path

from . import views

urlpatterns = [
    path('users/', include('api.v1.groups.users.urls')),
    path('telegram/', include('api.v1.groups.telegram.urls')),
    path('notifications/', views.NotificationListView.as_view()),
]
