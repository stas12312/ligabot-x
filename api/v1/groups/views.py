from rest_framework import views
from rest_framework.response import Response

from bot.models import UserGroup


class NotificationListView(views.APIView):

    def get(self, request, *args, **kwargs):
        notifications = dict(UserGroup.NOTIFICATIONS)
        data = []
        for k, v in notifications.items():
            data.append({
                'name': v,
                'number': k,
            })
        return Response({'results': data})
