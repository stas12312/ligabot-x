from django.urls import path

from . import views

urlpatterns = [
    path('', views.SortingListLogView.as_view()),
    path('<int:pk>/', views.SortingRetrieveView.as_view()),
    path('<int:pk>/apply/', views.SortingActionsViewSet.as_view({'get': 'apply'})),
    path('<int:pk>/cancel/', views.SortingActionsViewSet.as_view({'get': 'cancel'})),
]
