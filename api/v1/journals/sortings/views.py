from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from api.core.filters import IsFranchiseeFilterBackend
from api.v1.journals.sortings.serializers import SortingLogSerializer
from bot import bot
from bot.models import SortingLog


class SortingListLogView(generics.ListAPIView):
    serializer_class = SortingLogSerializer
    queryset = SortingLog.objects.all().order_by('-timestamp')
    filterset_fields = ['status']
    ordering_fields = ['timestamp']
    filter_backends = [IsFranchiseeFilterBackend, DjangoFilterBackend, OrderingFilter]


class SortingRetrieveView(generics.RetrieveAPIView):
    queryset = SortingLog.objects.all()
    serializer_class = SortingLogSerializer
    filter_backends = [IsFranchiseeFilterBackend]


class SortingActionsViewSet(viewsets.ModelViewSet):
    queryset = SortingLog.objects.all()

    @action(detail=True, methods=['get'])
    def apply(self, request, *args, **kwargs):
        sorting = self.get_object()
        sorting.status = 1
        sorting.save()
        bot.send_messages([sorting.teacher.id], bot.get_message_for_result_check(sorting))
        serializer = SortingLogSerializer(sorting)
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def cancel(self, request, *args, **kwargs):
        sorting = self.get_object()
        sorting.status = 2
        sorting.save()
        bot.send_messages([sorting.teacher.id], bot.get_message_for_result_check(sorting))

        serializer = SortingLogSerializer(sorting)
        return Response(serializer.data)
