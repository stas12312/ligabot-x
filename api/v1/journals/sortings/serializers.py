from rest_framework import serializers

from api.core.mixins import ImageSerializerMixin
from api.core.utils import CurrentFranchiseeDefault
from api.v1.locations.serializers import LocationShortSerializer
from api.v1.users.serializers import UserShortSerializer
from bot.models import SortingLog


class SortingLogSerializer(serializers.ModelSerializer, ImageSerializerMixin):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    images = serializers.SerializerMethodField()
    teacher = UserShortSerializer()
    location = LocationShortSerializer()
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        return {
            'number': obj.status,
            'text': obj.get_status_display()
        }

    class Meta:
        model = SortingLog
        fields = '__all__'


class SortingsStatisticSerializer(serializers.ModelSerializer):
    location = LocationShortSerializer()
    teacher = UserShortSerializer()

    class Meta:
        model = SortingLog
        fields = ('timestamp', 'location', 'teacher', 'status')
