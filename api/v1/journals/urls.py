from django.urls import path, include

urlpatterns = [
    path('cabinets/', include('api.v1.journals.cabinets.urls')),
    path('sortings/', include('api.v1.journals.sortings.urls')),
    path('preparations/', include('api.v1.journals.preparations.urls')),
    path('orders/', include('api.v1.journals.orders.urls')),
    path('photos/', include('api.v1.journals.photos.urls')),
]
