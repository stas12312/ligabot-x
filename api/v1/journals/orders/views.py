from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from api.core.filters import IsFranchiseeFilterBackend
from bot import bot
from bot.models import DetailsOrder
from .serializers import DetailOrderShortSerializer, DetailOrderSerializer


class DetailsOrderListView(generics.ListAPIView):
    serializer_class = DetailOrderShortSerializer
    queryset = DetailsOrder.objects.exclude(info={})
    filter_backends = [IsFranchiseeFilterBackend, DjangoFilterBackend]
    filterset_fields = ['teacher', 'location', 'status']


class DetailsOrderRetrieveView(generics.RetrieveAPIView):
    serializer_class = DetailOrderSerializer
    queryset = DetailsOrder.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]


class DetailsOrderView(APIView):

    def post(self, request, order_uuid):
        """Обработка входящего запроса на добавление деталей к заказу"""
        data = request.data

        kit_number = data['set_number']
        kit_name = data['set_name']
        kit_items = data['list_detail']

        order = DetailsOrder.objects.get(id=order_uuid)
        order_data = order.info
        if 'kits' in order_data:
            order_data['kits'].append({'name': kit_name, 'number': kit_number})
        else:
            order_data['kits'] = [{'name': kit_name, 'number': kit_number}]

        # Если есть информация о деталях, то дополняем его
        # TODO Придумать алгоритм получше:)
        if 'items' in order_data:
            for item in kit_items:
                # Если ключ есть в списе из БД, то добавляем к нему количество
                is_update = False
                for bd_item in order_data['items']:
                    if bd_item['id'] == item['id']:
                        bd_item['count'] += item['need_count']
                        is_update = True
                        break

                if not is_update:
                    item_info = {
                        'count': item['need_count'],
                        'name': item['name'],
                        'image': item['image'],
                        'id': item['id'],
                    }
                    order_data['items'].append(item_info)
        #
        else:
            order_data['items'] = []
            for item in kit_items:
                # Формируем итоговый результат
                item_info = {
                    'count': item['need_count'],
                    'name': item['name'],
                    'image': item['image'],
                    'id': item['id'],
                }
                order_data['items'].append(item_info)

        order.info = order_data
        order.save()

        return Response({'detail', 'Заказ обновлен'})


class DetailsOrderActionsView(viewsets.ModelViewSet):
    queryset = DetailsOrder.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]

    @action(detail=True, methods=['post'])
    def complete(self, request, pk):
        """Завершения заказа на детали"""
        order = self.get_object()
        if order.status != 0:
            return Response({'message': 'Заказ уже был завершен/отклонен'}, status=400)
        comment = request.data.get('comment', None)
        if comment:
            order.comment = comment
        else:
            comment = 'Отсутствует'
        bot.bot.send_message(order.teacher.chat_id,
                             f'Ваш заказ на детали готов\nКомментарий: {comment}')
        order.status = 1
        order.save()
        return Response()

    @action(detail=True, methods=['post'])
    def cancel(self, request, pk):
        """Отклонение заказа на детали"""
        order = self.get_object()
        if order.status != 0:
            return Response({'message': 'Заказ уже был завершен/отклонен'}, status=400)
        comment = request.data.get('comment', None)
        if comment:
            order.comment = comment
        else:
            comment = 'Отсутствует'

        bot.bot.send_message(order.teacher.chat_id,
                             f'Ваш заказ на детали отклонен\nКомментарий: {comment}')
        order.status = 2
        order.save()
        return Response()
