from rest_framework import serializers

from api.v1.users.serializers import UserShortSerializer
from api.v1.locations.serializers import LocationShortSerializer
from bot.models import DetailsOrder


class DetailOrderShortSerializer(serializers.ModelSerializer):
    teacher = UserShortSerializer()
    status = serializers.SerializerMethodField()
    location = LocationShortSerializer()

    def get_status(self, obj):
        return {
            'number': obj.status,
            'text': obj.get_status_display()
        }

    class Meta:
        model = DetailsOrder
        fields = ['id', 'teacher', 'status', 'created', 'comment', 'location']


class DetailOrderSerializer(serializers.ModelSerializer):
    teacher = UserShortSerializer()
    status = serializers.SerializerMethodField()
    location = LocationShortSerializer()

    def get_status(self, obj):
        return {
            'number': obj.status,
            'text': obj.get_status_display()
        }

    class Meta:
        model = DetailsOrder
        fields = '__all__'
