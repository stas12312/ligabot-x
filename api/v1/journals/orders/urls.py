from django.urls import path

from . import views

urlpatterns = [
    path('', views.DetailsOrderListView.as_view()),
    path('<str:pk>/', views.DetailsOrderRetrieveView.as_view()),
    path('<str:pk>/complete/', views.DetailsOrderActionsView.as_view({'post': 'complete'})),
    path('<str:pk>/cancel/', views.DetailsOrderActionsView.as_view({'post': 'cancel'})),
    path('webhook/<str:order_uuid>/', views.DetailsOrderView.as_view()),
]
