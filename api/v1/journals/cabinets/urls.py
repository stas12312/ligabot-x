from django.urls import path

from . import views

urlpatterns = [
    path('', views.LessonLogListView.as_view()),
    path('<int:pk>/', views.LessonLogRetrieveView.as_view()),
]