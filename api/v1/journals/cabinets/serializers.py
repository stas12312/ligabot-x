from rest_framework import serializers

from api.core.mixins import ImageSerializerMixin
from api.core.utils import CurrentFranchiseeDefault
from api.v1.locations.serializers import LocationShortSerializer
from api.v1.lessons.serializers import LessonSerializer
from api.v1.users.serializers import UserShortSerializer
from bot.models import LessonLog


class LessonLogSerializer(serializers.ModelSerializer, ImageSerializerMixin):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    teacher = UserShortSerializer()
    lesson = LessonSerializer()
    images = serializers.SerializerMethodField()
    location = LocationShortSerializer()

    class Meta:
        model = LessonLog
        fields = '__all__'


class LessonShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = LessonLog
        fields = ['time_arrival', 'before_time']
