from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics

from api.core.filters import IsFranchiseeFilterBackend
from api.v1.journals.filters import DateIntervalFilterBackend
from bot.models import LessonLog
from .filters import ExpandedLessonLogBackend
from .serializers import LessonLogSerializer


class LessonLogListView(generics.ListAPIView):
    serializer_class = LessonLogSerializer
    queryset = LessonLog.objects.all()
    filter_backends = [IsFranchiseeFilterBackend,
                       DateIntervalFilterBackend,
                       DjangoFilterBackend,
                       ExpandedLessonLogBackend]
    filterset_fields = ['teacher', 'timestamp', 'location']


class LessonLogRetrieveView(generics.RetrieveAPIView):
    serializer_class = LessonLogSerializer
    queryset = LessonLog.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]
