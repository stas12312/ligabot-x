from rest_framework import filters


class ExpandedLessonLogBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        before_time_min = request.query_params.get('before_time_min', None)
        before_time_max = request.query_params.get('before_time_max', None)

        if before_time_min:
            queryset = queryset.filter(before_time__gte=before_time_min)
        if before_time_max:
            queryset = queryset.filter(before_time__lte=before_time_max)

        return queryset
