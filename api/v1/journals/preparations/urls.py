from django.urls import path

from . import views

urlpatterns = [
    path('', views.PreparationsListCreateView.as_view()),
]