from datetime import timedelta
from django.utils import timezone
from rest_framework import generics

from api.core.filters import IsFranchiseeFilterBackend
from api.v1.journals.filters import DateIntervalFilterBackend
from bot.models import Preparation, User
from .serializers import PreparationSerializer, PreparationDetailSerializer
from api.v1.users.serializers import UserShortSerializer
from bot import utils


class PreparationsListCreateView(generics.ListCreateAPIView):
    """
    Реализация API для работы с подготовками
    """
    queryset = Preparation.objects.exclude(status=2)
    serializer_class = PreparationSerializer
    filter_backends = [IsFranchiseeFilterBackend, DateIntervalFilterBackend]
    filterset_fields = ['status', 'teacher', 'franchisee']

    def get(self, request, *args, **kwargs):
        # Получаем параметр offset
        offset = request.GET.get('offset')
        # Получаем текущую дату
        now = timezone.localtime(timezone.now())
        # Если offset параметр передан
        # Сдвигаем текущую неделю
        if offset:
            now = now - timedelta(weeks=int(offset))
        weak = utils.get_weak(now)
        # Получаем сортировки за выбранный период
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(timestamp__lte=weak['end'], timestamp__gte=weak['start']).select_related()

        teachers = User.objects.filter(roles__name='Преподаватель', is_active=True,
                                       cities=request.franchisee).order_by('full_name')
        teachers = self.paginate_queryset(teachers)
        results = []
        for teacher in teachers:
            try:
                preparation = queryset.get(teacher=teacher)
                results.append(PreparationDetailSerializer(preparation).data)
            except Preparation.DoesNotExist:
                results.append({'teacher': UserShortSerializer(teacher).data, 'id': None})

        # Получаем информацию о странице и формирует ответ
        data = self.get_paginated_response({})
        data.data['start'] = weak['start'].date().isoformat()
        data.data['end'] = weak['end'].date().isoformat()
        data.data['offset'] = offset
        # Удаляем ключ чтобы добавить результат в конец
        del data.data['results']
        data.data['results'] = results

        return data

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return PreparationSerializer
        elif self.request.method == 'GET':
            return PreparationDetailSerializer


class PreparationsRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PreparationSerializer
    queryset = Preparation.objects.all()
    filter_backends = [IsFranchiseeFilterBackend]