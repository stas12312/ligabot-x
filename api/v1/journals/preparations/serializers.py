from rest_framework import serializers

from api.core.mixins import ImageSerializerMixin
from api.v1.users.serializers import UserShortSerializer
from api.v1.utils import CurrentFranchiseeDefault
from bot.models import Preparation


class PreparationSerializer(serializers.ModelSerializer, ImageSerializerMixin):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    images = serializers.SerializerMethodField()

    class Meta:
        model = Preparation
        fields = '__all__'

    def validate_teacher(self, teacher):
        """
        Валидатор для проверки принадлежности пользователя франчайзи
        :param teacher:
        :return:
        """
        request = self.context['request']
        if request.franchisee != teacher.franchisee:
            raise serializers.ValidationError('%s не является вашим преподавателем' % teacher.full_name)

        return teacher


class PreparationDetailSerializer(serializers.ModelSerializer, ImageSerializerMixin):
    franchisee = serializers.HiddenField(default=CurrentFranchiseeDefault())
    images = serializers.SerializerMethodField()
    teacher = UserShortSerializer()

    class Meta:
        model = Preparation
        fields = '__all__'
