from django.urls import path

from . import views

urlpatterns = [
    path('', views.PhotoLogListView.as_view()),
]
