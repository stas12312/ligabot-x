from dateutil.parser import parse

from rest_framework import filters


class PhotoLogFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        start = request.query_params.get('start', None)
        end = request.query_params.get('end', None)
        if start:
            start = parse(start)
            queryset = queryset.filter(lesson_date__gte=start)

        if end:
            end = parse(end)
            queryset = queryset.filter(lesson_date__lte=end)

        return queryset