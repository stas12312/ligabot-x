from rest_framework import serializers

from api.v1.locations.serializers import LocationShortSerializer
from api.v1.users.serializers import UserShortSerializer
from api.v1.lessons.serializers import LessonDetailSerializer
from bot.models import PhotoLog


class PhotoLogSerializer(serializers.ModelSerializer):

    teacher = UserShortSerializer()
    location = LocationShortSerializer()
    lesson = LessonDetailSerializer()
    group = serializers.SerializerMethodField()

    def get_group(self, obj):
        if obj.lesson and obj.lesson.group_name:
            return obj.lesson.group_name
        else:
            return None

    class Meta:
        model = PhotoLog
        fields = ['teacher', 'created', 'location', 'url',
                  'photos_count', 'lesson', 'lesson_date', 'group']
        read_only_fields = fields
