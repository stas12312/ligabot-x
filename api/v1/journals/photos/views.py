from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics

from api.core.filters import IsFranchiseeFilterBackend
from api.v1.journals.photos.filters import PhotoLogFilterBackend
from api.v1.journals.photos.serializers import PhotoLogSerializer
from bot.models import PhotoLog


class PhotoLogListView(generics.ListAPIView):
    queryset = PhotoLog.objects.all().order_by('-lesson_date')
    serializer_class = PhotoLogSerializer
    filterset_fields = ['teacher', 'location']
    filter_backends = [IsFranchiseeFilterBackend,
                       DjangoFilterBackend,
                       PhotoLogFilterBackend]
