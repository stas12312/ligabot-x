from dateutil.parser import parse
from rest_framework import filters


class DateIntervalFilterBackend(filters.BaseFilterBackend):

    """
    Фильтр, для выбора интервала.
    Может применяться ко всем типам отчётов
    """

    def filter_queryset(self, request, queryset, view):

        # Проверяем, что есть параметр начала интервала
        start_interval = request.GET.get('start', None)
        timezone_ = request.franchisee.get_timezone()
        if start_interval:
            start_interval = timezone_.localize(parse(start_interval))
            queryset = queryset.filter(timestamp__date__gte=start_interval.date())

        # Проверяем, что есть параметр конца интервала
        end_interval = request.GET.get('end', None)
        if end_interval:
            end_interval = timezone_.localize(parse(end_interval))
            queryset = queryset.filter(timestamp__date__lte=end_interval.date())

        return queryset
