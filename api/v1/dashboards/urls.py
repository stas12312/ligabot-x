from django.urls import include, path

urlpatterns = [
    path('arrival/', include('api.v1.dashboards.arrival.urls')),
]