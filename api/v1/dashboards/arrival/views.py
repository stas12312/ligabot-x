from datetime import timedelta

from dateutil import parser
from rest_framework.generics import views
from rest_framework.response import Response

from api.v1.journals.cabinets.serializers import LessonShortSerializer
from api.v1.locations.serializers import LocationShortSerializer
from bot.models import LessonLog, Lesson, Location


class DashboardView(views.APIView):
    """
    Формирование дашбоарда с информацией о
    прибытиях преподавателей на филиалы
    """

    def get(self, request, *args, **kwargs):

        token = request.query_params.get('token', -1)
        now = request.query_params.get('datetime')

        city = request.franchisee
        if now:
            now = parser.isoparse(now)
        else:
            now = city.get_localtime()

        # Получаем сегодняшние занятия и записи о прибытиях на локации
        locations = Location.objects.filter(franchisee=city, is_active=True).prefetch_related('lesson_set',
                                                                                              'lessonlog_set')

        start_time = now + timedelta(minutes=30)
        # Получаем занятие, до начала которых 30 минут и которые еще не окончены
        lessons = Lesson.objects.filter(date=now.date(), start__lte=start_time, end__gte=now,
                                        franchisee=city, is_active=True).select_related('location').prefetch_related(
            'teachers')
        lessons_entries = LessonLog.objects.filter(lesson__in=lessons).select_related('location',
                                                                                      'teacher').prefetch_related()

        # Считаем значение токена
        # Токен высчитывается по следующей формуле
        # Сумма ввсех id занятий + Сумма всех id записей о приходах
        calculated_token = sum(lessons.values_list('id', flat=True)) + sum(lessons.values_list('id', flat=True))

        data = {'token': calculated_token}

        if calculated_token == int(token):
            return Response(data)

        data['results'] = []
        # Для всех локаций строим отчеты
        for location in locations:
            # Для каждого занятия проверяем, был ли отчет от преподавателя
            lessons_data = []
            for lesson in filter(lambda x: x.location.id == location.id, lessons):
                teachers_data = []
                for teacher in lesson.teachers.all():
                    # Если у преподавателя был отчет не связанный
                    # с этим занятием, то переходим на следующего
                    # преподавателия
                    if location.lessonlog_set.filter(teacher=teacher).exclude(id=lesson.id).exists():
                        continue

                    # Если от преподавателя есть отчет для
                    # проверяемого занятие, то отмечаем это
                    try:
                        entry = lessons_entries.get(location=location, teacher=teacher)
                        entry = LessonShortSerializer(entry).data
                    except LessonLog.DoesNotExist:
                        entry = None

                    teachers_data.append({
                        'id': teacher.id,
                        'name': teacher.full_name,
                        'entry': entry,
                    })
                if teachers_data:
                    lessons_data.append({
                        'start': lesson.start.isoformat(),
                        'end': lesson.end.isoformat(),
                        'teachers': teachers_data,
                    })
            data['results'].append({
                **LocationShortSerializer(location).data,
                'lessons': lessons_data
            })

        return Response(data)
