from django.urls import path, include

urlpatterns = [
    path('', include('api.v1.urls')),
    path('v1/', include('api.v1.urls')),
    ]
