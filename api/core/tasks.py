import time

from celery import shared_task
from django.core.cache import cache

from bot.models import CRMInfo
from core.synchronization.sync import AlphaCRMSynchronization, AlphaCRMProcessingWebhook


@shared_task()
def sync_data(franchisee_id, entity: str):
    """Задача для синхронизации CRM системы с TelegramBot"""
    crm_info = CRMInfo.objects.get(franchisee_id=franchisee_id)
    sync = AlphaCRMSynchronization(crm_info.email, crm_info.api_key, crm_info.franchisee, crm_info.url)
    # Помечяем, что идет синхронизация
    # с помощью Redis хранилища
    cache.set(f'sync_{franchisee_id}', True, timeout=240)

    start = time.time()
    if entity == 'teachers':
        sync.users_synchronization()
    elif entity == 'locations':
        sync.locations_synchronization()
    elif entity == 'regular_lessons':
        # Для корректной синхронизации регулярных занятий
        # синхронизируем пользователей и локации
        sync.users_synchronization()
        sync.locations_synchronization()
        sync.regular_lessons_synchronization()
    elif entity == 'all':
        sync.users_synchronization()
        sync.locations_synchronization()
        sync.regular_lessons_synchronization()

    diff = time.time() - start
    cache.delete(f'sync_{franchisee_id}')
    print(f'Синхронизация завершена: {crm_info.franchisee.name} за {diff} сек.')


@shared_task(max_retries=2)
def proc_webhook(franchisee_id, data):
    """Фоновая задача для обработки вебхука"""
    crm_info = CRMInfo.objects.get(franchisee_id=franchisee_id)
    proc = AlphaCRMProcessingWebhook(data, crm_info.franchisee, crm_info)
    entity = data.get('entity', None)
    try:
        if entity == 'Location':
            # Обновление информации об филиале
            proc.on_location()
        elif entity == 'RegularLesson':
            # Обновление информации о регулярном занятии
            proc.on_regular_lesson()
        elif entity == 'Lesson':
            # Обновление информации о занятии
            proc.on_lesson()
        elif entity == 'Teacher':
            # Обвноление информации о преподавателе
            proc.on_teacher()
        else:
            # Ничего не делаем
            pass
    except AttributeError:
        proc_webhook.retry(countdown=2)
