from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.core.paginator import InvalidPage


class CustomPagination(PageNumberPagination):
    """Пагинация записей"""

    def paginate_queryset(self, queryset, request, view=None):
        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        page_size = self.get_page_size(request)
        if not page_size:
            return None

        paginator = self.django_paginator_class(queryset, page_size)
        self.paginator = paginator
        page_number = request.query_params.get(self.page_query_param, 1)
        if page_number in self.last_page_strings:
            page_number = paginator.num_pages
        try:
            self.page = paginator.page(page_number)
            self.page_number = page_number
            data = self.page
        except InvalidPage as ex:
            # При воникновении ошибки возвращаем пустое data
            self.page_number = page_number if page_number.isdigit() else 1
            data = {}

        if paginator.num_pages > 1 and self.template is not None:
            self.display_page_controls = True

        self.request = request
        return list(data)

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('total_count', self.paginator.count),
            ('page_count', self.paginator.num_pages),
            ('current_page', self.page_number),
            ('page_size', self.page_size),
            ('results', data)
        ]))


class LargeResultsSetPagination(CustomPagination):
    page_size = 1000
    max_page_size = 10000
