from rest_framework.permissions import BasePermission


class APIPermission(BasePermission):
    """
    Проверка доступности к View
    """

    def has_permission(self, request, view):
        if not view.need_permission:
            raise ValueError('View должен иметь атрибут need_permission')

        controller, action = view.need_permission.split(',')
        permissions = request.permissions

        if request.method == 'get':
            action = 'index'
        elif request.method == 'post':
            action = 'create'
        elif request.method in ('put', 'path'):
            action = 'update'
        elif request.method == 'delete':
            action = 'delete'

        return self.check_permissions(controller, action, permissions)

    def check_permissions(self, controller, action, permissions):
        for permission in permissions:
            if (permission['service'] == 'ligabot'
                    and permission['controller'] == controller
                    and permission['action'] == action):
                return True
            return False
