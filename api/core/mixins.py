from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.settings import api_settings


class CreateModelMixin:
    """
    Примись для создания модели
    Для создания используется create_serializer_class
    """
    def create(self, request, *args, **kwargs):
        serializer = self.create_serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        show_serializer = self.get_serializer(instance)
        return Response(show_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


class UpdateModelMixin:
    """
    Примись для обновления моделей
    Для обновления использует update_serializer_class
    """
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.update_serializer_class(instance, data=request.data, partial=partial,
                                                  context={'request': request})
        serializer.is_valid(raise_exception=True)
        instance_ = self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        show_serializer = self.get_serializer(instance_)
        return Response(show_serializer.data)

    def perform_update(self, serializer):
        return serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class ImageSerializerMixin:
    """Получение ссылок на изображения для сериализаторов с галлереей"""

    def get_images(self, obj):
        # Получаем список изображений для данной подготовки
        images = obj.images.image_set
        results = []
        for i, image in enumerate(images.all()):
            results.append({'thumb': settings.API_HOST + image.image.url,
                            'src': settings.API_HOST + image.image.url,
                            'caption': f'Фото {i + 1}'})
        return results
