from rest_framework import filters


class IsFranchiseeFilterBackend(filters.BaseFilterBackend):
    """Фильр для получение записей гоо, который запрашивает API"""

    def filter_queryset(self, request, queryset, view):
        franchisee = request.franchisee
        return queryset.filter(franchisee=franchisee)


class ActiveFilterBackend(filters.BaseFilterBackend):
    """Фильтр для получения активных/архивированных записей"""

    def filter_queryset(self, request, queryset, view):
        active = request.query_params.get('active')
        active = active.lower() if active else None

        if active == 'false' or active == '0':
            return queryset.filter(is_active=False)
        elif active == 'all' or active == '-1':
            return queryset.all()
        else:
            return queryset.filter(is_active=True)