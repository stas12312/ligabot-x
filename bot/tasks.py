import textwrap
from datetime import timedelta

import pytz
from celery import shared_task
from django.utils import timezone
from django.db.models import Q

from core.google_drive import GoogleDrive
from .models import (Notification, Franchisee,
                     User, Task, GoogleInfo, PhotoLog,
                     Message, Lesson)
from requests.exceptions import ConnectionError

def pluralize(value, arg="день,дня,дней"):
    args = arg.split(",")
    number = abs(int(value))
    a = number % 10
    b = number % 100

    if (a == 1) and (b != 11):
        return args[0]
    elif (a >= 2) and (a <= 4) and ((b < 10) or (b >= 20)):
        return args[1]
    else:
        return args[2]


@shared_task()
def send_notifications():
    """
    Отправка уведомлений и запланированных сообщений
    :return:
    """
    notifications = Notification.objects.filter(sending_datetime__lte=timezone.now())
    for notification in notifications:
        notification.send()

    messages = Message.objects.filter(sending_datetime__lte=timezone.now(), is_sent=False)
    for message in messages:
        message.send()


@shared_task()
def create_teacher_notifications(is_test=False, hour=0):
    """
    Создание уведомлений для преподавателей
    - О занятиях на следующий день
    - О ближайшем занятии
    return:
    """
    # Получаем франчайзи у которых сейчас полночь))
    all_cities = Franchisee.objects.filter(is_active=True)
    cities = []
    for city in all_cities:
        time_in_city = timezone.localtime(timezone.now(), pytz.timezone(city.timezone))
        if time_in_city.hour == hour or is_test:
            city.datetime = time_in_city
            cities.append(city)

    for city in cities:
        # Для каждого преподавателя города создаём уведомление о занятиях
        for teacher in User.objects.filter(cities=city):
            Notification.create_tomorrow_teacher_notification(teacher, city)
            Notification.create_today_teacher_notification(teacher, city)


@shared_task()
def send_regular_notifications(hour=10):
    """Отправка уведомления о просроченных задачах"""
    # Получаем франчайзи у которых сейчас полночь))
    all_cities = Franchisee.objects.filter(is_active=True)
    cities = []
    for city in all_cities:
        time_in_city = timezone.localtime(timezone.now(), pytz.timezone(city.timezone))
        if time_in_city.hour == hour:
            city.datetime = time_in_city
            cities.append(city)

    if cities:
        # Так как у всез горродов одинаковый час
        now = cities[0].datetime
        msg_birthday = ''
        # Построение информации о днях рождений сотрудников
        users = User.objects.filter(cities__in=cities, birthday=now.date())
        if users:
            msg_birthday += '🎉 Сегодня день рождения отмечают:\n'
            for user in users:
                msg_birthday += f'{user.markdown_link}\n'
            msg_birthday += '\n'

        tomorrow_birthday = User.objects.filter(cities__in=cities, birthday=(now.date() + timedelta(days=1)))
        if tomorrow_birthday:
            msg_birthday += '🎉 Завтра день рождения отмечают:\n'
            for user in tomorrow_birthday:
                msg_birthday += f'{user.markdown_link}\n'
            msg_birthday += '\n'

        # Для каждого пользователя получаем задачи
        # которые просрочены или дедлайн сегодня
        tasks = Task.objects.filter(franchisee__in=cities, deadline__isnull=False,
                                    deadline__lte=now.date(), status=1)
        # Уведомления строятся по преподавателям
        for user in User.objects.filter(cities__in=cities, is_active=True).exclude(chat_id=0):
            # Получаем задачи, в которых учавствует пользователь
            user_tasks = tasks.filter(Q(executor=user) | Q(director=user)).prefetch_related()
            msg_tasks = ''
            # TODO Провести рефакторинг
            if user_tasks:
                msg_tasks = '❗Напоминаем вам о задачах, требующих вашего внимания\n\n'
                # Если есть просроченные задачи, поставленные пользователем
                if user_tasks.filter(director=user).exclude(executor=user).exists():
                    msg_tasks += '↗️ Поставленные задачи:\n\n'
                    for task in user_tasks.filter(director=user).exclude(executor=user):
                        overdue_days = task.overdue_day(now.date())
                        if overdue_days == 0:
                            overdue_text = 'Сегодня дедлайн'
                        else:
                            ends = pluralize(overdue_days)
                            overdue_text = f'Просрочена на {overdue_days} {ends}'

                        msg_tasks += f'Задача для {task.executor.markdown_link}\n' \
                                     f'"{textwrap.shorten(task.clear_text, 100)}"\n' \
                                     f'{overdue_text}\n'
                # Если есть просроченные задачи, поставленные пользователю
                if user_tasks.filter(executor=user).exists():
                    msg_tasks += '↘️ Исполняемые задачи:\n\n'
                    for task in user_tasks.filter(executor=user):
                        overdue_days = task.overdue_day(now.date())
                        if overdue_days == 0:
                            overdue_text = 'Сегодня дедлайн'
                        else:
                            ends = pluralize(overdue_days)
                            overdue_text = f'Просрочена на {overdue_days} {ends}'

                        msg_tasks += f'Задача от {task.director.markdown_link}\n' \
                                     f'"{textwrap.shorten(task.clear_text, 100)}"\n' \
                                     f'{overdue_text}\n'
            msg = msg_birthday + msg_tasks
            if msg:
                from bot import bot
                try:
                    bot.bot.send_message(user.chat_id, msg, parse_mode='Markdown')
                except bot.ApiException as e:
                    pass


@shared_task()
def upload_photo_on_google_drive(franchisee_id: int, images_url: list, lesson_id: int, user_id: int):
    """Загрузка фото на гугл диск"""

    import io
    from .bot import bot, FileInfo

    lesson = Lesson.objects.get(id=lesson_id)
    user = User.objects.get(id=user_id)

    google_info = GoogleInfo.objects.get(franchisee_id=franchisee_id)

    files = []

    now = timezone.localtime(timezone.now(), pytz.timezone(google_info.franchisee.timezone))

    # Загружаем фото/видео из Telegram
    for file_info in images_url:
        file_type = file_info[0].split('.')[-1]  # Получаем тип файла
        file_name = f'{user.full_name}.{file_type}'
        image_file = io.BytesIO()
        try:
            image_file.write(bot.download_file(file_info[0]))  # Сохраняем изображение в файл
        except ConnectionError as e:
            bot.send_message(user.chat_id, 'При загрузке файлов на Google диск произошла ошибка\n'
                                           'Попробуйте еще раз:)')
        files.append((image_file, file_name, FileInfo(file_info[0], file_info[1])))
    try:
        google_drive = GoogleDrive(credentials_dict=google_info.credentials, google_info=google_info)
        path_items = [
            lesson.location.name,
            lesson.date.strftime('%Y.%m.%d'),
        ]
    except Exception as e:
        print(f'Error: {e}')

    if lesson.group_name:
        group_name = lesson.group_name
    else:
        group_name = ''

    lesson_start = lesson.start.strftime('%H:%M')
    lesson_end = lesson.end.strftime('%H:%M')

    path_items.append(f'{group_name} ({lesson_start} - {lesson_end}) {user.full_name}')

    # Собираем нужный путь
    path = '/'.join(path_items)

    result = google_drive.upload_file_on_disk(files, path, google_info.folder)
    bot.send_message(user.chat_id, f'Фотографии/видео успешно загружены на Google диск. '
                                   f'Для просмотра перейдите по ссылке {result.folder_url}')
    # Создаем или обновляем отчет
    try:
        photo_log = PhotoLog.objects.get(
            teacher_id=user_id,
            lesson=lesson,
        )

        photo_log.photos_count += len(images_url)
        photo_log.save()

    except PhotoLog.DoesNotExist:
        photo_log = PhotoLog.objects.create(
            teacher_id=user_id,
            location_id=lesson.location.id,
            franchisee_id=franchisee_id,
            photos_count=len(images_url),
            lesson_date=lesson.date,
            created=now,
            url=result.folder_url,
            lesson_id=lesson_id,
        )
