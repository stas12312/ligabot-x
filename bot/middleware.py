import json

import pytz
import requests
from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import status

from .models import Franchisee

API_HOST = settings.API_HOST


class FranchiseeMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Проверяем наличие заголовка токена
        path = request.META['PATH_INFO']
        if 'admin' not in path and 'web_hook' not in path and 'views' not in path \
                and 'webhook' not in path and 'callback' not in path:
            if 'HTTP_AUTHORIZATION' in request.META:
                auth = request.META['HTTP_AUTHORIZATION'].split()
                if auth[0].lower() != 'bearer':
                    return HttpResponse('Token is not found', status=status.HTTP_401_UNAUTHORIZED)

                if len(auth) == 1:
                    msg = 'Invalid token header. No credentials provided.'
                    return HttpResponse(msg, status=status.HTTP_401_UNAUTHORIZED)
                elif len(auth) > 2:
                    msg = 'Invalid token header'
                    return HttpResponse(msg, status=status.HTTP_401_UNAUTHORIZED)

                # Формируем запрос к API для получения пользователя
                url = API_HOST + '/api/auth/me'
                headers = {'Authorization': f'Bearer {auth[1]}'}
                r = requests.get(url, headers=headers)
                if r.status_code == 401:
                    msg = 'Invalid token header'
                    return HttpResponse(msg, status=status.HTTP_401_UNAUTHORIZED)

                data_user = json.loads(r.text)

                if not data_user['selectedCity']:
                    msg = 'The user has no cities'
                    return HttpResponse(msg, status=status.HTTP_400_BAD_REQUEST)
                # Получаем текущий город пользователя
                city = data_user['selectedCity']
                available_cities = data_user['cities']
                # Проверяем, что франчайзи зарегистрирован в системе
                try:
                    franchisee = Franchisee.objects.get(api_id=city['id'])
                except Franchisee.DoesNotExist:
                    # Регистрируем город
                    franchisee = Franchisee.objects.create(api_id=city['id'],
                                                           timezone=city['timezone'],
                                                           name=city['name'])
                selected_city = city
                # Регистрируем все доступные города пользователя
                for city in available_cities:
                    city_id = city['id']
                    city_name = city['name']
                    city_timezone = city['timezone']
                    # Проверяем существование города
                    try:
                        Franchisee.objects.get(api_id=city_id)
                    except Franchisee.DoesNotExist:
                        Franchisee.objects.create(
                            name=city_name,
                            timezone=city_timezone,
                            api_id=city_id,
                        )

                request.franchisee = franchisee
                request.timezone = selected_city['timezone']
                request.token = auth[1]
                request.delta_id = data_user['id']
                timezone.activate(pytz.timezone(franchisee.timezone))
            else:
                return HttpResponse('Token is not found', status=status.HTTP_401_UNAUTHORIZED)

        response = self.get_response(request)
        timezone.deactivate()
        return response
