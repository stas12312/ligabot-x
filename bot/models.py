import datetime
import enum
import re
import secrets
import uuid
from datetime import timedelta
from itertools import chain

import pytz
from django.conf import settings
from django.contrib.postgres import fields
from django.core.cache import cache
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from telebot.apihelper import ApiException

DATETIME_FORMAT = "%d.%m.%Y %H:%M"


def get_clear_text(text: str):
    text = text.replace('[', r'\[')
    text = text.replace('_', r'\_')
    text = text.replace('*', r'\*')
    return text


class Franchisee(models.Model):
    """
    Модель франчайзи
    """
    uuid = models.UUIDField(default=uuid.uuid4, verbose_name='UUID', blank=True, null=True, unique=True)
    name = models.CharField(max_length=256, verbose_name='Название')
    api_id = models.IntegerField(verbose_name='Идентификатор в DeltaCRM', primary_key=True)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    timezone = models.CharField(max_length=64, verbose_name='Временная зона', default='Europe/Moscow')

    def __str__(self):
        return f'{self.api_id}'

    @property
    def id(self):
        return self.api_id

    class Meta:
        verbose_name_plural = 'Франчайзи'
        verbose_name = 'Франчайзи'

    def get_localtime(self) -> datetime.datetime:
        """Получение локального времени для города"""
        return timezone.localtime(timezone.now(), pytz.timezone(self.timezone))

    def get_timezone(self):
        """Получение временной зоны"""
        return pytz.timezone(self.timezone)


class Role(models.Model):
    """
    Модель ролей
    """
    name = models.CharField(max_length=32, verbose_name='Название', unique=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']
        verbose_name_plural = 'Роли'
        verbose_name = 'Роль'


class Token(models.Model):
    """
    Модель токена для авторизации в боте
    """
    TOKEN_LENGTH = 16

    value = models.CharField(max_length=64, verbose_name='Значение', editable=False, unique=True)
    user = models.OneToOneField('User', verbose_name='Пользователь', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.value = secrets.token_hex(self.TOKEN_LENGTH)
        super(Token, self).save(args, kwargs)

    def __str__(self):
        return f'{self.user} - {self.value}'

    class Meta:
        verbose_name = 'Токен'
        verbose_name_plural = 'Токены'


class User(models.Model):
    """
    Модель пользователя в телеграм
    """
    full_name = models.CharField(max_length=128, verbose_name='Имя')
    phone = models.CharField(max_length=16, verbose_name='Номер телефона', null=True, blank=True)
    email = models.EmailField(max_length=64, verbose_name='E-mail', null=True, blank=True)
    roles = models.ManyToManyField(Role, verbose_name='Роли')
    chat_id = models.IntegerField(verbose_name='Идентификатор в Telegram', default=0)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    cities = models.ManyToManyField(Franchisee, verbose_name='Города', related_name='cities')
    delta_id = models.IntegerField(verbose_name='Идентификатор в Delta-CRM', default=0)
    crm_id = models.IntegerField(verbose_name='Идентификатор в сторонней CRM системе', default=0)
    birthday = models.DateField(verbose_name='День рождения', null=True, blank=True)
    profile_token = models.UUIDField(verbose_name='Токен для профиля',
                                     default=uuid.uuid4, unique=True, blank=True, null=True)

    def save(self, *args, **kwargs):
        # Обрабатываем номер телефона, если он указан
        if self.phone:
            self.phone = re.sub(r'[()-]', "", self.phone)

        if not self.pk:
            token = Token()
            super(User, self).save(*args, **kwargs)
            token.user = self
            token.save()
        else:
            super(User, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.full_name}'

    class Meta:
        ordering = ['full_name']
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def set_status(self, status: enum.Enum):
        cache.set(f'status:{self.chat_id}', status, timeout=None)

    def get_status(self) -> enum.Enum:
        from .helpers import TeacherStatuses
        return cache.get_or_set(f'status:{self.chat_id}', TeacherStatuses.NOTHING)

    @property
    def html_link(self) -> str:
        """Ссылка на пользователя в Telegram в html режиме"""
        return f''

    @property
    def markdown_link(self) -> str:
        """Ссылка на пользователя в Telegram в markdown режиме"""
        return f'[{self.full_name}](tg://user?id={self.chat_id})'

    @property
    def first_name(self) -> str:
        return self.full_name.split()[1]

    def set_city(self, city: Franchisee):
        cache.set(f'city:{self.id}', city, timeout=None)

    def get_city(self) -> Franchisee:
        return cache.get(f'city:{self.id}')

    def has_many_cities(self) -> bool:
        """Проверка на то, что у пользователь привязан к нескольким городам"""
        if self.cities.count() > 1:
            return True
        else:
            return False

    def get_one_city(self) -> Franchisee:
        """Получение города, если он один"""
        if not self.has_many_cities():
            return self.cities.first()
        else:
            raise ValueError(f'Пользователь {self.full_name} приязан больше чем к одному городу')


class NotificationsMixin:
    SORTINGS = 0
    PREPARATIONS = 1
    ARRIVAL = 2
    ALARM = 3

    NOTIFICATIONS = (
        (SORTINGS, 'Уведомление о сортировке'),
        (PREPARATIONS, 'Уведомление о подготовке'),
        (ARRIVAL, 'Уведомление о приходе/уходе'),
        (ALARM, 'Уведомление об отсутствии преподавателя')
    )


class UserGroup(NotificationsMixin, models.Model):
    """Модель для пользовательских групп"""
    name = models.CharField(max_length=255, verbose_name='Имя группы')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Город', on_delete=models.PROTECT)
    users = models.ManyToManyField(User, verbose_name='Пользователи группы')
    notifications = fields.ArrayField(models.IntegerField(choices=NotificationsMixin.NOTIFICATIONS),
                                      verbose_name='Получаемые уведомления',
                                      blank=True, default=list)


class TelegramGroup(NotificationsMixin, models.Model):
    """Модель для групп и каналов в Телеграм боте"""
    GROUP = 0
    CHANNEL = 1

    TYPES = (
        (GROUP, 'Группа'),
        (CHANNEL, 'Канал'),
    )

    franchisee = models.ForeignKey(Franchisee, verbose_name='Город', on_delete=models.PROTECT)
    chat_id = models.BigIntegerField(verbose_name='Идентификатор')
    type = models.IntegerField(choices=TYPES, verbose_name='Тип')
    name = models.CharField(max_length=255, verbose_name='Название')
    notifications = fields.ArrayField(models.IntegerField(choices=NotificationsMixin.NOTIFICATIONS),
                                      verbose_name='Получаемые уведомления',
                                      blank=True, default=list)

    @property
    def full_name(self):
        return self.name


def save_images(instance, filename):
    timestamp = datetime.datetime.now()
    # Формируем путь для сохранения "Франчайзи/тип_фото/год/месяц/день/имя_файла"
    return f'{instance.franchisee.pk}/{timestamp.year}/' \
           f'{timestamp.month}/{timestamp.day}/{secrets.token_hex(16)}_{filename}'


class Gallery(models.Model):
    """
    Модель для связи нескольких фото с отчетом
    """

    def __str__(self):
        return f'({self.id}) Изображений: {self.image_set.count()}'

    def delete(self, *args, **kwargs):
        self.image_set.all().delete()
        super(Gallery, self).delete(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('show_gallery', args=[self.id])

    class Meta:
        verbose_name = 'Галерея'
        verbose_name_plural = 'Галереи'


@receiver(pre_delete, sender=Gallery)
def delete_gallery_hook(sender, instance, using, **kwargs):
    instance.image_set.all().delete()


class Image(models.Model):
    """
    Модель для изображений
    """
    image = models.ImageField(upload_to=save_images, verbose_name='Место хранения')
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name='Дата добавления')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    gallery = models.ForeignKey(Gallery, verbose_name='Галерея', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return f'{settings.MEDIA_URL}{self.image.name}'

    def delete(self, *args, **kwargs):
        self.image.delete()
        super(Image, self).delete(*args, **kwargs)

    class Meta:
        ordering = ['-timestamp']
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


@receiver(pre_delete, sender=Image)
def delete_image_hook(sender, instance, using, **kwargs):
    instance.image.delete()


class BaseLog(models.Model):
    """
    Абстрактная модель для всех видов отчетов
    """
    STATUSES = (
        (0, 'Не проверено'),
        (1, 'Принято'),
        (2, 'Отклонено'),
    )

    type_str = ''

    timestamp = models.DateTimeField(verbose_name='Дата и время создания')
    teacher = models.ForeignKey(User, verbose_name='Преподаватель', on_delete=models.PROTECT)
    images = models.ForeignKey(Gallery, verbose_name='Изображения', on_delete=models.PROTECT, null=True, blank=True)
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    status = models.IntegerField(choices=STATUSES, verbose_name='Статус отчёта', default=0)

    def delete(self, *args, **kwargs):
        super(BaseLog, self).delete(*args, **kwargs)

    class Meta:
        abstract = True
        ordering = ['-timestamp']

    def get_name(self):
        return self._meta.verbose_name

    def apply(self):
        """Приянтие отчёта"""
        self.status = 1
        self.save()

    def cancel(self):
        """Отклонение отчёта"""
        self.status = 2
        self.save()


class LessonLog(BaseLog):
    """
    Модель для фотоотчёта кабинета
    Фиксируется время прихода и ухода
    """
    type_str = 'lesson'

    lesson = models.ForeignKey('Lesson', verbose_name='Занятие', on_delete=models.PROTECT, null=True, blank=True)
    location = models.ForeignKey('Location', verbose_name='Филиал', on_delete=models.PROTECT)
    before_time = models.IntegerField(verbose_name='Время до начала занятия', null=True, blank=True)
    time_arrival = models.TimeField(verbose_name='Время прибытия')
    time_exit = models.TimeField(verbose_name='Время ухода', null=True, blank=True)
    is_seen = models.BooleanField(verbose_name='Просмотрено', default=False)

    def save(self, *args, **kwargs):
        if self.lesson:
            from . import utils
            self.before_time = utils.minute_interval(self.time_arrival, self.lesson.start, is_abs=False)
        super(LessonLog, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-timestamp']
        verbose_name = 'Фото кабинета'
        verbose_name_plural = 'Фото кабинетов'


class Preparation(BaseLog):
    """
    Модель подготовки
    """
    type_str = 'prepare'

    class Meta:
        verbose_name = 'Подготовка'
        verbose_name_plural = 'Подготовки'

    def __str__(self):
        return f"Подготовка {self.teacher} {self.timestamp}"

    def cancel(self):
        """Отклонение подготовки"""
        self.delete()


class DayOfWeak(models.Model):
    """
    Модель для хранение дней недели
    """
    full_name = models.CharField(max_length=16, verbose_name='Полное наименование дня недели')
    short_name = models.CharField(max_length=8, verbose_name='Сокращенное название дня недели')
    number = models.IntegerField(verbose_name='Порядковый номер дня недели')

    def __str__(self):
        return f'{self.number}: {self.full_name} ({self.short_name})'

    class Meta:
        ordering = ['number']
        verbose_name = 'День недели'
        verbose_name_plural = 'Дни недели'


class RegularLesson(models.Model):
    """Модель регулярного расписания"""
    start_date = models.DateField(verbose_name='Дата начала')
    end_date = models.DateField(verbose_name='Дата окончания')
    comment = models.CharField(max_length=128, verbose_name='Название', default='', blank=True)
    days_of_weak = models.ManyToManyField(DayOfWeak, verbose_name='Дни недели')
    teachers = models.ManyToManyField(User, verbose_name='Преподаватели')
    location = models.ForeignKey('Location', verbose_name='Филиал', on_delete=models.PROTECT, null=True)
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    time_start = models.TimeField(verbose_name='Время начала занятия')
    time_end = models.TimeField(verbose_name='Время окончания занятия')
    crm_id = models.IntegerField(verbose_name='Идентификатор в CRM', default=0)
    is_active = models.BooleanField(verbose_name='Активный', default=True)

    group_crm_id = models.IntegerField(verbose_name='Идентификатор в CMR системе', blank=True, default=0)
    group_name = models.CharField(max_length=255, verbose_name='Название группы', blank=True, null=True)

    class Meta:
        ordering = ['location', '-start_date']
        verbose_name = 'Регулярное расписание'
        verbose_name_plural = 'Регулярные расписания'

    def range_date(self, start, end):
        for n in range(int((end - start).days) + 1):
            yield start + timedelta(n)

    def create_lessons(self):
        """Создание занятий внутри выбранного интервала"""
        # Перебираем все дни в выбранном интервале
        # для каждого дня проверяем есть ли день недели в
        # списке days_of_weak
        # Перебор всех дат в указанном интервале
        days = self.days_of_weak.all().values_list('number', flat=True)
        days = [day - 1 for day in days]
        # Получаем текущую дату
        now_date = timezone.localtime(timezone.now()).date()
        start = now_date if self.start_date < now_date else self.start_date

        for single_date in self.range_date(start, self.end_date):
            # Создаем конкретное занятие,
            # если день недели есть в регулярном расписании
            if single_date.weekday() in days:
                lesson = Lesson.objects.create(
                    comment=self.comment,
                    start=self.time_start,
                    end=self.time_end,
                    franchisee=self.franchisee,
                    date=single_date,
                    location=self.location,
                    crm_id=0,
                    regular_lesson=self,
                    group_name=self.group_name,
                )
                lesson.teachers.set(self.teachers.all())

        # При создании всех занятий для регулярного занятия
        for teacher in chain(self.teachers.all()):
            Notification.create_tomorrow_teacher_notification(teacher, self.franchisee)
            Notification.create_today_teacher_notification(teacher, self.franchisee)

    def delete_lessons(self, date_: datetime.date):
        """Архивирование занятий, привязанных к данному расписанию"""
        # Получаем не прошедшие занятия
        # и удаляем их
        self.lesson_set.filter(date__gt=date_).update(is_active=False,
                                                      is_parent_archive=True)

        # При удалении регулярных занятий
        for teacher in chain(self.teachers.all()):
            Notification.create_tomorrow_teacher_notification(teacher, self.franchisee)
            Notification.create_today_teacher_notification(teacher, self.franchisee)

    def update_lessons(self, old_regular_lesson):
        """Обновление занятий внутри выбранного интервала"""
        days = self.days_of_weak.all().values_list('number', flat=True)
        days = [day - 1 for day in days]
        # Получаем текущую дату
        now = timezone.localtime(timezone.now())
        now_date = now.date()

        # Получаем непрошедшие занятия
        lessons = self.lesson_set.filter(date__gt=now_date)

        if self.group_name:
            self.lesson_set.update(group_name=self.group_name)

        # Удаляем занятия, которые не попадают в новый интервал
        if old_regular_lesson.start_date < self.start_date:
            lessons.filter(date__lt=self.start_date).delete()

        if old_regular_lesson.end_date > self.end_date:
            lessons.filter(date__gt=self.end_date).delete()

        # Обновляем информацию у оставшихся занятий
        for lesson in lessons:
            # Если день недели отсутствует в обновленном расписании
            if lesson.date.weekday() not in days:
                lesson.delete(hard_delete=True)
            else:
                # Удаляем преподавателей, которые были в обновляемом
                # регулярном занятии
                for old_teacher in old_regular_lesson.teachers:
                    lesson.teachers.remove(old_teacher)
                for new_teacher in self.teachers.all():
                    lesson.teachers.add(new_teacher)

                # Меняем только те поля, значения которых
                # были в обновляемом расписании
                if lesson.location == old_regular_lesson.location:
                    lesson.location = self.location
                if lesson.start == old_regular_lesson.time_start:
                    lesson.start = self.time_start
                if lesson.end == old_regular_lesson.time_end:
                    lesson.end = self.time_end

                lesson.save()
                # Если занятие сегодня, то перестраиваем уведомление
                # для преподавателей

        start = now_date if self.start_date < now_date else self.start_date
        # Создаем занятия для нового диапазона
        for single_date in self.range_date(start, self.end_date):
            if single_date.weekday() in days:
                try:
                    self.lesson_set.get(date=single_date)
                except Lesson.DoesNotExist:
                    lesson = Lesson.objects.create(
                        comment=self.comment,
                        start=self.time_start,
                        end=self.time_end,
                        franchisee=self.franchisee,
                        date=single_date,
                        location=self.location,
                        crm_id=0,
                        regular_lesson=self,
                        group_name=self.group_name
                    )
                    lesson.teachers.set(self.teachers.all())

        # Если регулярное расписание поменялось
        for teacher in chain(self.teachers.all(), old_regular_lesson.teachers):
            Notification.create_tomorrow_teacher_notification(teacher, self.franchisee)
            Notification.create_today_teacher_notification(teacher, self.franchisee)

    def delete(self, *args, **kwargs):
        """При удалении помечаем регулярное расписание неактивным"""
        self.is_active = False
        self.save()

        # Получаем текущую дату
        now_date = timezone.localtime(timezone.now()).date()

        self.delete_lessons(now_date)
        for teacher in self.teachers.all():
            Notification.create_tomorrow_teacher_notification(teacher, self.franchisee)
            Notification.create_today_teacher_notification(teacher, self.franchisee)

    def archiving(self):
        self.is_active = False
        self.save()

    def restore(self):
        """Помечаем регулярное расписание, как активное"""
        self.is_active = True
        self.save()

        self.lesson_set.filter(is_active=False).update(is_active=True, is_parent_archive=False)


class Lesson(models.Model):
    """
    Модель занятия
    """

    comment = models.CharField(verbose_name='Название', max_length=100, db_index=True, default='', blank=True)
    start = models.TimeField(verbose_name='Начало занятия')
    end = models.TimeField(verbose_name='Конец занятия')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    date = models.DateField(verbose_name='Дата проведения занятия')
    location = models.ForeignKey('Location', verbose_name='Филиал', on_delete=models.PROTECT, null=True)
    teachers = models.ManyToManyField(User, verbose_name='Преподаватели')
    crm_id = models.IntegerField(verbose_name='Идентификатор в CRM', default=0)
    regular_lesson = models.ForeignKey(RegularLesson, verbose_name='Регулярное расписание',
                                       on_delete=models.PROTECT, null=True, blank=True)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    is_parent_archive = models.BooleanField(verbose_name='Архивирован регулярным занятием', default=False)

    group_crm_id = models.IntegerField(verbose_name='Идентификатор в CMR системе', blank=True, default=0)
    group_name = models.CharField(max_length=255, verbose_name='Название группы', blank=True, null=True)

    def __str__(self):
        return f'{self.comment} Дата проведения: {self.date.strftime("%d.%m.%Y")} ' \
               f'({self.start.strftime("%H:%M")}-{self.end.strftime("%H:%M")})'

    class Meta:
        ordering = ['date']
        verbose_name = 'Занятие'
        verbose_name_plural = 'Занятия'

    def delete(self, hard_delete=False, is_parent_archive=False, *args, **kwargs):
        teachers = self.teachers.all()
        if hard_delete:
            super(Lesson, self).delete(*args, **kwargs)
        else:
            self.is_active = False
            self.is_parent_archive = is_parent_archive
            self.save()

        for teacher in teachers.all():
            Notification.create_today_teacher_notification(teacher, self.franchisee)

    def restore(self):
        self.is_active = True
        self.save()

        for teacher in self.teachers.all():
            Notification.create_today_teacher_notification(teacher, self.franchisee)


class Location(models.Model):
    """
    Модель филиала
    """
    name = models.CharField(max_length=255, db_index=True, verbose_name='Название филиала')
    is_active = models.BooleanField(default=True, verbose_name='Действующий')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    crm_id = models.IntegerField(verbose_name='Идентификатор в CRM', null=True)
    inspectors = models.ManyToManyField(User, verbose_name='Ответственные', blank=True)

    def __str__(self):
        try:
            return f'{self.franchisee} - {self.name}'
        except Franchisee.DoesNotExist:
            return f'Нет'

    class Meta:
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'
        unique_together = ['franchisee', 'name']  # Уникальность названия филиала в рамках фрачайзи
        ordering = ['name']


class SortingLog(BaseLog):
    """
    Модель сортировки
    """
    type_str = 'sorting'

    comment = models.CharField(max_length=256, verbose_name='Комментарий', null=True, blank=True)
    location = models.ForeignKey(Location, verbose_name='Филиал', on_delete=models.PROTECT)

    class Meta:
        ordering = ['timestamp']
        verbose_name = 'Сортировка'
        verbose_name_plural = 'Сортировки'


class PhotoLog(models.Model):
    """
    Модель фотоотчёта с занятия
    """
    type_str = 'photo'

    franchisee = models.ForeignKey(Franchisee, verbose_name='Город', on_delete=models.PROTECT)
    created = models.DateTimeField(verbose_name='Дата и время создания')
    teacher = models.ForeignKey(User, verbose_name='Преподаватель', on_delete=models.PROTECT)
    location = models.ForeignKey('Location', verbose_name='Филиал', on_delete=models.PROTECT)
    url = models.CharField(max_length=255, verbose_name='Ссылка на папку с фотографиями')
    photos_count = models.IntegerField(verbose_name='Количество фотографий', default=0)
    lesson_date = models.DateField(verbose_name='Дата проведения занятия', blank=True, null=True)
    lesson = models.ForeignKey(Lesson, verbose_name='Занятие', on_delete=models.PROTECT, blank=True, null=True)

    class Meta:
        verbose_name = 'Фотоотчёт'
        verbose_name_plural = 'Фотоотчёты'


class Meeting(models.Model):
    """
    Модель собрания
    """
    subject = models.CharField(max_length=128, verbose_name='Тема собрания')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    teachers = models.ManyToManyField(User, verbose_name='Преподаватели', blank=True)

    def __str__(self):
        dates = self.meetingdate_set.all()
        msg = f'{self.id} Тема: {self.subject}, Дата и время проведения: '
        for date in dates:
            msg += date.datetime_carried.strftime("%d.%m.%Y %H:%M") + ','
        return msg

    class Meta:
        ordering = ['-id']
        verbose_name = 'Собрание'
        verbose_name_plural = 'Собрания'


class MeetingDate(models.Model):
    """
    Модель даты собрания
    """

    meeting = models.ForeignKey(Meeting, verbose_name='Собрание', on_delete=models.CASCADE)
    datetime_carried = models.DateTimeField(verbose_name='Дата проведения собрания')

    def save(self, *args, **kwargs):

        is_create = False

        if not self.pk:
            is_create = True
        super(MeetingDate, self).save(args, kwargs)
        if is_create:
            text = f'Напоминаем, что {self.datetime_carried.strftime(DATETIME_FORMAT)} ' \
                   f'пройдет собрание на тему: {self.meeting.subject}'
            sending_datetime = self.datetime_carried - datetime.timedelta(days=1)
            receivers = list(
                User.objects.filter(franchisee=self.meeting.franchisee, is_active=True).values_list('id', flat=True))
            notification = Notification(text=text, type=1, object=self.pk,
                                        sending_datetime=sending_datetime, receivers=receivers,
                                        city=self.meeting.franchisee)
            notification.save()

    def delete(self, *args, **kwargs):
        notification = self.get_notification()  # Удаляем уведомление
        if notification:
            notification.delete()
        super(MeetingDate, self).delete(args, kwargs)

    def get_notification(self):
        """Получение уведомления для даты собрания"""
        try:
            return Notification.objects.get(object=self.id, type=1)
        except Notification.DoesNotExist:
            return None

    def __str__(self):
        return f'{self.meeting.subject}: {self.datetime_carried.strftime("%d.%m.%Y %H:%M")}'

    class Meta:
        verbose_name = 'Дата проведения собрания'
        verbose_name_plural = 'Даты проведения собрания'


class Notification(models.Model):
    """
    Модель уведомлений для рассылки по расписанию
    """
    ALARM = 4

    types = (
        (0, 'Напоминание об отправке фото'),
        (1, 'Напоминание о собрании'),
        (2, 'Напоминание о завтрашних занятиях'),
        (3, 'Напоминание о ближайшем занятии'),
        (ALARM, 'Уведомление об отсутствии преподавателя на филиале')
    )

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    text = models.TextField(verbose_name='Текст уведомления')
    receivers = fields.JSONField(verbose_name='Получатели')
    type = models.IntegerField(choices=types, verbose_name='Тип уведомления')
    object = models.IntegerField(verbose_name='Объект, которому принадлежит уведомление')
    location = models.ForeignKey(Location, verbose_name='Локация', on_delete=models.PROTECT, blank=True, null=True)
    sending_datetime = models.DateTimeField(verbose_name='Дата отправки')
    city = models.ForeignKey(Franchisee, verbose_name='Город', blank=True, null=True, on_delete=models.CASCADE)

    def send(self):
        """
        Отправка уведомления
        :return:
        """
        from . import bot

        receivers = self.receivers
        text = f'*Уведомление*\n' \
               f'{self.text}'
        # Если это уведомление об отсутствии преподавателей
        # то отправляем его Администраторам города
        tg_groups = None
        if self.type == self.ALARM and self.city is not None:
            # Если у локации есть ответственные, то отправляем
            # уведомления только им
            if self.location and self.location.inspectors.all().exists():
                receivers = self.location.inspectors.all()
            else:
                receivers = User.objects.filter(cities=self.city,
                                                is_active=True,
                                                ).filter(
                    Q(roles__name='Администратор') | Q(usergroup__notifications__contain=[NotificationsMixin.ALARM])
                    )
            tg_groups = TelegramGroup.objects.filter(
                franchisee=self.city,
                notifications__contains=[NotificationsMixin.ALARM]
            )

        if self.type == 1 and self.city is not None:
            receivers = User.objects.filter(cities=self.city,
                                            roles__name='Преподаватель',
                                            is_active=True
                                            )

        # Отправляем уведомление
        bot.send_messages(receivers, text, parse_mode='Markdown')
        if tg_groups:
            bot.send_messages(tg_groups, text, parse_mode='Markdown')
        # Удаление уведомления
        self.delete()

    @property
    def clear_text(self):
        """Получение экранированного текста задачи"""
        return get_clear_text(self.text)

    @classmethod
    def create_today_teacher_notification(cls, teacher: User, city: Franchisee,
                                          now_datetime: datetime.datetime = None) -> None:
        """Построение уведомлений преподавателю о ближайшем занятии на сегодня"""
        from . import utils
        # Получаем локальное время, если оно не передано
        now_datetime = city.get_localtime() if now_datetime is None else now_datetime

        # Получаем занятия для преподавателя на дату date_
        lessons = Lesson.objects.filter(teachers=teacher,
                                        date=now_datetime.date(),
                                        is_active=True).order_by('start')

        teacher_logs = LessonLog.objects.filter(teacher=teacher, timestamp__date=now_datetime.date())
        # Удаляем старые уведомления
        cls.objects.filter(type__in=[3, 4], object=teacher.id).delete()

        if lessons.exists() and not teacher_logs.exists():
            first_lesson = lessons[0]
            now_time = now_datetime.time()
            start = first_lesson.start
            if not now_time > start:

                # Находим кол-во часов, оставшихся до начала занятия
                diff = utils.minute_interval(now_time, start)
                if diff < 120:
                    send_time = now_datetime.replace(hour=start.hour, minute=start.minute) - \
                                datetime.timedelta(hours=diff)
                    last_str = f'скоро'
                else:
                    send_time = now_datetime.replace(hour=start.hour, minute=start.minute) - \
                                datetime.timedelta(hours=2)
                    last_str = f'через 2 часа'
                time_start = first_lesson.start.strftime("%H:%M")
                time_end = first_lesson.end.strftime("%H:%M")

                msg = f'Напоминаем, что {last_str} начнется ' \
                      f'занятие на локации {first_lesson.location.name} - ' \
                      f'({time_start} - {time_end})'

                cls.objects.create(
                    created_at=now_datetime,
                    text=msg,
                    object=teacher.id,
                    sending_datetime=send_time,
                    type=3,
                    receivers=[teacher.id],
                    city=city,
                )

                if diff > 15:
                    phone_str = f'{teacher.phone}' if teacher.phone else 'Не указан'
                    # Формируем уведомление об отсутствии преподавателя
                    # Отправка будет за 15 минут до начала занятия
                    msg = f'❗ ВНИМАНИЕ ❗\n' \
                          f'🎓 Отсутствует преподаватель {teacher.markdown_link}\n' \
                          f'📱 Тел: {phone_str}\n' \
                          f'🏢 Локация: {first_lesson.location.name}\n' \
                          f'⏳ Начало занятия: {time_start}\n'

                    admins = User.objects.filter(cities=city, is_active=True,
                                                 roles__name='Администратор').values_list('id', flat=True)
                    sending_time = now_datetime.replace(hour=start.hour, minute=start.minute) - timedelta(minutes=15)
                    Notification.objects.create(
                        created_at=now_datetime,
                        text=msg,
                        object=teacher.id,
                        sending_datetime=sending_time,
                        type=4,
                        receivers=list(admins),
                        city=city,
                        location=first_lesson.location,
                    )

    @classmethod
    def create_tomorrow_teacher_notification(cls, teacher: User,
                                             city: Franchisee,
                                             now_datetime: datetime = None) -> None:
        """Построение уведомления о завтрашних занятий"""

        timezone.activate(city.timezone)
        now_datetime = timezone.localtime(timezone.now()) if now_datetime is None else now_datetime
        timezone.deactivate()

        # Получаем занятия преподавателя на следующий день
        tomorrow = now_datetime + timedelta(days=1)
        lessons = Lesson.objects.filter(teachers=teacher,
                                        date=tomorrow.date(),
                                        is_active=True)

        # Удаляем уведомление, если оно есть
        notification = cls.objects.filter(type=2, object=teacher.id).delete()
        msg = f'Напоминаем о завтрашних занятиях\n'
        # Формируем новое уведомление на 18:00
        if lessons:
            # Создание уведомлений для преподавателей о занятиях на следующий день
            for lesson in lessons:
                # Формируем строку по шаблону
                # {Филиал - (Время начала:Время окончания)}
                msg += f'Филиал: {lesson.location.name} - ' \
                       f'{lesson.start.strftime("%H:%M")}:{lesson.end.strftime("%H:%M")}\n'
            Notification.objects.create(
                created_at=now_datetime,
                text=msg,
                object=teacher.id,
                sending_datetime=now_datetime.replace(hour=20, minute=0),
                type=2,
                receivers=[teacher.id],
            )

    def __str__(self):
        return f'{self.text} - {self.type}'

    class Meta:
        verbose_name = 'Уведомление'
        verbose_name_plural = 'Уведомления'


class Message(models.Model):
    TYPES = (
        (0, 'Пользователи'),
        (1, 'Группы'),
        (2, 'Чаты'),
    )

    """Модель для хранения информации об отправленном сообщении"""
    created = models.DateTimeField(verbose_name='Время создания')
    receivers = fields.ArrayField(models.IntegerField(),
                                  verbose_name='Идентификаторы получателей')
    type = models.IntegerField(verbose_name='Тип рассылки', choices=TYPES, default=0)

    text = models.TextField(verbose_name='Сообщение')
    sender = models.IntegerField(verbose_name='Отправитель', default=0)
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи',
                                   on_delete=models.PROTECT)
    additional = fields.JSONField(verbose_name='Дополнительное поле', blank=True, null=True)
    sending_datetime = models.DateTimeField(verbose_name='Время и дата отправки сообщения', blank=True, null=True)
    is_sent = models.BooleanField(verbose_name='Отправлено', default=False)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['-created']

    def delete(self, *args, **kwargs):
        # Удаляем сообщение в Telegram
        from .bot import bot
        if self.is_sent:
            for chat_id, message_id in self.additional.items():
                try:
                    bot.delete_message(chat_id, message_id)
                except ApiException:
                    pass
        super(Message, self).delete(*args, **kwargs)

    def send(self):
        """Отправка сообщения в телеграм бот"""
        from .bot import send_messages

        # Если отправка пользователем, то получаем их по ID
        # и отправляем сообщения
        if self.type == 0:
            receivers = User.objects.filter(id__in=self.receivers,
                                            franchisee=self.franchisee)
        # Если отправка группам пользователей, то получаем всех пользователей
        # которые состоят в группах
        elif self.type == 1:
            receivers = User.objects.filter(usergroup__id__in=self.receivers,
                                            franchisee=self.franchisee)
        # Получаем группы и чаты
        elif self.type == 2:
            receivers = TelegramGroup.objects.filter(id__in=self.receivers,
                                                     franchisee=self.franchisee)
        else:
            return

        info = send_messages(receivers, self.text)
        self.additional = info['message_info']
        self.is_sent = True
        self.save()


# ================== Модели для работы с задачами =========================== #
class Department(models.Model):
    """Модель для отдела"""
    name = models.CharField(max_length=128, verbose_name='Название')
    employees = models.ManyToManyField(User, verbose_name='Сотрудники')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    only_office = models.BooleanField(verbose_name='Доступен только офисным сотрудникам', default=False)

    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'
        ordering = ['name']

    def __str__(self):
        return self.name


class Task(models.Model):
    """Модель для задач"""

    STATUSES = (
        (0, 'Ожидает принятия'),
        (1, 'Выполняется'),
        (2, 'На подтверждении'),
        (3, 'Завершена'),
    )

    director = models.ForeignKey(User, verbose_name='Постановщик задачи',
                                 related_name='director', on_delete=models.PROTECT)
    executor = models.ForeignKey(User, verbose_name='Исполнитель задачи',
                                 related_name='executor', on_delete=models.PROTECT,
                                 blank=True, null=True)
    status = models.IntegerField(verbose_name='Статус задачи', choices=STATUSES, default=0)

    created = models.DateTimeField(verbose_name='Дата и время создания')
    finished = models.DateTimeField(verbose_name='Дата и время завершения', blank=True, null=True)
    deadline = models.DateField(verbose_name='Дата')
    department = models.ForeignKey(Department, verbose_name='Отдел',
                                   on_delete=models.SET_NULL, null=True)
    text = models.TextField(verbose_name='Описание задачи')
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT,
                                   null=True, blank=True)
    finish_comment = models.TextField(verbose_name='Комментарий при заврешении',
                                      blank=True, null=True)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        ordering = ['-created']

    @property
    def clear_text(self):
        """Получение экранированного текста задачи"""
        return get_clear_text(self.text)

    @property
    def clear_deadline(self):
        """Получение обработанного дедлайна"""
        deadline = self.deadline.strftime("%d.%m.%Y") if self.deadline else "Отсутствует"
        return deadline

    @property
    def clear_department(self):
        """Получение обработанного отдела"""
        return self.department if self.department else 'Отсутствует'

    def overdue_day(self, date_: datetime.date) -> int:
        diff = date_ - self.deadline
        return diff.days

    def __str__(self):
        return self.text


class CRMInfo(models.Model):
    """Класс для хранения данных CRM системы"""

    # CRM системы
    TYPES = (
        (0, 'Альфа-CRM'),
    )

    franchisee = models.OneToOneField(Franchisee, verbose_name='Франчайзи',
                                      on_delete=models.PROTECT, unique=True)
    email = models.EmailField(verbose_name='E-mail', blank=True, null=True)
    api_key = models.CharField(max_length=256, verbose_name='Ключ API', blank=True, null=True)
    type_crm = models.IntegerField(verbose_name='CRM система', choices=TYPES, default=0)
    url = models.CharField(max_length=256, verbose_name='Хост', blank=True, null=True)
    info = fields.JSONField(verbose_name='Последние синхронизации', default=dict, blank=True)

    class Meta:
        verbose_name_plural = 'Данные CRM системы'
        verbose_name = 'Данные CRM системы'

    def save(self, *args, **kwargs):
        # Дописываем слэш в конце URL если его нет
        if self.url[-1] != '/':
            self.url += '/'
        # При первом создании инициализируем словарь для информации о синхронизациях
        if not self.pk:
            self.info = {
                'teachers': None,
                'locations': None,
                "lessons": None,
                "regular_lessons": None,
            }
        super(CRMInfo, self).save(*args, **kwargs)


class DetailsOrder(models.Model):
    """Модель для хранения заказа на детали"""

    STATUSES = (
        (0, 'Собирается'),
        (1, 'Собран'),
        (2, 'Отклонен'),
    )

    class Meta:
        ordering = ['-created']
        verbose_name = 'Заказ на детали'
        verbose_name_plural = 'Заказы на детали'

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    info = fields.JSONField(verbose_name='Информация о заказе', default=dict, blank=True)
    teacher = models.ForeignKey(User, verbose_name='Преподаватель', on_delete=models.PROTECT)
    status = models.IntegerField(verbose_name='Статус заказа', choices=STATUSES, default=0)
    created = models.DateTimeField(verbose_name='Дата и время формирования')
    location = models.ForeignKey(Location, verbose_name='Локация', on_delete=models.PROTECT, blank=True, null=True)
    comment = models.CharField(max_length=255, verbose_name='Комментарий', blank=True, null=True)


class Reference(models.Model):
    """Класс для полезных ссылок"""
    TYPES = (
        (0, 'Ссылка'),
        (1, 'Раздел'),
    )

    franchisee = models.ForeignKey(Franchisee, verbose_name='Франчайзи', null=True, on_delete=models.PROTECT,
                                   related_name='notes_city')
    title = models.CharField(max_length=256, verbose_name='Заголовок')
    body = models.TextField(verbose_name='Содержимое')
    updated = models.DateTimeField(verbose_name='Дата и время обновления')
    position = models.IntegerField(verbose_name='Позиция в списке')
    type = models.IntegerField(verbose_name='Тип', choices=TYPES, default=1)

    def save(self, *args, **kwargs):
        # При первом создании добавлем в конец списка
        if self.pk is None:
            position = Reference.objects.filter(franchisee=self.franchisee).count()
            self.position = position

        # Сохраняем дату обновления
        self.updated = timezone.localtime(timezone.now())
        super(Reference, self).save(*args, **kwargs)

    @property
    def clear_title(self):
        return get_clear_text(self.title)

    @property
    def clear_body(self):
        return get_clear_text(self.body)

    class Meta:
        ordering = ['position']


class GoogleInfo(models.Model):
    """Информация для интеграции с Google диском"""
    franchisee = models.OneToOneField(Franchisee, verbose_name='Франчайзи', on_delete=models.PROTECT)
    folder = models.CharField(max_length=255, verbose_name='ID папки')
    credentials = fields.JSONField(verbose_name='Данные для авторизации в Google')
    is_active = models.BooleanField(verbose_name='Активный')
