import io
import json
import logging
import sys
import traceback
from collections import namedtuple
from typing import Type, Optional

import telebot
from dateutil.relativedelta import relativedelta
from django.core.files import File
from django.db.models import Q, QuerySet
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from telebot import apihelper
from telebot import types

from settings.models import DefaultSetting
from . import utils
from .helpers import KeyboardHelper, Buttons, TeacherStatuses as statuses
from .models import *
from .task_framework import TaskHelper, TaskProcessing, Statuses as task_statuses
from .tasks import upload_photo_on_google_drive

BOT_TOKEN = settings.BOT_TOKEN
bot = telebot.TeleBot(settings.BOT_TOKEN, threaded=False)

# подключение прокси, для работы с telegram
apihelper.proxy = {'https': 'socks5://911rush:L7m6JnP@176.114.8.151:65234'}

translate_map = {
    'prepare': 'подготовка',
    'sorting': 'сортировка',
    'lesson': 'занятие',
    'photo': 'фотоотчёт',
}

# Разрешенные форматы изображений
available_mime_types = ['image/jpeg', 'image/jpg', 'image/png', 'video/mp4']

# Создаём объекты для работы с задачами
task_processing = TaskProcessing(bot)

# Создаем logger для логирования
logger = logging.getLogger(__name__)
formatter = logging.Formatter(f'%(asctime)s - %(name)s - %(levelname)s - %(message)s\n'
                              )
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('bot.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

sh = logging.StreamHandler()
sh.setFormatter(formatter)

logger.addHandler(fh)

# Именованный кортеж, для хранения информации о файле
FileInfo = namedtuple('FileInfo', ['path', 'mime_type'])


def logging_response(update: types.Update, json_response: json):
    """
    Получение информации о пользователе, который отправил запрос
    :param update: Обработанный ответ Телеграм
    :param json_response: Json ответ от Телеграм
    :return:
    """
    # Извлекаем message из запроса
    if update.callback_query:
        message = update.callback_query.message
    elif update.message:
        message = update.message
        if message.chat.type != 'private':
            return
    elif update.channel_post:
        message = update.channel_post
        return
    else:
        return

    chat_id = message.chat.id
    user = utils.check_auth(chat_id)
    if user:
        status = user.get_status()
        user_info = {
            'ФИО': user.full_name,
            'Города': [city.name + ',' for city in user.cities.all()],
            'Текущий город': user.get_city().name if user.get_city() else 'Нет информации',
            'Статус': f'{status.__class__.__name__}.{status.name} ({status.value})',
            'Временные данные': cache.get(f'temp:{chat_id}')
        }
    else:
        user_info = {
            'ФИО': 'Неавторизованный пользователь',
        }

    log = {
        'user': user_info,
        'response': json_response
    }

    # log = json.dumps(log, indent=4, ensure_ascii=False)

    logger.debug(log)


@csrf_exempt
def reception_update(request, bot_token):
    """
    Функция принимает WebHook от телеграм и передает запрос на дальнейшую обработку
    :param request: входящий запрос
    :param bot_token: идентификтор бота
    :return Http: ответ сервера
    """
    if bot_token != BOT_TOKEN:
        return HttpResponse('ERROR: Invalid token')

    if request.META.get('CONTENT_TYPE'):
        if (request.META['CONTENT_TYPE'] == 'application/json'
                or request.META['CONTENT_TYPE'] == 'application/x-www-form-urlencoded'):

            json_string = json.loads(request.body.decode())
            update = telebot.types.Update.de_json(json_string)
            # добавляем входящий запрос в логи
            try:
                # if settings.DEBUG:
                #    statprof.start()
                logging_response(update, json_string)
                bot.process_new_updates([update])
            except Exception as e:
                exc_type, exc_value, exc_tb = sys.exc_info()
                bot.send_message(
                    112752666,
                    'Ошибка {} {}'.format(e, traceback.format_exception(exc_type, exc_value, exc_tb)))

                logger.exception('При запросе произошла ошибка', exec_info=True, stack_info=True)
            finally:
                return HttpResponse(json.dumps({}), content_type="application/json")
    return HttpResponse("Принятый токен: {}".format(bot_token))


def auth_user(chat_id: int, token: str) -> Optional[Type[User]]:
    """
    Проверка токена и авторизация пользователя
    :param token: Токен доступа
    :param chat_id: Идентификатор пользователя в Telegram
    :return: Type[User]
    """

    try:
        # Получаем токен и авторизируем пользователя, затем удаляем токен
        print(f'[{token}]')
        token_ = Token.objects.get(value=token)
        print(token_)
        user = token_.user
        user.chat_id = chat_id
        user.set_status(statuses.NOTHING)
        user.save()
        token_.delete()
        return user
    except Token.DoesNotExist:
        # Если токен не найден, возвращаем None
        return None


def move_images_between_gallery(from_: Gallery, to: Gallery) -> None:
    """
    Перемещение изображений из галереи from в галерею to
    :param from_:
    :param to:
    :return:
    """
    # Получаем изображения галереи и привязываем их к другой галереи
    images = Image.objects.filter(gallery=from_)
    for image in images:
        image.gallery = to
        image.save()

    # Удаляем галерею
    try:
        from_.delete()
    except Exception:
        pass


def save_temp_data(chat_id: int, type_log: str, location: Location = None, lesson: Lesson = None) -> str:
    """
    Сохранение временных данных в БД
    :param lesson: Занятие
    :param location: Локация
    :param chat_id: Идентификатор в Telegram
    :param type_log: Тип отчёта
    :return: Сообщение для отправки преподавателю
    """

    # Получаем временные данные
    temp_data = cache.get(f'temp:{chat_id}')

    # Получаем преподавателя
    teacher = User.objects.get(chat_id=chat_id)

    # Создаем галерею для связи с изображениями
    gallery = Gallery()
    gallery.save()
    # Сообщение для отправки преподавателю
    message = ''
    # Сохраняем все изображения и связываем их с галереей
    for image_path, mime_type in temp_data['files']:
        file_type = image_path.split('.')[-1]  # Получаем тип файла
        file_name = f'.{file_type}'
        image = Image(franchisee=teacher.franchisee, gallery=gallery)
        with io.BytesIO() as image_file:
            try:
                image_file.write(bot.download_file(image_path))  # Сохраняем изображение в файл
                image.image.save(file_name, File(image_file))  # Сохраняем ссылки на изображение в БД
            except ApiException as e:
                logger.debug(f'Ошибка при сохранении фотографии '
                             f'{teacher.franchisee.name} {teacher.full_name} {image_path}')
        image.save()

    localtime = temp_data['created']
    timezone.activate(teacher.franchisee.get_timezone())
    # Сохраняем подготовку
    if type_log == 'prepare':
        preparation = Preparation(
            timestamp=temp_data['created'],
            images=gallery,
            teacher=teacher,
            franchisee=teacher.franchisee,
        )

        # Получаем начало и конце текущей недели для проверки, есть ли непроверенные подготовки
        weak = utils.get_weak(localtime)
        try:
            # Ищем подготовки отправленные на текущей недели со статусом 'Не проверено' и 'Принято'
            existing_preparation = Preparation.objects.get(timestamp__date__gte=weak['start'],
                                                           timestamp__date__lte=weak['end'],
                                                           teacher=teacher, status__in=(0, 1))
            # Если статус 'Не проверено', то дополняем подготовку
            if existing_preparation.status == 0:
                move_images_between_gallery(gallery, existing_preparation.images)
                message = f'🔄  Ваш отчёт о подготовке дополнен'
            # Если статус 'Принято', то сообщаем, что отправлять подготовку не надо
            else:
                message = f'Ваша подготовка уже была принята на этой недели (#{existing_preparation.id})'
        except Preparation.DoesNotExist:
            preparation.save()
            message = f'✅ Ваш отчёт о подготовке отправлен'
            # Отправляем отчет на проверку
            send_log_for_check(preparation)
    # Сохраняем сортировку
    elif type_log == 'sorting':
        sorting = SortingLog(
            timestamp=temp_data['created'],
            images=gallery,
            teacher=teacher,
            franchisee=teacher.franchisee,
            location=location,
        )
        try:
            # Ищем непроверенные сортировки с указанной локации текущего дня
            existing_sorting = SortingLog.objects.get(location=location, status=0,
                                                      teacher=teacher, timestamp__date=localtime.date())
            move_images_between_gallery(gallery, existing_sorting.images)
            message = f'🔄 Ваш отчёт о Сортировке дополнен'
        except SortingLog.DoesNotExist:
            sorting.save()
            message = f'✅ Ваш отчёт о Сортировке отправлен'
            # Отправляем отчет на проверку
            send_log_for_check(sorting)
    # Сохраняем отчет о прибытии/уходе с локации
    elif type_log == 'lesson':
        # Проверяем, был ли сегодня отчёт о приходе на локацию от преподавателя
        try:
            existing_lesson_log = LessonLog.objects.get(timestamp__date=localtime.date(),
                                                        teacher=teacher, location=location)
            move_images_between_gallery(gallery, existing_lesson_log.images)
            # Удаляем уведомление об отсутсвии преподавателя

            message = f'🔄 Ваш отчет о кабинете дополнен\n' \
                      f'🏢 Локация: {existing_lesson_log.location.name}'
            # Добавляем отметку об уходе из локации, если прошло больше часа после прибытия
            # if utils.minute_interval(temp_data['created'], existing_lesson_log.time_arrival) > 60:
            #    existing_lesson_log.time_exit = temp_data['created']
            #    existing_lesson_log.save()
            #    message = 'Ваш уход отмечен'

        except LessonLog.DoesNotExist:
            # Создаем новый лог
            lesson_log = LessonLog(time_arrival=temp_data['created'], lesson=lesson, timestamp=temp_data['created'],
                                   teacher=teacher, location=location, franchisee=teacher.franchisee, images=gallery)
            lesson_log.save()
            lesson_info = str(lesson) if lesson else 'Нет информации'
            message = f'✅ Ваше прибытие отмечено\n' \
                      f'⌚ Зафиксированное время: {temp_data["created"].strftime("%H:%M")}\n' \
                      f'🏢 Локация: {location.name}\n' \
                      f'📝 {lesson_info}'
            for notification in Notification.objects.filter(type=4, object=teacher.id):
                notification.delete()

            send_log_for_check(lesson_log)
    elif type_log == 'photo':
        # Проверяем, есть ли непроверенный лог сегодня с этой локации
        try:
            existing_photo_log = PhotoLog.objects.get(timestamp__date=localtime, teacher=teacher,
                                                      location=location, status=0)

            move_images_between_gallery(gallery, existing_photo_log.images)

            message = f'Ваш отчёт [Фото с занятия #{existing_photo_log.id}] обновлен'
        except PhotoLog.DoesNotExist:
            photo_log = PhotoLog(timestamp=temp_data['created'], teacher=teacher, location=location,
                                 franchisee=teacher.franchisee, images=gallery)
            photo_log.save()
            message = f'Ваш отчет [Фото с занятия #{photo_log.id}] отправлен, ожидайте проверки'
            send_log_for_check(photo_log)

    # Удаляем временные данные и устанавливаем нужный статус
    cache.delete_pattern(f'*{chat_id}')
    set_status(chat_id, statuses.NOTHING)

    return message


def get_keyboard_for_action(actions: list) -> types.ReplyKeyboardMarkup:
    """
    Формирование клавиатуры для выбора действия с фото
    :return:
    :param actions: Список действий
    """

    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for action in actions:
        markup.row(action)
    return markup


def get_keyboard_for_check_logs(log) -> types.InlineKeyboardMarkup:
    """
    Формирование клавиатуры для проверки отчёта
    :param log:
    :return:
    """
    log_type = log.type_str

    default_data = f'check:{log_type}:{log.id}:'

    keyboard = telebot.types.InlineKeyboardMarkup()
    items = [
        telebot.types.InlineKeyboardButton(text=Buttons.APPLY, callback_data=default_data + 'apply'),
        telebot.types.InlineKeyboardButton(text=Buttons.REJECT, callback_data=default_data + 'cancel')
    ]
    keyboard.row(*items)
    return keyboard


def get_keyboard_for_choice_lesson(teacher, lessons) -> types.InlineKeyboardMarkup:
    """
    Формирование клавиатуры для выбора занятий
    :param teacher: Преподаватель, для которого строится клавиатура
    :param lessons: Список занятий
    :return:
    """
    default_data = f'choice_lesson:{teacher.id}:'

    keyboard = telebot.types.InlineKeyboardMarkup()
    for lesson in lessons:
        lesson_info = f'{lesson.location.name} ' \
                      f'({lesson.start.strftime("%H:%M")}-' \
                      f'{lesson.end.strftime("%H:%M")})'
        keyboard.add(telebot.types.InlineKeyboardButton(text=lesson_info,
                                                        callback_data=f'{default_data + f"{lesson.id}"}'))

    return keyboard


def get_message_for_check(log) -> str:
    """
    Формирование сообщение, которое отправляется админисраторам и техникам при проверке
    :param log:
    :return:
    """
    # Данные для генерации ссылки для просмотра фотографий отчета
    base_url = settings.ALLOWED_HOSTS[0]

    base_iv_url = 'https://t.me/iv?url=https%3A%2F%2F' + base_url
    hash_iv = '&rhash=3270308b3cce04'
    view_gallery_url = '/api/bot/views/gallery/'

    # Формуриуем часть сообщения, для просмотра фото
    view_image_part = f'Для просмотра фотографий перейдите по ' \
                      f'<a href="{base_url}{view_gallery_url}{log.images.id}/">ссылке</a>\n'

    if isinstance(log, Preparation):
        main_part = f'🎓 Преподаватель {log.teacher.full_name} прислал(а) отчёт о подготовке\n'
    elif isinstance(log, SortingLog):
        main_part = f'🎓 Преподаватель {log.teacher.full_name} прислал(а) отчёт о сортировке\n' \
                    f'🏢 Локация: {log.location.name}'
    elif isinstance(log, LessonLog):
        main_part = f'🎓 Преподаватель {log.teacher.full_name} прибыл(а)\n' \
                    f'🏢 Локация: {log.location.name}\n' \
                    f'⌚ Время прибытия: {log.timestamp.strftime("%H:%M")}\n' \
                    f'⏳ Начало занятия в {log.lesson.start.strftime("%H:%M")}\n'
    elif isinstance(log, PhotoLog):
        main_part = f'Преподаватель {log.teacher.full_name} прислал(а) фотоотчёт с занятия\n'
    else:
        main_part = ""

    return main_part + view_image_part


def send_log_for_check(log):
    """
    Отправка отчёта на проверку через Telegram администраторам или техникам
    :param log:
    :return:
    """
    # Если это отчёт подготовки или занятия, отправляем его администраторам
    admin_role = Role.objects.get(name='Администратор')
    technician_role = Role.objects.get(name='Техник')

    admin_query = Q(roles=admin_role)
    technician_query = Q(roles=technician_role)

    # Подготавливаем запрос для получение всех авторизованных и активных пользователей франчайзи
    users = User.objects.exclude(chat_id=0).filter(cities=log.franchisee, is_active=True)

    receivers, message = None, None
    # Получаем сообщение с информацией об отчёте
    message = get_message_for_check(log)
    message = f'Город: {log.franchisee.name}\n' + message
    keyboard = None
    if isinstance(log, Preparation):
        receivers = users.filter(admin_query
                                 | Q(usergroup__notifications__contains=[NotificationsMixin.PREPARATIONS]))
        tg_groups = TelegramGroup.objects.filter(franchisee=log.franchisee,
                                                 notifications__contains=[NotificationsMixin.PREPARATIONS])

    # Если это отчет о сортировке, отправляем его администраторам и техникам
    elif isinstance(log, SortingLog):
        receivers = users.filter(Q(roles=technician_role)
                                 | Q(usergroup__notifications__contains=[NotificationsMixin.SORTINGS]))
        tg_groups = TelegramGroup.objects.filter(franchisee=log.franchisee,
                                                 notifications__contains=[NotificationsMixin.SORTINGS])
        keyboard = get_keyboard_for_check_logs(log)

    # Если это отчет о приходе на локацию, отправляем его администраторам
    elif isinstance(log, LessonLog):
        if log.location.inspectors.all().exists():
            receivers = log.location.inspectors.all()
        else:
            receivers = users.filter(admin_query |
                                     Q(usergroup__notifications__contains=[NotificationsMixin.ARRIVAL]))
        tg_groups = TelegramGroup.objects.filter(franchisee=log.franchisee,
                                                 notifications__contains=[NotificationsMixin.ARRIVAL])

    elif isinstance(log, PhotoLog):
        receivers = users.filter(admin_query)

    # Получаем список идентификаторов для рассылки и отправляем уведомление
    for user in set(receivers.values_list('chat_id', flat=True)):
        try:
            bot.send_message(user, message, reply_markup=keyboard, parse_mode='HTML')
        except apihelper.ApiException as e:
            pass
    for group in set(tg_groups.values_list('chat_id', flat=True)):
        try:
            bot.send_message(group, message, parse_mode='HTML')
        except apihelper.ApiException as e:
            pass


def get_message_for_result_check(log) -> str:
    """
    Формирование сообщения для преподавателя о результате проверки
    :param log:
    :return:
    """

    # Получаем текстовое название отчёта
    log_type = log.get_name()

    if log.status == 1:
        status = 'принят'
        sign = '✅'
    elif log.status == 2:
        status = 'отклонён'
        sign = '❌'
    else:
        return None

    return f'{sign} Отчёт {status}\n' \
           f'📝 {log_type} от {log.timestamp.strftime("%d.%m.%Y")}'


def send_messages(users: list, message: str, parse_mode='HTML') -> dict:
    """
    Рассылка сообщений через бота
    :param id_users: Список идентификаторов пользователей
    :param message: Сообщение для рассылки
    """
    # Если передан список или кортеж
    if isinstance(users, (list, tuple)):
        # Получаем пользователей по списку id
        users = User.objects.filter(id__in=users)
    elif isinstance(users, (QuerySet, TelegramGroup)):
        pass
    else:
        raise ValueError(f'Недопустимый тип users ({type(users)})')

    result = {'successful': [], 'unsuccessful': [], 'message_info': {}}
    # Рассылаем сообщание и сохраняем информацию об отправке
    for user in users:
        try:
            msg = bot.send_message(user.chat_id, message, parse_mode=parse_mode)
            result['message_info'][msg.chat.id] = msg.message_id
            result['successful'].append(user.full_name)
        except telebot.apihelper.ApiException:
            result['unsuccessful'].append(user.full_name)
    return result


def get_status(chat_id: int) -> enum.Enum:
    """Получение статуса пользователя"""
    return cache.get(f'status:{chat_id}')


def set_status(chat_id: int, status: enum.Enum):
    """Установка статуса для пользователя"""
    cache.set(f'status:{chat_id}', status, timeout=None)


@bot.message_handler(commands=['start'])
def processing_start(message):
    """
    Обработка начала работы с ботом и проверка наличия токена в команде /start
    :param message: Сообщение
    :return:
    """
    # Получаем идентификатор пользователя в Telegram
    chat_id = message.chat.id

    # Проверяем, авторизовани ли пользователь
    if User.objects.filter(chat_id=chat_id).exists():
        bot.send_message(chat_id, 'Вы уже авторизованы под другим аккаунтом')
        return

    user = utils.check_auth(chat_id)
    # Проверяем, авторизован ли пользователь в системе
    if user:
        keyboard = None
        if user.get_status() == statuses.NOTHING:
            keyboard = KeyboardHelper.get_main_menu()
        bot.send_message(chat_id, 'Вы уже авторизованы в системе',
                         reply_markup=keyboard)
        return None

    # Проверяем, содержит ли сообщение токен
    words = message.text.split()
    if len(words) == 2:
        user = auth_user(chat_id, words[1])
        if user:
            bot.send_message(chat_id, 'Вы успешно авторизовались в боте:)',
                             reply_markup=KeyboardHelper.get_main_menu()
                             )
        else:
            bot.send_message(chat_id, 'Неверный токен',
                             reply_markup=KeyboardHelper.get_keyboard_markup())
    else:
        bot.send_message(chat_id, 'Для входа в систему введите токен доступа',
                         reply_markup=KeyboardHelper.get_keyboard_markup())
    return None


@bot.message_handler(commands=['info'])
def processing_info(message):
    """Получение отладочной информации"""
    chat_id = message.chat.id
    user = User.objects.get(chat_id=chat_id)
    server_now = datetime.datetime.now()
    local_now = utils.get_localtime(user)
    status = get_status(chat_id)
    test_setting = DefaultSetting.get_setting('test_setting', user.franchisee)
    bot.send_message(chat_id, f'Время на сервере: {server_now}\n'
                              f'Локальное время: {local_now}\n'
                              f'Временная зона: {user.franchisee.timezone}\n'
                              f'Статус: {status}\n'
                              f'Тестовая настройка {test_setting}\n'
                              f'TEMP {cache.get(f"temp:{chat_id}")} ')


@bot.message_handler(commands=['kpi'])
@bot.message_handler(func=lambda msg: msg.text == Buttons.ADDITIONAL)
@utils.auth_required(bot)
def processing_show_additional_menu(message):
    user = User.objects.get(chat_id=message.chat.id)

    # Если у пользователя несколько городов
    # то предлагаем выбрать нужный
    if user.cities.all().count() > 1:
        items = []
        for city in user.cities.all():
            items.append((city.name, KeyboardHelper.get_string_for_params('choice_city', city.id)))

        items.append(KeyboardHelper.CANCEL_INLINE)
        keyboard = KeyboardHelper.get_inline_keyboard(items)
        msg = 'Выберите нужный город из списка'
    else:
        keyboard = KeyboardHelper.get_additional_menu(message.user)
        msg = 'Дополнительное меню'
        user.set_city(user.cities.first())
    bot.send_message(message.chat.id, msg, reply_markup=keyboard)


@bot.message_handler(commands=['show'])
@utils.auth_required(bot)
def show_main_menu(message):
    bot.send_message(message.chat.id, 'Показ клавиатуры', reply_markup=KeyboardHelper.get_main_menu())


@bot.message_handler(commands=['schedule'])
@bot.message_handler(func=lambda msg: msg.text == Buttons.MEETINGS)
@utils.auth_required(bot)
def processing_schedule(message):
    chat_id = message.chat.id
    teacher = User.objects.get(chat_id=chat_id)
    pass


@bot.message_handler(commands=['cancel'])
@bot.message_handler(func=lambda msg: msg.text == Buttons.CANCEL)
@utils.auth_required(bot)
def processing_cancel(message):
    """
    Удаление временных данных
    :param message:
    :return:
    """
    chat_id = message.chat.id
    cache.delete_pattern(f'*{chat_id}')
    TaskHelper.set_temp_task(chat_id, dict())
    bot.send_message(chat_id, 'Операция отменена, все временные данные удалены',
                     reply_markup=KeyboardHelper.get_main_menu())
    set_status(chat_id, statuses.NOTHING)


@bot.message_handler(commands=['list'])
@bot.message_handler(func=lambda msg: msg.text == Buttons.LOCATION)
@utils.auth_required(bot)
def processing_list(message):
    """
    Получение списка локаций
    :param message:
    :return:
    """
    chat_id = message.chat.id
    franchisee = User.objects.get(chat_id=chat_id).franchisee
    locations = Location.objects.filter(franchisee=franchisee, is_active=True)
    locations_list = "<b>Список локаций:\n</b>"
    for location in locations:
        locations_list += f'{location.id} - {location.name}\n'

    bot.send_message(chat_id, locations_list, parse_mode='HTML')


@bot.message_handler(func=lambda msg: msg.text == Buttons.CABINET_LOG)
@utils.auth_required(bot)
def show_choice_location_keyboard(message):
    chat_id = message.chat.id

    user = message.user
    # Получаем текущее время
    now = user.get_city().get_localtime()
    if not user.get_status() in (statuses.CHOICE_TYPE_REPORT,):
        bot.send_message(chat_id, 'Требуется загрузить фото')
        return
    # Ищем занятия на сегодняшнюю дату для преподавателя
    today_lessons = Lesson.objects.filter(date=now.date(), is_active=True, teachers=user)
    keyboard = KeyboardHelper.get_main_menu()

    # Оставляем ближайшие занятия
    current_lessons = utils.get_current_lessons(today_lessons, now, 120)

    today_logs = LessonLog.objects.filter(teacher=user, timestamp__date=now.date()).order_by('timestamp')
    now = user.get_city().get_localtime()

    temp_data = cache.get(f'temp:{chat_id}')
    log_timestamp = temp_data['created']

    # Проверяем, если прошло больше 15 минут, то ставим текущее время
    limit_now = now - timedelta(minutes=15)
    if log_timestamp < limit_now:
        log_timestamp = now

    temp_data['created'] = log_timestamp
    cache.set(f'temp:{chat_id}', temp_data)

    # Если сегодня уже были отправлены отчеты
    if today_logs.count():
        last_log = today_logs.last()
        # Если есть ближайшие занятия
        if current_lessons.count() == 1 and current_lessons.first().location == today_logs.last().location:
            msg = save_temp_data(chat_id, 'lesson', last_log.lesson.location, last_log.lesson)
            user.set_status(statuses.NOTHING)
        elif current_lessons:
            last_log = today_logs.last()
            current_lesson = current_lessons.first()
            keyboard = get_keyboard_for_choice_lesson(user, [last_log.lesson] + list(current_lessons))
            msg = 'Найдено несколько подходящих занятий, ' \
                  'выберите нужное'
            user.set_status(statuses.CHOICE_LESSON)

        else:
            lesson = last_log.lesson
            msg = save_temp_data(chat_id, 'lesson', lesson.location, lesson)
            user.set_status(statuses.NOTHING)
    else:
        if current_lessons:
            if current_lessons.count() == 1:
                lesson = current_lessons.first()
                msg = save_temp_data(chat_id, 'lesson', lesson.location, lesson)
                user.set_status(statuses.NOTHING)
            else:
                msg = 'Найдено несколько подходящих занятий, ' \
                      'выберите нужное'
                keyboard = get_keyboard_for_choice_lesson(user, current_lessons)
                user.set_status(statuses.CHOICE_LESSON)
        else:
            msg = 'В ближайшее время занятий не найдено, ' \
                  'для решения проблемы свяжитесь с вашим учебно-методическим отделом'
            cache.set(f'temp:{chat_id}', {})
            user.set_status(statuses.NOTHING)

    bot.send_message(chat_id, msg, reply_markup=keyboard)


@bot.message_handler(func=lambda msg: msg.text == Buttons.BACK)
@utils.auth_required(bot)
def processing_back(message):
    user = message.user
    if user.get_status() == statuses.CHOICE_LESSON_FOR_PHOTO_FILE:
        keyboard = KeyboardHelper.get_keyboard_markup((Buttons.PHOTO_LOG, (Buttons.CANCEL)))
        new_status = statuses.CHOICE_TYPE_REPORT_FOR_FILE
    else:
        new_status = statuses.CHOICE_TYPE_REPORT
        keyboard = KeyboardHelper.get_type_report_menu()

    user.set_status(new_status)
    bot.send_message(user.chat_id, 'Выберите нужный тип отчёта', reply_markup=keyboard)


@bot.message_handler(func=lambda msg: msg.text == Buttons.NEW_TASK)
@utils.auth_required(bot)
def processing_new_task(message):
    """Создание новой задачи"""
    chat_id = message.chat.id
    user = User.objects.get(chat_id=chat_id)

    set_status(chat_id, task_statuses.CHOICE_DEPARTMENT)
    TaskHelper.set_temp_task(chat_id, dict())
    # Отправляем клавиатуру с выбором отдела
    bot.send_message(chat_id, 'Создание задачи', reply_markup=KeyboardHelper.get_keyboard_markup())
    if user.cities.count() > 1:
        cities = user.cities.all()
        keyboard = TaskHelper.get_cities_keyboard(cities)
        bot.send_message(chat_id, 'Выберите город', reply_markup=keyboard)
    else:
        keyboard = TaskHelper.get_department_keyboard(user, user.get_one_city().id)
        bot.send_message(chat_id, 'Выберите отдел, для которого хотите поставить задачу',
                         reply_markup=keyboard)


@bot.message_handler(func=lambda msg: msg.text == Buttons.LIST_TASK)
@utils.auth_required(bot)
def processing_list_task(message):
    """Показ списка задач по категориям"""
    user = User.objects.get(chat_id=message.chat.id)
    keyboard = TaskHelper.get_task_menu(user)

    bot.send_message(user.chat_id, 'Выберите нужный пункт меню для просмотра списка задач',
                     reply_markup=keyboard)


@bot.message_handler(content_types=['photo', 'video', 'document'])
@utils.auth_required(bot)
def processing_files(message):
    """
    Обработка поступающих файлов

    Хранение информации об времени отправки и отправленных файлов
    осуществляется с помощью Redis хранилища
    """

    user = message.user
    # Получаем идентификатор файла, в зависимости от типа
    # отправленного файла
    if message.document:
        file = message.document
        file_type = 'Document'
    elif message.photo:
        file = message.photo[-1]
        mime_type = 'image/jpeg'
        file_type = 'Photo'
    elif message.video:
        file = message.video
        file_type = 'Video'

    file_id = file.file_id

    # Если у файла есть mime_type
    if hasattr(file, 'mime_type'):
        mime_type = file.mime_type

    file_size = file.file_size

    # Проверяем, что загрузка файла возможна
    if mime_type not in available_mime_types:
        bot.reply_to(message, f'Загрузка файла с данным форматом {mime_type} не поддерживается')
        return

    if file_size > 46 * 1024 * 1024:
        bot.reply_to(message, f'Размер файла не может превышать 50МБ ({round(file_size / 1024 / 1024, 2)})')
        return

    user_status = user.get_status()

    # Если у пользователя неккоректный статус, то меняем его на нужный нам
    if user_status not in (statuses.CHOICE_TYPE_REPORT_FOR_FILE,
                           statuses.CHOICE_TYPE_REPORT,
                           statuses.CHOICE_CITY):
        user_status = statuses.NOTHING

    if user_status == statuses.NOTHING or user_status is None:
        temp_data = {'created': user.franchisee.get_localtime(), 'files': list()}
        cache.set(f'temp:{user.chat_id}', temp_data, timeout=None)

        if user.has_many_cities():
            keyboard = KeyboardHelper.get_choice_city_menu(user)
            msg = 'Выберите город, к которому относится ваш отчет'
            user.set_status(statuses.CHOICE_CITY)
        else:
            msg = 'Дождитесь окончания загрузки файлов'
            user.set_city(user.get_one_city())
            user.set_status(statuses.CHOICE_TYPE_REPORT)
            keyboard = KeyboardHelper.get_type_report_menu(user)

        bot.send_message(
            user.chat_id,
            msg,
            reply_markup=keyboard,
        )

    if file_type in ['Document', 'Video'] and user_status == statuses.CHOICE_TYPE_REPORT:
        user.set_status(statuses.CHOICE_TYPE_REPORT_FOR_FILE)

    keyboard = keyboard = KeyboardHelper.get_type_report_menu(user)

    # Получаем информацию о файле
    try:
        file_info = bot.get_file(file_id)
    except ApiException as e:
        bot.reply_to(message, f'При загрузке файла произошла ошибка {e}')
        return
    # Обновляем временные данные
    temp_data = cache.get(f'temp:{user.chat_id}')
    # Заносим во временные данные путь к фото на сервере Telegram и сохраняем временную запись
    try:
        temp_data['files'].append(FileInfo(file_info.file_path, mime_type))
        cache.set(f'temp:{user.chat_id}', temp_data, timeout=None)

        bot.reply_to(message, 'Файл сохранен', reply_markup=keyboard)
    except KeyError:
        bot.send_message(user.chat_id, 'При загрузке фото произошла ошибка, '
                                       'введите /cancel и повторите попытку')


@bot.message_handler(content_types=['text'],
                     func=lambda msg: msg.chat.type == 'private' and msg.text
                                      and len(msg.text) == Token.TOKEN_LENGTH * 2)
def check_token(message):
    """
    Проверка токена
    :param message:
    :return:
    """
    chat_id = message.chat.id
    # Проверяем, авторизован ли пользователь в системе
    # Если пользователь авторизован,
    # вызываем обработку входящего сообщения
    if User.objects.filter(chat_id=chat_id).count():
        processing_text(message)
        return None
    user = auth_user(chat_id, message.text)
    if user:
        bot.send_message(chat_id, f'Добро пожаловать, {user.full_name}.',
                         reply_markup=KeyboardHelper.get_main_menu())
    else:
        bot.send_message(chat_id, 'Неверный токен доступа, для решение проблемы обратитесь к администратору')
    return None


@bot.message_handler(content_types=['text'], func=lambda msg: msg.chat.type == 'private')
@utils.auth_required(bot)
def processing_text(message):
    """
    Обработка текстовых сообщений от пользователя
    :param message:
    :return:
    """
    chat_id = message.chat.id
    text = message.text
    user = message.user
    # Текущее время
    now = timezone.localtime(timezone.now(), pytz.timezone(message.user.franchisee.timezone))
    # Клавиатура и сообщения по умолчанию
    keyboard = KeyboardHelper.get_main_menu()
    msg = f'Что-то пошло не так {user.get_status()}'

    # Получаем статус преподавателя
    teacher_status = user.get_status()

    # Если статус пользователя связан с задачами
    # то передаем сообщение объекту для работы с задачами
    parse_mode = None
    if teacher_status in task_statuses:
        task_processing.proc_message(message, user)
        return
    # Обрабатываем отправку отчета
    elif teacher_status in (statuses.CHOICE_TYPE_REPORT, statuses.CHOICE_TYPE_REPORT_FOR_FILE):
        if text == Buttons.PREPARE_LOG:
            msg = save_temp_data(chat_id, 'prepare')

        elif text == Buttons.SORTING_LOG:
            user.set_status(statuses.INPUT_LOCATION_NAME)
            msg = 'Введите/Выберите название локации, на котором вы сортировали наборы'
            lessons = utils.get_last_locations(user, 6)
            items = list(set([lesson.location.name for lesson in lessons]))
            items.append((Buttons.LOCATION, Buttons.BACK))
            keyboard = KeyboardHelper.get_keyboard_markup(items)

        elif text == Buttons.PHOTO_LOG:
            # Получаем занятия на это недели на локациях

            lessons = utils.get_last_lessons(user, 6)
            items = [f'{lesson.date.strftime("%d.%m.%Y")} ' \
                     f'({lesson.start.strftime("%H:%M")} - {lesson.end.strftime("%H:%M")}) ' \
                     f'{lesson.location.name}' for lesson in lessons]
            items.append((Buttons.CANCEL, Buttons.BACK))

            keyboard = KeyboardHelper.get_keyboard_markup(items)
            if teacher_status == statuses.CHOICE_TYPE_REPORT_FOR_FILE:
                user.set_status(statuses.CHOICE_LESSON_FOR_PHOTO_FILE)
            else:
                user.set_status(statuses.CHOICE_LESSON_FOR_PHOTO)
            msg = 'Выберите нужное занятие из списка'

    elif teacher_status == statuses.INPUT_LOCATION_NAME:
        location = utils.find_location(user.franchisee, text)
        if location:
            msg = save_temp_data(chat_id, 'sorting', location)
        else:
            keyboard = None
            msg = f'Локация {text} не найден, посмотреть список локаций /list'

    elif teacher_status == statuses.CHOICE_CITY:
        keyboard = None
        try:
            city = user.cities.get(name=text)
            user.set_city(city)
            user.set_status(statuses.CHOICE_TYPE_REPORT)

            msg = 'Дождитесь окончания загрузки фотографий и выберите тип отчета'
            keyboard = KeyboardHelper.get_type_report_menu(user)

        except Franchisee.DoesNotExist:
            msg = 'Город не найден или вы не привязаны к нему'

    elif teacher_status == statuses.CHOICE_LOCATION_FOR_DETAIL_ORDER:

        location = utils.find_location(user.get_city(), text)

        now = user.get_city().get_localtime()
        try:
            detail_order = DetailsOrder.objects.get(created__date=now.date(), teacher=user,
                                                    status=0, franchisee=user.get_city(), location=location)
        except DetailsOrder.DoesNotExist:
            detail_order = DetailsOrder.objects.create(
                teacher=user,
                created=now,
                franchisee=user.get_city(),
                location=location
            )

        callback_url = settings.API_HOST + f'/api/bot/journals/orders/webhook/{detail_order.id}/'

        url = settings.ORDER_HOST + f'?callback={callback_url}'
        msg = ('Для заказа деталей перейдите по ссылке\n'
               'ВНИМАНИЕ: При заказе указывается фактическое количество деталей в наборе\n'
               f'{url}')

    elif teacher_status in (statuses.CHOICE_LESSON_FOR_PHOTO, statuses.CHOICE_LESSON_FOR_PHOTO_FILE):
        # Выбор занятия для отправки фото на Google диск
        result = re.search(r'^([\d.]*) \(([\d:]*) - ([\d:]*)\) ([\w\W]*)', text, re.M | re.S | re.U)
        if not result:
            bot.send_message(user.chat_id, 'Неверный формат ввода занятия, выберите занятие с помощью клавиатуры')
            return
        date, lesson_start, lesson_end, location_name = result.groups()

        date = datetime.datetime.strptime(date, "%d.%m.%Y")
        lesson_start = datetime.datetime.strptime(lesson_start, '%H:%M').time()
        lesson_end = datetime.datetime.strptime(lesson_end, '%H:%M').time()
        # Ищем занятие по полученным данным
        try:
            lesson = Lesson.objects.filter(teachers=user, date=date,
                                           start=lesson_start, end=lesson_end, is_active=True).first()
        except Exception as e:
            bot.send_message(user.chat_id, e)
            return

        # Получаем временные данные и передаем их для постановки задачи
        temp_data = cache.get(f'temp:{chat_id}')
        try:
            location = Location.objects.get(name=location_name, is_active=True)
            # Запускаем фоновую задачу на добавление фотографий
            # на Google диск
            upload_photo_on_google_drive.delay(
                user.franchisee.id,
                temp_data['files'],
                lesson.id,
                user.id,
            )

            cache.delete_pattern(f'*{chat_id}')
            user.set_status(statuses.NOTHING)
            msg = 'Файлы загружаются на Google диск'
        except Location.DoesNotExist:
            keyboard = None
            msg = 'Не удалось найти указанную вами локацию'

    elif text == Buttons.SEND_REPORT:
        msg = 'Дла отправки отчета:\n*- Фото/видео кабинета\n- Фото с занятий\n- Подготовка\n' \
              '- Сортировка*\n выберите нужные файлы и отправьте их боту'
        parse_mode = 'Markdown'
    bot.send_message(chat_id, msg, reply_markup=keyboard, parse_mode=parse_mode)


@bot.message_handler(content_types=['text'], func=lambda msg: msg.chat.type in ('group', 'supergroup', 'channel'))
def processing_text_from_group(message):
    """Обработка сообщений, поступающих от групп из Telegram"""
    import uuid

    token = message.text
    chat_id = message.chat.id
    try:
        uuid.UUID(token)
    except ValueError:
        return

    if not TelegramGroup.objects.filter(franchisee__uuid=token, chat_id=chat_id).exists():
        try:
            city = Franchisee.objects.get(uuid=token)
            tg_chat = TelegramGroup.objects.create(
                franchisee=city,
                chat_id=chat_id,
                type=TelegramGroup.GROUP,
                name=message.chat.title,
            )
        except Franchisee.DoesNotExist:
            pass
    bot.delete_message(chat_id, message.message_id)


@bot.channel_post_handler(func=lambda post: True)
def processing_text_from_channel(message):
    """Обработка сообщений, поступающих от канала Telegram"""
    import uuid
    token = message.text
    chat_id = message.chat.id
    try:
        uuid.UUID(token)
    except ValueError:
        return

    if not TelegramGroup.objects.filter(franchisee__uuid=token, chat_id=chat_id).exists():
        try:
            city = Franchisee.objects.get(uuid=token)
            tg_channel = TelegramGroup.objects.create(
                franchisee=city,
                chat_id=chat_id,
                type=TelegramGroup.CHANNEL,
                name=message.chat.title,
            )
        except Franchisee.DoesNotExist:
            pass
    bot.delete_message(chat_id, message.message_id)


@bot.callback_query_handler(func=lambda call: True)
@utils.auth_required(bot, message_type='call')
def test_callback(call: types.CallbackQuery):
    """
    Обработка call вызова от кнопки
    :param call:
    :return:
    """
    chat_id = call.message.chat.id
    message_id = call.message.message_id
    bot.answer_callback_query(call.id)

    # Получаем параметры запроса, которые разделяются двоеточием
    data_params = call.data.split(':')
    query_type = data_params[0]  # Тип запроса
    # Если статус пользователя связан с задачами
    # то передаем сообщение объекту для работы с задачами
    user_status = get_status(chat_id)
    user = call.user
    timezone.activate(user.franchisee.timezone)
    back_to_additional_menu = KeyboardHelper.get_inline_keyboard(
        (
            (Buttons.BACK, 'show_additional_menu'),
        )
    )
    print('Тип inline запроса', query_type)
    if query_type == 'task':
        task_processing.proc_message(call, user)

    # Если запрос на проверку
    elif query_type == 'check':
        log_type = data_params[1]  # Тип лога
        log_id = data_params[2]  # Идентификатор отчета
        action = data_params[3]  # Действие (Принять/Отклонить)

        # Получаем нужный лог, в зависимости от его типа
        if log_type == 'prepare':
            log = Preparation.objects.get(id=log_id)
        elif log_type == 'sorting':
            log = SortingLog.objects.get(id=log_id)
        elif log_type == 'lesson':
            log = LessonLog.objects.get(id=log_id)
        elif log_type == 'photo':
            log = PhotoLog.objects.get(id=log_id)
        else:
            log = None

        # Если отчет уже проверен, то сообщаем об этом
        if log.status != 0:
            bot.answer_callback_query(callback_query_id=call.id,
                                      text=f'Данный отчет уже был проверен')
            bot.delete_message(chat_id, call.message.message_id)
            return
        if action == 'apply':
            log.apply()
        elif action == 'cancel':
            log.cancel()

        bot.answer_callback_query(callback_query_id=call.id,
                                  text=f'Отчет проверен')
        bot.delete_message(chat_id, call.message.message_id)
        log.save()

        # Сообщаем преподавателю о том, что его отчет проверен
        bot.send_message(log.teacher.chat_id, get_message_for_result_check(log))
    # Если тип запроса выбор занятия
    elif query_type == 'choice_lesson':
        # Получаем нужные объекты для создания отчета прихода/ухода
        lesson_id = data_params[2]
        temp_data = cache.get(f'temp:{chat_id}')
        lesson = Lesson.objects.get(id=lesson_id)
        # Сохраняем данные и отправляем преподавателю сообщение о результате
        message = save_temp_data(chat_id, 'lesson', location=lesson.location, lesson=lesson)
        bot.delete_message(chat_id, call.message.message_id)
        bot.send_message(chat_id, message)
    # Закрытие меню
    elif query_type == 'close_menu':
        bot.edit_message_text('Выберите дальнейшее действие', chat_id, message_id)
    # Обработка пунктов из дополнительного меню
    elif query_type == 'show_kpi':
        date_start = utils.get_localtime(user).replace(day=1, hour=0, minute=0, second=0)
        date_end = utils.get_localtime(user) + relativedelta(day=31)
        date_end = date_end.replace(hour=23, minute=59, second=59)
        kpi = utils.get_kpi_for_user(date_start, date_end, user)
        kpi_string = f'<b>Информация о KPI</b>\n' \
                     f'KPI за подготовки: {kpi["total_preparations"]}\n' \
                     f'KPI за SMM: {kpi["total_photos"]}\n' \
                     f'KPI за собрания: {kpi["total_meetings"]}\n' \
                     f'Итоговый KPI: {kpi["total_kpi"]} '
        bot.edit_message_text(kpi_string, chat_id, message_id, parse_mode='HTML',
                              reply_markup=back_to_additional_menu)

    # Отображение локаций для пользователя
    elif query_type == 'show_locations':
        locations = Location.objects.filter(franchisee=user.get_city(), is_active=True)
        locations_list = f"<b>Список локаций:</b> [{user.get_city().name}]\n"
        for location in locations:
            locations_list += f'{location.id} - {location.name}\n'

        bot.edit_message_text(locations_list, chat_id, message_id,
                              parse_mode='HTML', reply_markup=back_to_additional_menu)

    elif query_type == 'show_meetings':
        date_start = utils.get_localtime(user)
        date_end = date_start + datetime.timedelta(days=30)
        meetings_dates = MeetingDate.objects.filter(meeting__franchisee=user.get_city(),
                                                    datetime_carried__gte=date_start,
                                                    datetime_carried__lte=date_end).order_by('datetime_carried')
        if meetings_dates:
            meetings_str = f'<b>Расписание собраний</b> [{user.get_city().name}]\n'
        else:
            meetings_str = '<b>В ближайшее время собраний не найдено</b>'
        # Формируем список собраний
        # Переводим время в локальное
        tz = pytz.timezone(user.get_city().timezone)
        for meeting_date in meetings_dates:
            meetings_str += f'{meeting_date.datetime_carried.astimezone(tz).strftime(utils.DATETIME_FORMAT)} - ' \
                            f'{meeting_date.meeting.subject}\n'

        bot.edit_message_text(meetings_str, chat_id, message_id,
                              parse_mode='HTML', reply_markup=back_to_additional_menu)

    elif query_type == 'show_additional_menu':
        keyboard = KeyboardHelper.get_additional_menu(user)
        bot.edit_message_text('Дополнительное меню', chat_id, message_id, reply_markup=keyboard)

    elif query_type == 'order_details':

        # При выборе "Заказать детали"
        # пользователь выбирает локацию,
        # к которой относится заказ
        user.set_status(statuses.CHOICE_LOCATION_FOR_DETAIL_ORDER)

        # Получаем актуальные локации
        msg = 'Введите/Выберите название локации, для которой вы делаете заказ деталей'
        lessons = utils.get_last_locations(user, 6)
        items = list(set([lesson.location.name for lesson in lessons]))
        items.append((Buttons.LOCATION, Buttons.BACK))
        keyboard = KeyboardHelper.get_keyboard_markup(items)

        bot.send_message(user.chat_id, msg, reply_markup=keyboard)

    elif query_type == 'choice_city':
        city = Franchisee.objects.get(api_id=int(data_params[1]))
        user.set_city(city)
        keyboard = KeyboardHelper.get_additional_menu(user)
        bot.edit_message_text(f'Дополнительное меню [{city.name}]', chat_id, message_id, reply_markup=keyboard)

    elif query_type == 'show_references':
        city = user.get_city()
        references = Reference.objects.filter(franchisee=city)
        items = []
        for reference in references:
            if reference.type == 0:
                items.append((reference.title, reference.body, 1))
            else:
                params = KeyboardHelper.get_string_for_params('show_reference', reference.id)
                items.append((reference.title, params, 0))

        items.append(((Buttons.BACK, 'show_additional_menu'), (Buttons.EXIT, 'close_menu')))
        keyboard = KeyboardHelper.get_inline_keyboard(items)
        bot.edit_message_text('Выберите интересующий раздел', chat_id, message_id, reply_markup=keyboard)

    elif query_type == 'show_reference':
        reference_id = int(data_params[1])
        try:
            reference = Reference.objects.get(id=reference_id)
            updated = timezone.localtime(reference.updated, user.get_city().get_timezone())
            msg = f'*{reference.clear_title}*\n\n' \
                  f'{reference.clear_body}\n\n' \
                  f'Обновлено: _{updated.strftime("%d.%m.%Y в %H:%M")}_'

        except Reference.DoesNotExist:
            msg = 'Данная запись была удалена'

        items = [
            ((Buttons.BACK, 'show_references'), (Buttons.EXIT, 'close_menu'))
        ]

        keyboard = KeyboardHelper.get_inline_keyboard(items)
        bot.edit_message_text(msg, chat_id, message_id, reply_markup=keyboard, parse_mode='Markdown')

    elif query_type == 'show_lessons':
        from .helpers import weekdays
        # Получаем занятия на ближайшие две недели
        now = user.get_city().get_localtime()
        end = now + timedelta(days=13)
        lessons = user.lesson_set.filter(franchisee=user.get_city(), date__gte=now.date(),
                                         date__lte=end.date(), is_active=True)
        lessons = lessons.order_by('date')
        msg = '*Ваше расписание на 14 дней*\n\n'

        msg += f'Сегодня: _{now.strftime("%d.%m.%Y")}_\n'
        # Строим расписание по дням недели
        dates = sorted(set(lessons.values_list('date', flat=True)))
        for date in dates:
            lessons_for_date = lessons.filter(date=date)
            msg += f'\n_{date.strftime("%d.%m.%Y")} {weekdays[date.weekday()]}_\n'
            for lesson in lessons_for_date:
                msg += f'{lesson.location.name} - ({lesson.start.strftime("%H:%M")} - {lesson.end.strftime("%H:%M")})\n'

        items = [
            ((Buttons.BACK, 'show_additional_menu'), (Buttons.EXIT, 'close_menu'))
        ]

        keyboard = KeyboardHelper.get_inline_keyboard(items)

        bot.edit_message_text(msg, chat_id, message_id, reply_markup=keyboard, parse_mode='Markdown')

    # Отправка ссылки на личный кабинет
    elif query_type == 'show_profile_link':
        profile_token = user.profile_token

        url = settings.API_HOST + f'/уteacher/{profile_token}'

        bot.send_message(user.chat_id, f'Дла просмотра своих отчетов перейдите по ссылке: {url}')

    elif query_type == 'show_messages':
        from .pagination import BotPagination
        # Получаем сообщения, где пользователь является получателем
        messages = Message.objects.filter(type=0,
                                          receivers__contains=[user.id], is_sent=True).order_by('-sending_datetime')
        paginator = BotPagination(messages, 3, 'show_messages')
        page_number = int(data_params[1])
        page, pagin = paginator.get_page(page_number)
        # Формируем список кнопок с сообщением
        items = []
        for message in page:
            items.append((message.text, f'show_message:{message.id}:{page_number}'))
        if paginator.paginator.num_pages > 1:
            items.append(pagin)
        items.append((
            (Buttons.BACK, 'show_additional_menu'), (Buttons.EXIT, 'close_menu')
        ))
        keyboard = KeyboardHelper.get_inline_keyboard(items)
        bot.edit_message_text(text='Входящие сообщения', chat_id=user.chat_id,
                              message_id=message_id, reply_markup=keyboard)

    elif query_type == 'show_message':
        message_id_ = int(data_params[1])
        page_number = int(data_params[2])

        message = Message.objects.get(id=message_id_)

        message_info = (f'Просмотр сообщения\n\n'
                        f'{message.text}\n\n'
                        f'Отправлено {message.sending_datetime.strftime("%d.%m.%Y в %H:%M")}')
        items = (
            (
                (Buttons.BACK, f'show_messages:{page_number}'),
                (Buttons.EXIT, 'close_menu')
            ),
        )

        keyboard = KeyboardHelper.get_inline_keyboard(items)

        bot.edit_message_text(text=message_info, chat_id=user.chat_id,
                              message_id=message_id, reply_markup=keyboard)
