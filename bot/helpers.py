import enum
from math import ceil
from typing import Union, Optional
from django.conf import settings
from telebot import types

from .models import User

weekdays = {
    0: 'Понедельник',
    1: 'Вторник',
    2: 'Среда',
    3: 'Четверг',
    4: 'Пятница',
    5: 'Суббота',
    6: 'Воскресенье',
}


class Buttons:
    """Класс для хранение кнопок"""

    CANCEL = '❌ Отмена'
    REJECT = '❌ Отклонить'
    EXIT = '❌ Закрыть'
    APPLY = '✅ Принять'
    CONFIRM = '✅ Подтвердить'
    FINISH = '✅ Завершить'
    BACK = '⬅️ Назад'
    KPI = '📈 Мой KPI'
    MEETINGS = '📅 Собрания'
    LESSONS = '🎓 Расписание'
    LOCATION = '🏢 Локации'
    SEND_REPORT = '✉ Отправить отчёт'
    NEW_TASK = '🔔 Новая задача'
    LIST_TASK = '📃 Список задач'
    NO_COMMENT = '⭕ Без комментария'
    TRANSFER_TASK = '📤 Передать задачу'
    CHANGE_DEADLINE = ' 📅 Изменить дедлайн'
    ADDITIONAL = '💡 Прочее'
    ORDER_DETAIL = '🎁 Заказать детали'
    REFERENCE = '📝 Заметки'
    PROFILE = '💼 Личный кабинет'
    MESSAGES = '📩 Сообщения'

    CABINET_LOG = '🖼 Фото кабинета'
    SORTING_LOG = '📦 Сортировка'
    PREPARE_LOG = '📝 Подготовка'
    PHOTO_LOG = '📷 Фото/Видео с занятий'


class KeyboardHelper:
    """Класс для формирования основных клавиатур"""
    MAIN_MENU_ITEMS = (
        Buttons.SEND_REPORT,
        Buttons.ADDITIONAL,
        (Buttons.NEW_TASK, Buttons.LIST_TASK),
    )
    TYPE_REPORT_ITEMS = (
        Buttons.CABINET_LOG,
        (Buttons.SORTING_LOG, Buttons.PREPARE_LOG),
        Buttons.PHOTO_LOG,
        Buttons.CANCEL
    )
    CHOICE_LOCATION_ITEMS = ((Buttons.LOCATION, Buttons.CANCEL),)
    CANCEL_ACTION = (Buttons.CANCEL,)
    CANCEL_INLINE = (Buttons.CANCEL, 'close_menu')
    ADDITIONAL_MENU = (
        (Buttons.LOCATION, 'show_locations'),
        #    (Buttons.KPI, 'show_kpi'),
        (Buttons.MEETINGS, 'show_meetings'),
        (Buttons.ORDER_DETAIL, 'order_details'),
        (Buttons.REFERENCE, 'show_references'),
        (Buttons.LESSONS, 'show_lessons'),
        (Buttons.MESSAGES, 'show_messages:1'),
    )

    @classmethod
    def get_keyboard_markup(
            cls, items: Union[tuple, list] = None,
            resize_keyboard: bool = True,
            selective: bool = False,
    ) -> Union[types.ReplyKeyboardRemove, types.ReplyKeyboardMarkup]:
        """
        Получение markup клавиатуры, принимает набор элементов меню
        в виде кортежа или списка
        :param items: Элементы меню
        :param resize_keyboard: Ресайз клавиатуры
        :param selective: Сокрытие клавиатуры после выбора
        :return: Markup клавиатура
        """

        # Если кортеж пуст, возвращаем объект удаления клавиатуры
        if not items:
            return types.ReplyKeyboardRemove()
        markup = types.ReplyKeyboardMarkup(resize_keyboard=resize_keyboard, selective=selective)
        # Построчное формирование клавиатуры
        for item in items:
            row = []
            if isinstance(item, (list, tuple)):
                for element in item:
                    row.append(element)
            else:
                row.append(item)
            markup.row(*row)
        return markup

    @classmethod
    def get_main_menu(cls) -> types.ReplyKeyboardMarkup:
        """
        Получение markup клавиатуры главного меню
        :return: Markup клавиатура главного меню
        """
        return cls.get_keyboard_markup(cls.MAIN_MENU_ITEMS, selective=True)

    @classmethod
    def get_type_report_menu(cls, user: User = None) -> types.ReplyKeyboardMarkup:
        """
        Получение markup клавиатуры для выбора типа отчета
        :return: Markup клавиатура с типами отчётов
        """
        if user:
            items = []
            if user.get_status() == TeacherStatuses.CHOICE_TYPE_REPORT:
                items = [
                    Buttons.CABINET_LOG,
                    (Buttons.SORTING_LOG, Buttons.PREPARE_LOG),
                ]
                if hasattr(user.get_city(), 'googleinfo') and user.get_city().googleinfo.is_active:
                    items.append(Buttons.PHOTO_LOG)
            elif user.get_status() == TeacherStatuses.CHOICE_TYPE_REPORT_FOR_FILE:
                if hasattr(user.get_city(), 'googleinfo') and user.get_city().googleinfo.is_active:
                    items.append(Buttons.PHOTO_LOG)
            items.append(Buttons.CANCEL)
        else:
            items = cls.TYPE_REPORT_ITEMS
        return cls.get_keyboard_markup(items)

    @classmethod
    def get_choice_city_menu(cls, user: User) -> types.ReplyKeyboardMarkup:
        cities = user.cities.all()
        items = [city.name for city in cities]
        items.append(Buttons.CANCEL)
        return cls.get_keyboard_markup(items)

    @classmethod
    def get_inline_keyboard(cls, items: Optional[Union[tuple, list]] = None) \
            -> Union[types.InlineKeyboardMarkup,
                     types.ReplyKeyboardRemove]:
        """
        Получение inline клавиатуры
        :param items: Список или кортеж содержащий элементы клавиатуры

        Пример:
        [
            ('Строка 11', 'Данные 11', 'Тип кнопки'),
            [
                ('Строка 21', 'Данные 21', 'Тип кнопки'),
                ('Строка 22', 'Данные 22', 'Тип кнопки'),
            ],
            ('Строка 31', 'Данные 31', 'Тип кнопки')
        ]

        Элемент клавиатуры это кортеж из двух/трех элементов
        Пример:
        ('Название кнопки', 'Callback/Url кнопки', 'Тип кнопки': Опционально, по умолчанию 0)

        Тип кнопки: 0 - Кнопка, 1 - URL

        :return: Inline клавиатура
        """
        # Если элементы не переданы, возвращаем объект удаления клавиатуры
        if not items:
            return types.ReplyKeyboardRemove()

        inline_keyboard = types.InlineKeyboardMarkup()
        for item in items:
            row = []
            # Если вложенный элемент является списком
            if isinstance(item[0], (list, tuple)):
                for element in item:
                    if len(element) == 2 or len(element) == 3 and element[2] == 0:
                        row.append(types.InlineKeyboardButton(text=element[0], callback_data=element[1]))
                    elif len(element) == 3 and element[2] == 1:
                        row.append(types.InlineKeyboardButton(text=element[0], url=element[1]))
            else:
                elem = item
                if len(elem) == 2 or len(elem) == 3 and elem[2] == 0:
                    row.append(types.InlineKeyboardButton(text=elem[0], callback_data=elem[1]))
                elif len(elem) == 3 and elem[2] == 1:
                    row.append(types.InlineKeyboardButton(text=elem[0], url=elem[1]))
            inline_keyboard.row(*row)
        return inline_keyboard

    @classmethod
    def get_string_for_params(cls, *args: Union[int, str], sep=':') -> str:
        """
        Получение строки с параметрами для callback_data
        :param sep: Разделитель параметров в строке
        :param args: Параметр или список параметров 

        Пример:
        ('Параметр 1', 'Параметр 2', 3, 'Параметр 4')

        :return: Строка с параметрами для callback_data
        """
        return sep.join(str(x) for x in args)

    @classmethod
    def get_additional_menu(cls, user: User):
        """
        Получение дополнительного меню в виде inline клавиатуры
        """

        url = settings.API_HOST + f'/teacher/{user.profile_token}'
        items = list(cls.ADDITIONAL_MENU)
        items.append((Buttons.PROFILE, url, 1))
        items = cls.list_to_table(items, 2)
        items.append((Buttons.EXIT, 'close_menu'))
        return cls.get_inline_keyboard(items)

    @classmethod
    def list_to_table(cls, list_, col=2) -> list:
        """Преобразования списка в таблицу"""
        row = ceil(len(list_) / col)
        new_list = []
        for i in range(row):
            new_list.append(list_[i * col:i * col + col])
        return new_list


class TeacherStatuses(enum.Enum):
    """Перечисления для статусов пользователя в системе"""
    NOTHING = 0
    CHOICE_TYPE_REPORT = 1
    INPUT_LOCATION_NAME = 2
    CHOICE_LESSON = 3
    CHOICE_TYPE_REPORT_FOR_FILE = 10
    CHOICE_LESSON_FOR_PHOTO = 11
    CHOICE_LESSON_FOR_PHOTO_FILE = 12
    CHOICE_CITY = 13
    CHOICE_LOCATION_FOR_DETAIL_ORDER = 14
