from django.core.paginator import Paginator


class BotPagination:
    """Класс для формаирования пагинаци в боте"""

    NEXT_PAGE = '▶️'
    PREV_PAGE = '◀️'

    def __init__(self, objects, per_page, action):
        """
        Конструктор класса
        :param objects: Объекты для которых строится пагинация
        :param per_page: Количество элементов на странице
        :param action: Действие для inline-клавиатуры
        """
        self.paginator = Paginator(objects, per_page)
        self.action = action

    def get_page(self, number):
        """
        Получение нужной страницы и клавиатуры с кнопками
        :param number: Номер страницы
        :return:
        """
        page = self.paginator.get_page(number)

        keyboard_items = []
        # Информация для вывода информации о пагинации
        page_info = f'Стр. {number}/{self.paginator.num_pages}'

        if page.has_previous():
            keyboard_items.append(
                (self.PREV_PAGE, f'{self.action}:{page.previous_page_number()}')
            )
        else:
            keyboard_items.append(
                (self.PREV_PAGE, f'none')
            )
        keyboard_items.append((page_info, 'none'))
        if page.has_next():
            keyboard_items.append(
                (self.NEXT_PAGE, f'{self.action}:{page.next_page_number()}')
            )
        else:
            keyboard_items.append(
                (self.NEXT_PAGE, f'none')
            )
        # Возвращаем информацию о странице и клавиатуру для
        return page, keyboard_items
