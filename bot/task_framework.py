import enum
from datetime import timedelta
from typing import Union, Optional

import telebot
from dateutil.parser import parse
from django.core.cache import cache
from django.db.models import QuerySet
from django.utils import timezone
from telebot import types

from . import bot as bot_
from .helpers import KeyboardHelper, Buttons
from .models import Task, User, Department


class Statuses(enum.Enum):
    """Класс для статусов пользователя при работе с задачами"""
    INPUT_TASK = 0
    CHOICE_DEADLINE = 1
    CHOICE_DEPARTMENT = 2
    INPUT_CUSTOM_DEADLINE = 3
    INPUT_FINISH_COMMENT = 4
    INPUT_REJECT_COMMENT = 5
    CHANGE_DEADLINE = 6
    INPUT_COMMENT_FOR_CHANGE_DEADLINE = 7
    TRANSFER_TASK = 9


class Actions(enum.Enum):
    """Класс для действий при выборе действия в клавиатуре"""
    SELECT_DEPARTMENT = 0
    SELECT_DEADLINE = 1
    SELECT_EXECUTOR = 2
    CANCEL = 3
    APPLY = 4
    EXIT = 5
    LIST = 7
    BACK = 8
    MENU = 9
    SHOW = 11
    FINISH = 12
    CONFIRM = 13
    REJECT = 14
    CHANGE_DEADLINE = 15
    TRANSFER = 16
    APPLY_NEW_DEADLINE = 17
    REJECT_NEW_DEADLINE = 18
    SELECT_CITY = 19


class Types(enum.Enum):
    """Класс для хранения типов задач"""
    DIRECTOR = 0
    EXECUTOR = 1
    WAIT_CONFIRM = 2
    NEED_CONFIRM = 3
    WAIT_APPLY = 4


class TaskManager:
    """
    Менеджер задач

    Отвечает за все действия, связанные с задачей,
    кроме её создания :)
    """

    def __init__(self, bot: telebot.TeleBot, task: Task, user: User) -> None:
        """Конструктор класса"""
        self.bot = bot
        self.task = task
        self.user = user

    def send_notification(self):
        """Отправка уведомления исоплнителю или отделу"""

        department = self.task.department

        # Рассылка уведомления с принятем задачи для отдела
        # если не назначен конкретный исполнитель
        if not self.task.executor:
            employees = department.employees.exclude(chat_id=0)
            message = f'Поступила новая задача для вашего отдела\n' \
                      f'Постановщик: {self.task.director.markdown_link}\n' \
                      f'Суть задачи: {self.task.clear_text}\n' \
                      f'Дедлайн: {self.task.clear_deadline}\n' \
                      f'Отдел: {department.name}\n [[{department.franchisee.name}]]'

            # TODO Хранение информации об отправленных уведомлениях в Redis
            params = KeyboardHelper.get_string_for_params('task', Actions.APPLY.value, self.task.id)
            buttons = [(Buttons.APPLY, params)]
            keyboard = KeyboardHelper.get_inline_keyboard(buttons)

            # Список для хранения информации об отправленных сообщениях
            msg_list = []
            # Отправляем уведомление
            for receiver in employees:
                msg_info = self.bot.send_message(receiver.chat_id, message, reply_markup=keyboard,
                                                 parse_mode='Markdown')
                msg_list.append({'message_id': msg_info.message_id, 'chat_id': msg_info.chat.id})

        # Рассылка уведомления для конкретного исполнителя
        else:
            message = f'{self.task.director.markdown_link} поставил(а) вам задачу\n' \
                      f'Суть задачи: {self.task.clear_text}\n' \
                      f'Дедлайн: {self.task.clear_deadline}\n' \
                      f'Отдел: {department.name} [[{department.franchisee.name}]]'

            self.bot.send_message(self.task.executor.chat_id, message,
                                  parse_mode='Markdown')

    def apply(self, message: Optional[types.Message] = None) -> None:
        """Принятие задачи пользователем"""

        keyboard = KeyboardHelper.get_inline_keyboard()
        # Проверяем, что задача еще не принята
        if self.task.executor:
            msg = f'Данную задачу уже принял {self.task.executor}'
        else:
            self.task.executor = self.user
            self.task.status = 1
            self.task.save()
            # TODO Удаление сообщений у других сотрудников отдела
            msg = f'Вы приняли задачу {self.task.text}'

            message_for_director = f'{self.user.markdown_link} из отдела {self.task.department.name}' \
                                   f' принял(а) вашу задачу "{self.task.clear_text}"'

            self.bot.send_message(self.task.director.chat_id, message_for_director, parse_mode='Markdown')

        if message:
            self.bot.edit_message_text(msg, message.chat.id, message.message_id)

    def finish(self, message: Optional[types.Message] = None, comment: Optional[str] = None) -> None:
        """Завершение задачи"""

        items = []
        # Если завершает постановщик, то просто закрываем задачу
        # И отправляем уведомление исполнителю
        if self.user == self.task.director:
            self.task.status = 3

            # Если самозадача
            if self.task.executor == self.user:
                self.bot.edit_message_text('Задача завершена', message.chat.id, message.message_id)
                self.task.save()
                return

            message_one = f'{self.task.director.markdown_link} отметил(а) задачу ' \
                          f'"{self.task.clear_text}" как завершенную'
            message_two = f'Задача "{self.task.clear_text}" завершена'
            receiver = self.task.executor
        # Иначе отправляем задачу на подтверждение постановщику
        else:
            self.task.status = 2
            comment = comment if comment else 'Отсутствует'
            message_one = f'Задача "{self.task.clear_text}" отправлена на подтверждение'
            message_two = f'{self.user.markdown_link} отправил(а) ' \
                          f'задачу "{self.task.clear_text}" на подтверждение\n' \
                          f'Комментарий: {comment}'
            receiver = self.task.director
            items.append(
                (
                    (Buttons.APPLY, KeyboardHelper.get_string_for_params('task', Actions.CONFIRM.value, self.task.id)),
                    (Buttons.REJECT, KeyboardHelper.get_string_for_params('task', Actions.REJECT.value, self.task.id)),
                )
            )

        # Редактируем сообщение у того, кто закрывает задачу
        if message:
            self.bot.edit_message_text(message_one, message.chat.id, message.message_id,
                                       parse_mode='Markdown')
        else:
            self.bot.send_message(self.user.chat_id, message_one,
                                  parse_mode='Markdown')

        # Сообщение исполнителю или постановщику
        if receiver:
            keyboard = KeyboardHelper.get_inline_keyboard(items)
            self.bot.send_message(receiver.chat_id, message_two, reply_markup=keyboard,
                                  parse_mode='Markdown')

        self.task.save()

    def confirmation(self, message: Optional[types.Message] = None) -> None:
        """Подтверждение запроса на завершения задачи"""
        self.task.status = 3
        self.task.save()

        if message:
            self.bot.edit_message_text(f'Задача: "{self.task.clear_text}" завершена',
                                       message.chat.id, message.message_id)

        message_for_executor = f'{self.task.director.markdown_link} подтвердил(а) ' \
                               f'выполнение задачи "{self.task.clear_text}"'

        # Отправляем уведомление о том, что выполнение задачи подтвердили
        self.bot.send_message(self.task.executor.chat_id, message_for_executor,
                              parse_mode='Markdown')

    def reject(self, message: Optional[types.Message] = None) -> None:
        """Отклонение запроса на завершение задачи"""
        self.task.status = 1
        self.task.save()

        if message:
            self.bot.edit_message_text(f'Вы отклонили завершение задачи "{self.task.clear_text}"',
                                       message.chat.id, message.message_id)

        message_for_executor = f'{self.task.director.markdown_link} отклонил(а) ' \
                               f'выполнение задачи "{self.task.clear_text}"'
        # Отправляем уведомление о том, что выполнение задачи отклонили
        self.bot.send_message(self.task.executor.chat_id, message_for_executor, parse_mode='Markdown')

    def transfer(self, new_executor: User = None,
                 new_department: Department = None,
                 comment: str = None,
                 message: types.Message = None) -> None:
        """Передача задачи новому исполнителю или отделу"""

        comment = comment if comment else 'Отсутствует'

        if new_executor and new_department:
            raise AttributeError('Переданы взаимоисключающие параметры')

        old_executor = self.task.executor
        if new_department:

            self.task.executor = None
            self.task.department = new_department

            self.bot.send_message(old_executor.chat_id,
                                  f'Задача успешно передана отделу {new_department.name}')

            # Получаем всех сотрудников отдела
            employees = new_department.employees.all().exclude(id=old_executor.id)
            params = KeyboardHelper.get_string_for_params('task', Actions.APPLY.value, self.task.id)
            buttons = [(Buttons.APPLY, params)]
            keyboard = KeyboardHelper.get_inline_keyboard(buttons)
            msg = f'{old_executor.markdown_link} передал(а) задачу "{self.task.clear_text}" вашему отделу\n' \
                  f'Постановщик: {self.task.director.markdown_link}\n' \
                  f'Суть задачи: {self.task.clear_text}\n' \
                  f'Дедлайн: {self.task.clear_deadline}\n' \
                  f'Отдел: {new_department.name} [[{new_department.franchisee.name}]]\n'

            # Список для хранения информации об отправленных сообщениях
            msg_list = []
            # Отправляем уведомление
            for receiver in employees:
                msg_info = self.bot.send_message(receiver.chat_id, msg, reply_markup=keyboard,
                                                 parse_mode='Markdown')
                msg_list.append({'message_id': msg_info.message_id, 'chat_id': msg_info.chat.id})
            self.task.save()

        elif new_executor:
            self.bot.send_message(old_executor.chat_id,
                                  f'Задача успешно передана новому исполнителю {new_executor.markdown_link}',
                                  parse_mode='Markdown')
            self.task.executor = new_executor
            self.task.save()
            # Отправляем уведомление новому исполнителю
            self.bot.send_message(new_executor.chat_id,
                                  f'{old_executor.markdown_link} передал вам задачу "{self.task.clear_text}"\n'
                                  f'Комментарий {comment}', parse_mode='Markdown')
        else:
            raise AttributeError('Не передан отдел или исполнитель')

    def change_deadline(self, new_deadline: timezone.datetime):
        """Изменение дедлайна"""
        self.task.deadline = new_deadline
        self.task.save()

        # Отправляем уведомление, что дедлайн изменён
        self.bot.send_message(self.task.executor.chat_id,
                              f'{self.user.markdown_link} подтвердил(а) перенос дедлайна '
                              f'по задаче {self.task.clear_text}',
                              parse_mode='Markdown')

    def request_for_change_deadline(self, new_deadline: timezone.datetime,
                                    comment: str,
                                    message: types.Message = None) -> None:
        """Отправка запроса на изменение дедлайна постановщику"""
        str_new_deadline = new_deadline.strftime("%d.%m.%Y")
        keyboard = KeyboardHelper.get_inline_keyboard((
            [
                (
                    Buttons.CONFIRM,
                    KeyboardHelper.get_string_for_params('task', Actions.APPLY_NEW_DEADLINE.value,
                                                         self.task.id,
                                                         str_new_deadline)
                ),
                (
                    Buttons.REJECT,
                    KeyboardHelper.get_string_for_params('task',
                                                         Actions.REJECT_NEW_DEADLINE.value,
                                                         self.task.id,
                                                         )
                )
            ]
        ))

        msg = f'{self.task.executor.markdown_link} отправил(а) запрос на перенос' \
              f'дедлайна по задаче {self.task.clear_text}\n' \
              f'Новый дедлайн: {str_new_deadline}\n' \
              f'Причина: {comment}'

        self.bot.send_message(self.task.director.chat_id, msg,
                              reply_markup=keyboard,
                              parse_mode='Markdown')

        self.bot.send_message(self.task.executor.chat_id,
                              'Запрос на перенос дедлайна успешно отправлен постановщику задачи')


class TaskHelper:
    """Вспомогательный класс для работы с задачами"""

    # Информация для клавиатуры с выбором дедлайна
    # Формат ('Название', 'Количество дней')
    deadlines = (
        ('🔥 Сегодня', 0),
        ('📆 Завтра', 1),
        ('📆 3 дня', 2),
        ('📆 Неделя', 7),
        ('🖍 Свой', -1),
    )

    def __init__(self, bot: telebot.TeleBot):
        """Конструктор класса"""
        self.bot = bot

    def get_manager(self, user: User, task_id: Optional[int] = 0,
                    task: Optional[Task] = None, ) -> Optional[TaskManager]:
        """Получение менеджера для работы с задачей для существующей задачи"""
        # Если передан id задачи
        if task_id:
            try:
                task = Task.objects.get(id=task_id)
                return TaskManager(bot=self.bot, task=task, user=user)
            except Task.DoesNotExist:
                return None
        # Если передан объект задачи
        elif task:
            return TaskManager(bot=self.bot, task=task, user=user)
        else:
            raise AttributeError()

    @staticmethod
    def get_temp_task(chat_id) -> dict:
        """Запись временных данных связанных с задачей в Redis"""
        temp = cache.get(f'temp_task:{chat_id}')
        temp = {} if not temp else temp
        return temp

    @staticmethod
    def set_temp_task(chat_id, data: dict) -> None:
        """Получение временных данных, связанных с задачей из Redis"""
        cache.set(f'temp_task:{chat_id}', data, timeout=None)

    @staticmethod
    def get_type_message(message: Union[types.Message, types.CallbackQuery]) -> Optional[str]:
        """Определение типа сообщения"""
        if isinstance(message, types.Message):
            return 'text'
        elif isinstance(message, types.CallbackQuery):
            return 'callback'
        else:
            return None

    @classmethod
    def get_deadline_keyboard(cls) -> types.InlineKeyboardMarkup:
        # Формируем нужный формат данных, для формирования клавиатуры
        buttons = []
        for deadline in cls.deadlines:
            buttons.append(
                (deadline[0], KeyboardHelper.get_string_for_params('task', Actions.SELECT_DEADLINE.value, deadline[1]))
            )

        buttons = KeyboardHelper.list_to_table(buttons)

        buttons.append(
            (Buttons.CANCEL, KeyboardHelper.get_string_for_params('task', Actions.CANCEL.value))
        )

        return KeyboardHelper.get_inline_keyboard(buttons)

    @staticmethod
    def is_office_user(user: User) -> bool:
        """
        Проверка, является ли пользователь офисным сотрудником

        Если сотрудник имеет роль Офисный сотрудник, то он
        является офисным сотрудником :)
        """
        # Если у сотрудника роль Офисного сотрудника
        # или сотрудник привязан к отделу, то он является офисным
        if user.roles.filter(name='Офисный сотрудник').count() or user.department_set.count():
            return True
        else:
            return False

    @staticmethod
    def get_task_menu(user: User) -> types.InlineKeyboardMarkup:
        """
        Получение меню для задач (Выполняемые, поставленные и т.д)

        Шаблон меню:

        - Поставленные (Кол-во)
        - Выполняемые (Кол-во)
        - Ожидают подтверждения (Кол-во)
        - Нужно подтвердить (Кол-во)
        - Поставленные для вашего отдела (Кол-во)
        """

        # Получаем все незавершенные задачи для пользователя
        tasks = Task.objects.exclude(status=3)

        # Подсчитываем кол-во задач для каждого пункта меню
        deliver_count = tasks.filter(director=user, status__in=[0, 1]).exclude(executor=user).count()
        execute_count = tasks.filter(executor=user, status=1).count()

        wait_confirm_count = tasks.filter(executor=user, status=2).count()
        need_confirm_count = tasks.filter(director=user, status=2).count()

        wait_apply_count = tasks.filter(status=0, department__employees=user).exclude(director=user).count()

        items = [
            (
                (
                    f'Поставленные ({deliver_count})',
                    KeyboardHelper.get_string_for_params('task', Actions.LIST.value, Types.DIRECTOR.value)
                ),
                (
                    f'Выполняемые ({execute_count})',
                    KeyboardHelper.get_string_for_params('task', Actions.LIST.value, Types.EXECUTOR.value)
                )
            ),
            (
                f'Ожидают подтверждения ({wait_confirm_count})',
                KeyboardHelper.get_string_for_params('task', Actions.LIST.value, Types.WAIT_CONFIRM.value)
            ),
            (
                f'Нужно подтвердить ({need_confirm_count})',
                KeyboardHelper.get_string_for_params('task', Actions.LIST.value, Types.NEED_CONFIRM.value)
            ),
            (
                f'Ожидают принятия ({wait_apply_count})',
                KeyboardHelper.get_string_for_params('task', Actions.LIST.value, Types.WAIT_APPLY.value)
            ),
            (Buttons.EXIT, KeyboardHelper.get_string_for_params('task', Actions.EXIT.value)),
        ]

        keyboard = KeyboardHelper.get_inline_keyboard(items)

        return keyboard

    @staticmethod
    def get_department_keyboard(user: User, city_id: int) -> types.InlineKeyboardMarkup:
        """Получение клавиатуры для выбора отдела"""

        # Получаем доступные отделы
        if TaskHelper.is_office_user(user):
            departments = Department.objects.filter(franchisee_id=city_id)
        else:
            departments = Department.objects.filter(franchisee_id=city_id, only_office=False)

        # Формируем клавиатуру для выбора отдела
        items = []
        for department in departments:
            params = KeyboardHelper.get_string_for_params('task', Actions.SELECT_DEPARTMENT.value, department.id)
            items.append((department.name, params))

        # Кнопка 'Поставить себе'
        params = KeyboardHelper.get_string_for_params('task', Actions.SELECT_DEPARTMENT.value, -1)
        items = KeyboardHelper.list_to_table(items)
        items.append(('📌 Поставить себе', params))
        # Кнопка 'Отмена'
        params = KeyboardHelper.get_string_for_params('task', Actions.CANCEL.value)
        items.append((Buttons.CANCEL, params))

        return KeyboardHelper.get_inline_keyboard(items)

    @staticmethod
    def get_cities_keyboard(cities_queryset: QuerySet) -> types.InlineKeyboardMarkup:
        """Получение клавиатуру для выбора города при постановке задачи"""

        items = []
        for city in cities_queryset:
            params = KeyboardHelper.get_string_for_params('task', Actions.SELECT_CITY.value, city.id)
            items.append((city.name, params))

        # Кнопка 'Отмена'
        params = KeyboardHelper.get_string_for_params('task', Actions.CANCEL.value)
        items.append((Buttons.CANCEL, params))

        return KeyboardHelper.get_inline_keyboard(items)

    @staticmethod
    def get_executor_keyboard(executors_queryset: QuerySet) -> types.InlineKeyboardMarkup:
        """Получение клавиатуры для выбора исполнителя"""
        items = []
        for executor in executors_queryset:
            params = KeyboardHelper.get_string_for_params('task', Actions.SELECT_EXECUTOR.value, executor.id)
            items.append((f'👨🏽‍💻 {executor.full_name}', params))

        # Кнока 'Отделу'
        params = KeyboardHelper.get_string_for_params('task', Actions.SELECT_EXECUTOR.value, -1)
        items.append(('🏢 Отделу', params))

        # Кнопка 'Отмена'
        params = KeyboardHelper.get_string_for_params('task', Actions.CANCEL.value)
        items.append((Buttons.CANCEL, params))

        return KeyboardHelper.get_inline_keyboard(items)


class TaskProcessing:
    """Класс для обработки входящего запроса связанного с задачами"""

    def __init__(self, bot: telebot.TeleBot) -> None:
        """Конструктор класса"""
        self.bot = bot
        self.task_helper = TaskHelper(bot)
        self.message = None  # Объект полученного сообщения
        self.user = None  # Объект пользователя
        self.chat_id = None  # chat_id пользователя
        self.timezone = None  # Часовой пояс

    @staticmethod
    def isint(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def proc_message(self, message: Union[types.Message, types.CallbackQuery], user: User) -> None:
        message_type = TaskHelper.get_type_message(message)
        # Сохраняем в объект информацию о сообщениии и пользователе
        self.message = message
        self.user = user
        self.chat_id = user.chat_id

        # Устанавливаем временную зону и сохраняем её в объект
        timezone.activate(user.franchisee.timezone)
        self.timezone = timezone.get_current_timezone()

        # Вызываем нужный обработчик
        if message_type == 'text':
            self._proc_text(message)
        elif message_type == 'callback':
            self._proc_callback(message)
        else:
            raise TypeError('Неподдерживаемый тип данных')

    def _proc_callback(self, callback: types.CallbackQuery) -> None:
        """Обработка callback запроса"""

        # Получаем параметры, разделенные ':' и преобразуем всё в int
        params = callback.data.split(':')[1:]  # Вырезаем первый параметр
        row_params = params  # Необработанные параметры
        params = [int(x) for x in params if self.isint(x)]

        action = params[0]
        # Часто используемые параметры сообщения
        message_id = callback.message.message_id

        # Обработка выбора отдела, для которого ставится задача
        if action == Actions.SELECT_DEPARTMENT.value:
            # Получаем id департамента
            department = params[1]

            # Если сотрудник офисный, то предоставляем
            # ему возможность выбрать конкретного исполнителя
            if department != -1:
                # Получаем сотрудников отдела и формируем клаиватуру выбора исполнителя
                # Исключаем из списка текущего сотрудника
                executors = User.objects.filter(department=Department.objects.get(id=department)) \
                    .exclude(chat_id__in=[self.chat_id, 0])

                # Если режим передачи задачи новому исполнителю
                # то убираем постановщика из списка исполнителей
                if self.user.get_status() == Statuses.TRANSFER_TASK:
                    task = TaskHelper.get_temp_task(self.user.chat_id)
                    task = Task.objects.get(id=task['id'])
                    executors = executors.exclude(id=task.director.id)

                keyboard = TaskHelper.get_executor_keyboard(executors)

                self.bot.edit_message_text('Выберите исполнителя', self.chat_id, message_id,
                                           reply_markup=keyboard)
            else:
                self.bot.edit_message_text('Выберите дедлайн', self.chat_id, message_id,
                                           reply_markup=TaskHelper.get_deadline_keyboard())
                self.user.set_status(Statuses.CHOICE_DEADLINE)

            # Сохраняем инофрмацию о задаче в Redis хранилище
            task = TaskHelper.get_temp_task(self.chat_id)
            task['department'] = department
            TaskHelper.set_temp_task(self.chat_id, task)

        # Обработка выбора города, для которого ставиться задача
        elif action == Actions.SELECT_CITY.value:
            city_id = params[1]
            keyboard = TaskHelper.get_department_keyboard(self.user, city_id)
            self.bot.edit_message_text('Выберите отдел, для которого вы хотите поставить задачу',
                                       chat_id=self.user.chat_id,
                                       message_id=message_id, reply_markup=keyboard)
        # Обработка выбора конкрентного исполнителя задачи
        elif action == Actions.SELECT_EXECUTOR.value:
            # Получаем id исполнителя
            executor = params[1]
            # Если id исполнителя не равно -1, то задаче
            # назначен конкретный исполнитель
            if executor != -1:
                task = TaskHelper.get_temp_task(self.chat_id)
                task['executor'] = executor
                TaskHelper.set_temp_task(self.chat_id, task)

            # Если выбран исполнитель при передачи задачи
            if self.user.get_status() == Statuses.TRANSFER_TASK:
                self.bot.delete_message(self.chat_id, message_id)
                self.bot.send_message(self.chat_id, 'Введите комментарий:',
                                      reply_markup=KeyboardHelper.get_keyboard_markup(
                                          (Buttons.NO_COMMENT, Buttons.CANCEL)
                                      ))
            else:
                # Переключаемся на выбор дедлайна
                self.bot.edit_message_text('Выберите дедлайн', self.chat_id, message_id,
                                           reply_markup=TaskHelper.get_deadline_keyboard())
                self.user.set_status(Statuses.CHOICE_DEADLINE)

        # Обработка выбора дедлайна для задачи
        elif action == Actions.SELECT_DEADLINE.value:
            self.bot.delete_message(self.chat_id, message_id)
            days = params[1]  # Получаем кол-во дней до дедлайна

            # Если выбран кастомный дедлайн
            if days == -1:
                self.bot.send_message(self.chat_id, 'Введите дату дедлайна в формате дд.мм.гггг\n'
                                                    'Пример: 21.12.2020')
                if self.user.get_status() == Statuses.CHANGE_DEADLINE:
                    pass
                else:
                    self.user.set_status(Statuses.INPUT_CUSTOM_DEADLINE)
            # Устанавливаем для задачи нужный дедлайн
            else:
                now = timezone.localtime(timezone.now(), self.timezone)
                days = params[1]
                deadline = now + timedelta(days=days)
                # Обновляем данные о задаче в Redis
                task = TaskHelper.get_temp_task(self.chat_id)
                task['deadline'] = deadline
                TaskHelper.set_temp_task(self.chat_id, task)

                if self.user.get_status() == Statuses.CHANGE_DEADLINE:
                    self.bot.send_message(self.chat_id, 'Введите причину переноса дедлайна')
                    self.user.set_status(Statuses.INPUT_COMMENT_FOR_CHANGE_DEADLINE)
                else:
                    self.bot.send_message(self.chat_id, 'Введите суть задачи',
                                          reply_markup=KeyboardHelper.get_keyboard_markup(KeyboardHelper.CANCEL_ACTION))
                    self.user.set_status(Statuses.INPUT_TASK)

        # Обработка отмены действий
        elif action == Actions.CANCEL.value:
            self.bot.delete_message(self.chat_id, message_id)

            main_menu = KeyboardHelper.get_main_menu()
            self.bot.send_message(self.chat_id, 'Действия отменены', reply_markup=main_menu)

            # Очищаем временные данные и статус пользователя
            TaskHelper.set_temp_task(self.chat_id, dict())
            self.user.set_status(bot_.statuses.NOTHING)

        # Обработка закрытия окна(удаление сообщения)
        elif action == Actions.EXIT.value:
            self.bot.edit_message_text('Выберите дальнейшее действие', self.chat_id, message_id)

        # Переход на меню с категориями задач
        elif action == Actions.MENU.value:
            keyboard = TaskHelper.get_task_menu(self.user)
            self.bot.edit_message_text('Список задач', self.chat_id, message_id, reply_markup=keyboard)

        # Обработка кнопки назад
        elif action == Actions.BACK.value:
            current_menu = params[1]
            if current_menu == Actions.LIST.value:
                keyboard = TaskHelper.get_task_menu(self.user)
                self.bot.edit_message_text('Список задач', self.chat_id, message_id, reply_markup=keyboard)

        # Обработка приянтия задачи, поставленной на отдел
        elif action == Actions.APPLY.value:
            task_id = params[1]
            manager = self.task_helper.get_manager(self.user, task_id=task_id)
            manager.apply(callback.message)

        # Обработка вывода списка задач
        elif action == Actions.LIST.value:
            task_type = params[1]

            if task_type == Types.DIRECTOR.value:
                tasks = Task.objects.filter(director=self.user, status__in=[0, 1]).exclude(executor=self.user)
                title = 'Список поставленных вами задач'
            elif task_type == Types.EXECUTOR.value:
                title = 'Список выполняемых вами задач'
                tasks = Task.objects.filter(executor=self.user, status=1)
            elif task_type == Types.NEED_CONFIRM.value:
                tasks = Task.objects.filter(director=self.user, status=2)
                title = 'Список задач, требующие вашего внимания'
            elif task_type == Types.WAIT_CONFIRM.value:
                tasks = Task.objects.filter(executor=self.user, status=2)
                title = 'Список задач, ожидающих подверждение'
            elif task_type == Types.WAIT_APPLY.value:
                tasks = Task.objects.filter(status=0, department__employees=self.user).exclude(director=self.user)
                title = 'Список задач, ожидающих принятие'
            else:
                raise ValueError('Неизвестный тип задачи')

            items = []
            # Из списка задач формируем список для клавиатуры
            for task in tasks:
                items.append(
                    (f'{task.text}', KeyboardHelper.get_string_for_params('task', Actions.SHOW.value,
                                                                          task_type, task.id))
                )
            # Кнопка назад
            items.append(
                (Buttons.BACK, KeyboardHelper.get_string_for_params('task', Actions.MENU.value))
            )

            keyboard = KeyboardHelper.get_inline_keyboard(items)

            self.bot.edit_message_text(title, self.user.chat_id, message_id, reply_markup=keyboard)

        # Обработка показа конкретной задачи
        elif action == Actions.SHOW.value:
            task_type = params[1]
            task_id = params[2]
            # Получаем менеджер для работы с задачей
            manager = self.task_helper.get_manager(self.user, task_id=task_id)

            task = manager.task
            task_info = (
                f'Задача #{task.id} [[{task.franchisee.name}]]\n'
                f'Постановщик: {task.director.markdown_link}\n'
                f'Исполнитель: {task.executor.markdown_link if task.executor else "Не назначен"}\n'
                f'Отдел: {task.clear_deadline}\n'
                f'Суть: {task.clear_text}\n'
                f'Дедлайн: {task.clear_deadline}'
            )

            items = []

            # Определяем допустимый набор действий с задачей
            if task_type == Types.DIRECTOR.value:
                # Завершить
                items.append(
                    (Buttons.FINISH, KeyboardHelper.get_string_for_params('task', Actions.FINISH.value, task.id))
                )
            elif task_type == Types.EXECUTOR.value:
                # Завершить
                # Передать
                # Запрос смены дедлайна
                items.append((
                    Buttons.FINISH,
                    KeyboardHelper.get_string_for_params('task', Actions.FINISH.value, task.id),
                ))
                items.append((
                    Buttons.TRANSFER_TASK,
                    KeyboardHelper.get_string_for_params('task', Actions.TRANSFER.value, task.id),
                ))
                items.append((
                    Buttons.CHANGE_DEADLINE,
                    KeyboardHelper.get_string_for_params('task', Actions.CHANGE_DEADLINE.value, task.id),
                ))
            elif task_type == Types.WAIT_CONFIRM.value:
                # Повторить запрос
                items.append((
                    Buttons.FINISH, KeyboardHelper.get_string_for_params('task', Actions.FINISH.value, task.id)
                ))
            elif task_type == Types.NEED_CONFIRM.value:
                # Подтвердить
                # Отклонить
                items.append(
                    (Buttons.APPLY,
                     KeyboardHelper.get_string_for_params('task', Actions.CONFIRM.value, task.id)),
                )
                items.append(
                    (Buttons.REJECT,
                     KeyboardHelper.get_string_for_params('task', Actions.REJECT.value, task.id)),
                )
            elif task_type == Types.WAIT_APPLY.value:
                # Принять
                items.append(
                    (Buttons.APPLY,
                     KeyboardHelper.get_string_for_params('task', Actions.APPLY.value, task.id)),
                )

            # Кнопка назад и закрыть
            items.append((
                (Buttons.BACK, KeyboardHelper.get_string_for_params('task', Actions.LIST.value, task_type)),
                (Buttons.EXIT, KeyboardHelper.get_string_for_params('task', Actions.EXIT.value)),
            ))

            keyboard = KeyboardHelper.get_inline_keyboard(items)
            self.bot.edit_message_text(task_info, self.chat_id, message_id, reply_markup=keyboard,
                                       parse_mode='Markdown')

        # Обработка звершения задачи
        elif action == Actions.FINISH.value:
            task_id = params[1]
            manager = self.task_helper.get_manager(self.user, task_id=task_id)
            # Если завершает постановщик
            if manager.task.director == self.user:
                manager.finish(callback.message)
            # Иначе ждём комментария от пользователя
            else:
                markup = KeyboardHelper.get_keyboard_markup((Buttons.NO_COMMENT,))
                self.user.set_status(Statuses.INPUT_FINISH_COMMENT)
                self.bot.delete_message(self.user.chat_id, message_id)
                self.bot.send_message(self.user.chat_id, 'Введите комментарий для отправки задачи на подтверждение',
                                      reply_markup=markup)
                # Сохраняем ID задачи
                task = {
                    'id': task_id
                }
                TaskHelper.set_temp_task(self.user.chat_id, task)

        # Обработка принятия завершения задачи
        elif action == Actions.CONFIRM.value:
            task_id = params[1]
            manager = self.task_helper.get_manager(self.user, task_id=task_id)
            manager.confirmation(callback.message)

        # Обработка отклонения завершения задачи
        elif action == Actions.REJECT.value:
            task_id = params[1]
            manager = self.task_helper.get_manager(self.user, task_id=task_id)
            manager.reject(callback.message)

        # Обработка передачи задачи
        elif action == Actions.TRANSFER.value:
            task_id = params[1]
            self.user.set_status(Statuses.TRANSFER_TASK)
            TaskHelper.set_temp_task(self.chat_id, {'id': task_id})
            task = Task.objects.get(id=task_id)
            keyboard = TaskHelper.get_department_keyboard(self.user, task.franchisee.id)
            self.bot.edit_message_text('Выберите отдел, которому хотите передать задачу', self.chat_id,
                                       message_id, reply_markup=keyboard)

        # Обрботка переноса дедлайна
        elif action == Actions.CHANGE_DEADLINE.value:
            task_id = params[1]
            self.user.set_status(Statuses.CHANGE_DEADLINE)
            TaskHelper.set_temp_task(self.chat_id, {'id': task_id})

            # Показываем клавиатуру выбора дедлайна
            keyboard = TaskHelper.get_deadline_keyboard()
            self.bot.edit_message_text('Выберите дедлайн', self.chat_id,
                                       message_id=message_id, reply_markup=keyboard)

        # Обработка принятия переноса дедлайна
        elif action == Actions.APPLY_NEW_DEADLINE.value:
            task_id = params[1]
            deadline = timezone.datetime.strptime(row_params[2], "%d.%m.%Y")
            manager = self.task_helper.get_manager(user=self.user, task_id=task_id)
            keyboard = KeyboardHelper.get_inline_keyboard()
            self.bot.edit_message_text('Перенос дедлайна принят', self.chat_id, message_id)
            manager.change_deadline(deadline)

        # Обработка отказа в переносе дедлайна
        elif action == Actions.REJECT_NEW_DEADLINE.value:
            task_id = params[1]
            manager = self.task_helper.get_manager(user=self.user, task_id=task_id)

            self.bot.edit_message_text('Перенос дедлайна отклонен', self.chat_id, message_id)
            self.bot.send_message(manager.task.executor.chat_id,
                                  f'{manager.task.director.markdown_link} отказал(а) '
                                  f'в переносе дедлайна по задаче {manager.task.clear_text}',
                                  parse_mode='Markdown')

    def _proc_text(self, message: types.Message) -> None:
        """Обработка текстового запроса"""
        status = self.user.get_status()
        text = message.text

        # Обрабатываем ввод кастомного дедлайна
        if status in (Statuses.INPUT_CUSTOM_DEADLINE, Statuses.CHANGE_DEADLINE):

            try:
                input_date = parse(text, dayfirst=True).date()
            except ValueError:
                self.bot.send_message(self.chat_id, 'Неверный формат даты')
                return

            if input_date < timezone.localtime(timezone.now(), self.timezone).date():
                self.bot.send_message(self.chat_id, 'Указанная дата меньше текущей')
                return
            # Сохраняем дедлайн
            task = TaskHelper.get_temp_task(self.chat_id)
            task['deadline'] = input_date
            TaskHelper.set_temp_task(self.chat_id, task)

            # Меняем статус на следующий
            if status == Statuses.INPUT_CUSTOM_DEADLINE:
                self.user.set_status(Statuses.INPUT_TASK)
                msg = 'Введите суть задачи'
            elif status == Statuses.CHANGE_DEADLINE:
                self.user.set_status(Statuses.INPUT_COMMENT_FOR_CHANGE_DEADLINE)
                msg = 'Введите причину переноса дедлайна'
            else:
                msg = 'Неверное значение даты, попробуйте еще раз:)'

            self.bot.send_message(self.chat_id, msg)

        # Обработка ввода сути задачи
        # и создание задачи
        elif status == Statuses.INPUT_TASK:
            task = TaskHelper.get_temp_task(self.chat_id)

            # Формируем параметры для создания задачи
            deadline = task.get('deadline', None)
            executor = task.get('executor', None)
            department = task.get('department')
            franchisee = self.user.franchisee
            status = 1 if executor else 0
            if department == -1:
                # Если самозадача
                task = Task(
                    director=self.user,
                    executor=self.user,
                    franchisee=franchisee,
                    text=text,
                    deadline=deadline,
                    created=timezone.now(),
                    status=1,
                )
                task.save()
            else:
                # Создаём задачу
                task = Task(
                    director=self.user,
                    executor_id=executor,
                    department_id=department,
                    franchisee=franchisee,
                    text=text,
                    deadline=deadline,
                    created=timezone.now(),
                    status=status,
                )
                task.save()
                # Получаем менеджер задач и отправляем уведомление
                manager = self.task_helper.get_manager(task=task, user=self.user)
                manager.send_notification()

            task_info = (
                f'Задача создана\n'
                f'Постановщик: {task.director.markdown_link}\n'
                f'Исполнитель: {task.executor.markdown_link if task.executor else "Не назначен"}\n'
                f'Отдел: {task.clear_department}\n'
                f'Суть: {task.clear_text}\n'
                f'Дедлайн: {task.clear_deadline}'
            )
            # Отправляем сообщение о том, что задача создана
            # и возвращаем главное меню
            main_menu = KeyboardHelper.get_main_menu()
            self.bot.send_message(self.chat_id, task_info, reply_markup=main_menu, parse_mode='Markdown')

            # Очищаем временные данные и статус пользователя
            TaskHelper.set_temp_task(self.chat_id, dict())
            self.user.set_status(bot_.statuses.NOTHING)

        # Обработка ввода комментария для завершения задачи
        elif status == Statuses.INPUT_FINISH_COMMENT:
            comment = text if text != Buttons.NO_COMMENT else None
            task = TaskHelper.get_temp_task(self.user.chat_id)
            task_id = task['id']
            manager = self.task_helper.get_manager(self.user, task_id=task_id)
            manager.finish(comment=comment)
            self.user.set_status(bot_.statuses.NOTHING)
            self.bot.send_message(self.user.chat_id, 'Выберите дальнейшее действие',
                                  reply_markup=KeyboardHelper.get_main_menu())

        # Обработка ввода коммнетария после выбора нового исполнителя задачи
        elif status == Statuses.TRANSFER_TASK:
            comment = text if text != Buttons.NO_COMMENT else None
            task = TaskHelper.get_temp_task(self.user.chat_id)
            manager = self.task_helper.get_manager(self.user, task_id=task['id'])

            # Передаем задачу нужному исполнителю или отделу
            if 'department' in task:
                department = Department.objects.get(id=task['department'])
                if 'executor' in task:
                    executor = User.objects.get(id=task['executor'])
                    manager.transfer(new_executor=executor, comment=comment)
                else:
                    manager.transfer(new_department=department, comment=comment)
            else:
                raise AttributeError('Не передан отдел')

            self.bot.send_message(self.user.chat_id, 'Выберите дальнейшее действие',
                                  reply_markup=KeyboardHelper.get_main_menu())

            self.user.set_status(bot_.statuses.NOTHING)

        # Обработка запроса на перенос дедлайна после ввода комментария
        elif status == Statuses.INPUT_COMMENT_FOR_CHANGE_DEADLINE:
            comment = text
            task = TaskHelper.get_temp_task(self.user.chat_id)
            manager = self.task_helper.get_manager(user=self.user, task_id=task['id'])
            # Отправляем запрос на перенос дедлайна
            manager.request_for_change_deadline(task['deadline'], comment)
            self.bot.send_message(self.user.chat_id, 'Выберите дальнейшее действие',
                                  reply_markup=KeyboardHelper.get_main_menu())
