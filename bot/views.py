from django.shortcuts import render, get_object_or_404
from .models import Gallery, Image


# Create your views here.
def show_gallery(request, gallery_id: int):
    gallery = get_object_or_404(Gallery, pk=gallery_id)
    images = Image.objects.filter(gallery=gallery)
    context = {
        'id': gallery_id,
        'images': images
    }
    return render(request, 'bot/show_gallery.html', context)
