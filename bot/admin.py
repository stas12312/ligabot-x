from django.contrib import admin
from .models import *


# Register your models here.
class TokenAdmin(admin.ModelAdmin):
    list_display = ('id', 'value', 'user', 'link_for_auth')
    list_display_links = ('id', 'value')
    readonly_fields = ('link_for_auth', )
    fields = ('user', 'link_for_auth')

    def link_for_auth(self, obj):
        return f'https://telegram.me/liga_robotovbot?start={obj.value}'

    link_for_auth.short_description = 'Инвайт ссылка'


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'full_name', 'franchisee', 'delta_id', 'crm_id', 'chat_id')


class MeetingAdmin(admin.ModelAdmin):
    list_display = ('id', 'subject', 'franchisee')


class ReferenceAdmin(admin.ModelAdmin):
    list_display = ('franchisee', 'title', 'body', 'type', 'updated')


class CityAdmin(admin.ModelAdmin):
    list_display = ('api_id', 'name', 'timezone')


class RegularLessonAdmin(admin.ModelAdmin):
    list_display = ('id',)


admin.site.register(Franchisee, CityAdmin)
admin.site.register(Role)
admin.site.register(Token, TokenAdmin)
admin.site.register(RegularLesson, RegularLessonAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Image)
admin.site.register(LessonLog)
admin.site.register(Preparation)
admin.site.register(DayOfWeak)
admin.site.register(Location)
admin.site.register(Lesson)
admin.site.register(SortingLog)
admin.site.register(Gallery)
admin.site.register(Meeting, MeetingAdmin)
admin.site.register(MeetingDate)
admin.site.register(PhotoLog)
admin.site.register(Notification)
admin.site.register(Message)
admin.site.register(Department)
admin.site.register(Task)
admin.site.register(CRMInfo)
admin.site.register(DetailsOrder)
admin.site.register(Reference, ReferenceAdmin)
admin.site.register(GoogleInfo)
admin.site.register(TelegramGroup)
