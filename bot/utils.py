import itertools
from typing import Optional

import telebot
from django.db.models import QuerySet
from openpyxl import Workbook
from openpyxl.styles import Alignment

from .helpers import KeyboardHelper
from .models import *

MIN_KPI = 0.7


def check_auth(chat_id: int) -> Optional[User]:
    """
    Проверка авторизации пользователя в Telegram боте
    Изначально поиск происходит в хранилище Redis, затем в БД.
    При нахождении пользователя в БД информация об авторизации кэшируется на 1 минуту
    :param chat_id:
    :return:
    """

    # Пытаемся найти chat_id в Redis хранилище
    is_user_auth = cache.get(f'auth:{chat_id}_')
    if is_user_auth:
        return User.objects.get(chat_id=chat_id)
    # Проверяем авторизацию пользователя через БД
    else:
        try:
            user = User.objects.get(chat_id=chat_id, is_active=True)
            cache.set(f'auth:{chat_id}_', 'True', 60)
            return user
        except User.DoesNotExist:
            return None


def auth_required(bot: telebot.TeleBot, message_type: str = 'message'):
    """
    Функция для проверки авторизации пользователя

    Сохраняет информацию о пользователе в объекте сообщения,
    содержащее данные от Telegram
    :param bot:
    :param message_type:
    :return:
    """

    def decorator(func):
        def wrapped(msg):
            if message_type == 'message':
                chat_id = msg.chat.id
            elif message_type == 'call':
                chat_id = msg.message.chat.id
            else:
                return lambda: False
            try:
                if msg.chat.type != 'private':
                    return lambda: False
            except Exception as e:
                pass

            user = check_auth(chat_id)
            if user:
                msg.user = user
                func(msg)
            else:
                bot.send_message(chat_id, 'Для выполнения данного действия необходима авторизоваться',
                                 reply_markup=KeyboardHelper.get_keyboard_markup())
                return lambda: False

        return wrapped

    return decorator


def get_localtime(user: User = None, timezone_=None):
    """Получение локального времени для пользователя"""
    if user:
        timezone_ = pytz.timezone(user.franchisee.timezone)
    else:
        timezone_ = pytz.timezone(timezone_)
    return timezone.localtime(timezone.now(), timezone=timezone_)


def find_location(franchisee: Franchisee, name: str):
    """
    Поиск филиала не учитывая порядок слов в названии
    :param franchisee: Франчайзи
    :param name: Название филиала
    :return:
    """
    try:
        location_id = int(name)
        return Location.objects.get(franchisee=franchisee, id=location_id)
    except (Franchisee.DoesNotExist, ValueError):
        list_words = name.lower().split()
        # Если много слов, то не делаем перестановки
        if len(list_words) > 3:
            try:
                return Location.objects.get(franchisee=franchisee, name__iexact=name)
            except Location.DoesNotExist:
                pass
        else:
            # Получаем список всевозможных перестановок и ищем совпадение в БД
            list_perm = itertools.permutations(list_words)
            for item in list_perm:
                str_name = ' '.join(item)
                try:
                    return Location.objects.get(franchisee=franchisee, name__iexact=str_name)
                except Location.DoesNotExist:
                    pass
    return None


def get_weak(date: datetime.datetime, shift: int = 0):
    """
    Функция возвращает начало и конец недели
    :param date: Дата, для которой находится начало и конец недели
    :param shift: Сдвиг недели
    :return: Начало и конец недели
    """

    start_date = (date - timedelta(days=date.weekday() - shift))
    start_date = start_date.replace(hour=0, minute=0, second=0)
    end_date = (start_date + timedelta(days=6))
    end_date = end_date.replace(hour=23, minute=59, second=59)
    return {'start': start_date, 'end': end_date}


def get_lessons(location: Location, time: datetime.datetime, interval: int):
    """
    Функция для нахождения ближайших занятий на филиале
    :param interval: Интервал, в котором ищутся занятия
    :param time: Время, для которого находятся занятия (текущее)
    :param location: Филиал для которого находятся занятия
    :return: Кортеж найденных занятий
    """
    diff = timedelta(minutes=interval)
    min_time = time - diff
    max_time = time + diff
    # Получаем занятия, которые проходят в текущий день недели, начало которых попадают в нужный интервал
    lessons = location.lesson_set.filter(days_of_weak__number=time.weekday(),
                                         start__gte=min_time,
                                         start__lte=max_time)
    return lessons


def get_current_lessons(lessons: QuerySet, time: datetime.datetime, interval: int = 120, is_end=False) -> QuerySet:
    diff = timedelta(minutes=interval)
    min_time = time - diff
    max_time = time + diff
    if min_time.date() < time.date():
        min_time = min_time.replace(hour=0, minute=0)

    if max_time.date() > time.date():
        max_time = max_time.replace(hour=23, minute=59)
    if is_end:
        lessons = lessons.filter(end__gte=min_time.time(), end__lte=max_time.time())
    else:
        # Получаем занятия, которые проходят в текущий день недели, начало которых попадают в нужный интервал
        lessons = lessons.filter(start__gte=min_time.time(), start__lte=max_time.time())

    return lessons


def minute_interval(start: datetime.time, end: datetime.time,
                    is_abs: bool = True) -> int:
    """
    Функция для получение интервала, между временем
    :param start:
    :param end:
    :param is_abs:
    :return:
    """
    start_min = start.hour * 60 + start.minute
    end_min = end.hour * 60 + end.minute

    if is_abs:
        return abs(int(end_min - start_min))
    else:
        return int(end_min - start_min)


def get_kpi_for_user(
        interval_start: datetime.datetime,
        interval_end: datetime.datetime,
        user: User
):
    """
    Составление KPI для пользователя
    :param interval_start: Начало интервала
    :param interval_end: Конец интервала
    :param user: Пользователь
    :return:
    """
    timezone.activate(user.franchisee.timezone)

    # Начало интервала изменяем на ближайший прошедший понедельник
    # Это нужно для корректного построения KPI
    interval_start = interval_start - timedelta(days=interval_start.weekday())
    if interval_end.weekday() != 6 and interval_end.weekday() != 5:
        interval_end = interval_end - timedelta(days=(interval_end.weekday() + 1))

    # Определяем начало и конец недели
    weak_start = interval_start
    weak_end = interval_start + timedelta(days=6)
    weak_end = weak_end.replace(hour=23, minute=59, second=59)
    kpi = {
        'lesson_dates': [],
        'preparations': [],
        'photos': [],
        'lessons': [],
        'meetings': [],

    }
    sum_preparation = 0
    sum_meetings = 0
    sum_photos = 0
    count_lessons = 0
    count_meetings = 0
    # Флаг для завершения бесконечного цикла
    end_flag = True
    while end_flag:
        # Если вышли за интервал, то конец недели равен концу интервала
        if weak_end > interval_end:
            weak_end = interval_end
            end_flag = False
        else:
            days = weak_end.day, (weak_end - timedelta(days=1)).day
            kpi['lesson_dates'].append(f'{days[1]}-{days[0]}')
        if weak_start > weak_end:
            break
        # Переводим дату в UTC для сбора отчетности в абсолютном времени
        date_start = weak_start.astimezone(pytz.utc)
        date_end = weak_end.astimezone(pytz.utc)
        # Получаем нужные данные, для определения KPI
        # Проверяем, были ли занятие на этой недели
        lesson_logs = LessonLog.objects.filter(teacher=user,
                                               timestamp__gte=date_start,
                                               timestamp__lte=date_end)

        # Если занятия были, то собираем другую отчётность
        if lesson_logs.exists():
            count_lessons += 1
            kpi['lessons'].append(1)
            preparation_logs = Preparation.objects.filter(teacher=user,
                                                          timestamp__gte=date_start,
                                                          timestamp__lte=date_end)
            if preparation_logs.exists():
                kpi['preparations'].append(1)
                sum_meetings += 1
            else:
                kpi['preparations'].append(0)

            # Смотрим фотоотчеты с субботы по пятницу следующей недели
            photo_date_start = date_start + timedelta(days=5)
            photo_date_end = photo_date_start + timedelta(days=6)
            photos_logs = PhotoLog.objects.filter(teacher=user,
                                                  timestamp__gte=photo_date_start,
                                                  timestamp__lte=photo_date_end)
            if photos_logs.exists():
                kpi['photos'].append(1)
                sum_photos += 1
            else:
                kpi['photos'].append(0)

        else:
            kpi['preparations'].append('')
            kpi['photos'].append('')
            kpi['lessons'].append('')

        # Собрания проверяем в любом случае
        # Получаем список собраний на этой недели
        meetings = MeetingDate.objects.filter(datetime_carried__date__gte=weak_start,
                                              datetime_carried__date__lte=weak_end,
                                              meeting__franchisee=user.franchisee).values_list('meeting',
                                                                                               flat=True)

        meeting = ''
        meetings_id = set(meetings)
        if meetings_id:
            count_meetings += 1
            meeting = 0
            for meeting_id in meetings_id:
                if Meeting.objects.filter(id=meeting_id, teachers=user).exists():
                    meeting = 1
                    sum_meetings += 1

        kpi['meetings'].append(meeting)
        # Сдвигаем неделю
        weak_start += timedelta(days=7)
        weak_end += timedelta(days=7)

    # Считаем итоговые показатели
    try:
        avg = round(sum_preparation / count_lessons, 2)
        kpi['total_preparations'] = avg if avg > MIN_KPI else MIN_KPI
    except ZeroDivisionError:
        kpi['total_preparations'] = 1.00

    try:
        avg = round(sum_photos / count_lessons, 2)
        kpi['total_photos'] = avg if avg > MIN_KPI else MIN_KPI
    except ZeroDivisionError:
        kpi['total_photos'] = 1.00
    try:
        avg = round(sum_meetings / count_meetings, 2)
        kpi['total_meetings'] = avg if avg > MIN_KPI else MIN_KPI
    except ZeroDivisionError:
        kpi['total_meetings'] = 1.00

    kpi['total_kpi'] = round((kpi['total_preparations'] + kpi['total_photos'] + kpi['total_meetings']) / 3, 2)
    return kpi


def render_table_for_kpi(data, **kwargs):
    wb = Workbook()

    ws = wb.active
    count_dates = len(data[0]['lesson_dates'])
    # Позиции текста для заголовка
    pos_prepare = count_dates + 2
    pos_photo = 1 + count_dates + pos_prepare
    pos_meeting = 1 + count_dates + pos_photo
    # Формирование первой строки таблицы

    columns = ws.column_dimensions['A']
    columns.width = 40

    ws.append(['ФИО'])
    ws.cell(row=1, column=2, value='Подготовки').alignment = Alignment(horizontal='center')
    ws.cell(row=1, column=pos_prepare + 1, value='Итого - Подготовки')
    ws.cell(row=1, column=pos_prepare + 2, value='SMM').alignment = Alignment(horizontal='center')

    ws.cell(row=1, column=pos_photo + 2, value='Собрания').alignment = Alignment(horizontal='center')
    ws.cell(row=1, column=pos_photo + 1, value='Итого - Фото')

    ws.cell(row=1, column=pos_meeting + 1, value='Итого - Собрания')
    ws.merge_cells(start_row=1, end_row=1, start_column=2, end_column=pos_prepare)
    ws.merge_cells(start_row=1, end_row=1, start_column=pos_prepare + 2, end_column=pos_photo)
    ws.merge_cells(start_row=1, end_row=1, start_column=pos_photo + 2, end_column=pos_meeting)

    # Формируем вторую строку таблицы с датами
    ws.append(['', 'ИТОГО'] + data[0]['lesson_dates'] + [''] + data[0]['lesson_dates'] + [''] + data[0]['lesson_dates'])

    for teacher_kpi in data:
        ws.append([teacher_kpi['teacher_name']]
                  + [teacher_kpi['total_kpi']]
                  + teacher_kpi['preparations']
                  + [teacher_kpi['total_preparations']]
                  + teacher_kpi['photos']
                  + [teacher_kpi['total_photos']]
                  + teacher_kpi['meetings']
                  + [teacher_kpi['total_meetings']])

    hex = secrets.token_hex(6)
    interval_kpi = kwargs['start'].strftime('%d-%m-%Y') + '-' + kwargs['end'].strftime('%d-%m-%Y')

    filename = f'{hex}_{interval_kpi}_kpi.xlsx'
    wb.save(f'{settings.MEDIA_ROOT}/{filename}')
    return f'{settings.MEDIA_URL}{filename}'


def get_last_locations(user: User, count_days: int = 7) -> list:
    """Получение последник локаций на которых юыли занятия для пользователей"""
    now = timezone.localtime(timezone.now(), pytz.timezone(user.get_city().timezone))
    start = now - timedelta(days=count_days)
    lessons = Lesson.objects.filter(date__gte=start,
                                    date__lte=now,
                                    teachers=user,
                                    is_active=True) \
        .order_by('location__name', 'date') \
        .distinct('location__name', 'date')

    return lessons


def get_last_lessons(user: User, count_days: int = 7):
    """Получение последних занятий"""
    now = user.get_city().get_localtime()
    start = now - timedelta(days=count_days)
    return Lesson.objects.filter(date__gte=start, date__lte=now,
                                 teachers=user, is_active=True).order_by('date')


def clear_text(text):
    text = text.replace('[', r'\[')
    text = text.replace('_', r'\_')
    text = text.replace('*', r'\*')
    return text
