from django.urls import path
from . import views
from . import bot
urlpatterns = [
    path('<bot_token>', bot.reception_update, name='reception_update'),
    path('<gallery_id>/', views.show_gallery, name='show_gallery'),
]
