from django.contrib import admin
from .models import DefaultSetting, UserSetting, CitySetting, SettingGroup

admin.site.register(DefaultSetting)
admin.site.register(UserSetting)
admin.site.register(CitySetting)
admin.site.register(SettingGroup)
