from typing import Union

from django.contrib.postgres import fields
from django.db import models

from bot.models import User, Franchisee


class SettingGroup(models.Model):
    """Модель для группы настроек"""
    name = models.CharField(max_length=256, verbose_name='Название')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Группа настроек'
        verbose_name_plural = 'Группы настроек'


class DefaultSetting(models.Model):
    """Модель для настроек по умолчанию"""
    TYPES = (
        (0, 'Для пользователя'),
        (1, 'Для города')
    )

    VALUE_TYPES = (
        (0, 'Логичесикй'),
        (1, 'Целочисленный'),
        (2, 'Текстовый'),
    )

    type = models.IntegerField(verbose_name='Тип настройки', choices=TYPES)
    group = models.ForeignKey(SettingGroup, verbose_name='Группа', on_delete=models.PROTECT)
    value_type = models.IntegerField(verbose_name='Тип значения', choices=VALUE_TYPES)
    value = fields.JSONField(verbose_name='Значение по умолчанию')
    params = fields.ArrayField(models.CharField(max_length=256, blank=True, null=True),
                               verbose_name='Доступные параметры', null=True, blank=True)
    name = models.CharField(max_length=256, verbose_name='Название настройки')
    alias = models.CharField(max_length=256, verbose_name='Псевдоним')
    description = models.CharField(max_length=512, verbose_name='Описание настройки')

    def __str__(self):
        return f'{self.alias}({self.name})'

    class Meta:
        verbose_name = 'Настройка по умолчанию'
        verbose_name_plural = 'Настройки по умолчанию'

    @classmethod
    def cast_value(cls, value: str, value_type: int):
        """Преобразование значения к нужному типу"""
        print(value_type)
        if value_type == 0:
            value = bool(value)
        elif value_type == 1:
            value = int(value)
        return {'value': value}

    @classmethod
    def _get_query_info(cls, model):
        if isinstance(model, User):
            setting_type = 0
            model_class = UserSetting
            query = {'user': model}
        elif isinstance(model, Franchisee):
            model_class = CitySetting
            setting_type = 1
            query = {'city': model}
        else:
            raise AttributeError(f'Аргумент model не может являтся классом {model.__class__.__name__}')

        return model_class, setting_type, query

    @classmethod
    def get_setting(cls, alias: str, model: Union[User, Franchisee]):
        """
        Получение настройки для пользователя или города
        :param alias: Псевдоним настройки
        :param model: Модель, для которой получаем настройку
        :return:
        """

        model_class, setting_type, query = cls._get_query_info(model)

        try:
            setting = model_class.objects.get(base_setting__alias=alias, **query)
        except model_class.DoesNotExist:
            setting = cls.objects.get(alias=alias, type=setting_type)

        return setting.value['value']

    @classmethod
    def get_all_settings(cls, model: Union[User, Franchisee]) -> list:
        """Получение всех настроек для модели"""
        model_class, setting_type, query = cls._get_query_info(model)

        # TODO Разработать универсальный способ получать это
        model_custom_settings = model_class.objects.filter(**query).select_related()
        aliases = [setting.base_setting.alias for setting in model_custom_settings]
        model_default_settings = cls.objects.filter(type=setting_type).exclude(alias__in=aliases)
        # В итоговом списке возвращаем alias, name, group, value, value_type, params
        results = []
        for model_setting in model_custom_settings:
            base_setting = model_setting.base_setting
            results.append({
                'alias': base_setting.alias,
                'name': base_setting.name,
                'group': base_setting.group.name,
                **model_setting.value,
                'value_type': {
                    'number': base_setting.value_type,
                    'text': base_setting.get_value_type_display(),
                },
                'params': base_setting.params,
            })
        for base_setting in model_default_settings:
            results.append({
                'alias': base_setting.alias,
                'name': base_setting.name,
                'group': base_setting.group.name,
                **base_setting.value,
                'value_type': {
                    'number': base_setting.value_type,
                    'text': base_setting.get_value_type_display(),
                },
                'params': base_setting.params,
            })

        return results

    @classmethod
    def set_setting(cls, alias, model: Union[User, Franchisee], value):

        model_class, setting_type, query = cls._get_query_info(model)
        # Пытаемся получить сначала кастомную настройку
        try:
            setting = model_class.objects.get(base_setting__alias=alias, **query)
            value_type = setting.base_setting.value_type
            base_setting = cls.objects.get(alias=alias)
        except model_class.DoesNotExist:
            try:
                base_setting = cls.objects.get(alias=alias)
                value_type = base_setting.value_type
            except cls.DoesNotExist:
                raise ValueError(f'Настройка с посевдонимом {alias} не найдена')

            setting = model_class(base_setting=base_setting, **query)
        value = cls.cast_value(value, value_type)
        setting.value = value
        setting.save()

        return {
            'alias': base_setting.alias,
            'name': base_setting.name,
            'group': base_setting.group.name,
            **setting.value,
            'value_type': {
                'number': base_setting.value_type,
                'text': base_setting.get_value_type_display(),
            },
            'params': base_setting.params,
        }


class CitySetting(models.Model):
    """Модель для настроки города"""
    base_setting = models.ForeignKey(DefaultSetting, verbose_name='Базовая настройка', on_delete=models.PROTECT)
    city = models.ForeignKey(Franchisee, verbose_name='Город', on_delete=models.PROTECT)
    value = fields.JSONField(verbose_name='Значение')
    unique_together = (
        'base_setting',
        'user',
    )

    def __str__(self):
        return f'{self.city.name} - {self.base_setting.alias} - {self.value}'


class UserSetting(models.Model):

    """Модель для настройки пользователя"""
    base_setting = models.ForeignKey(DefaultSetting, verbose_name='Базовая настройка', on_delete=models.PROTECT)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.PROTECT)
    value = fields.JSONField(verbose_name='Значение')

    def __str__(self):
        return f'{self.user.full_name} - {self.base_setting.alias} - {self.value}'

    class Meta:
        unique_together = (
            'base_setting',
            'user',
        )
